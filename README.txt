* Add
* libraries from
* \\pekbamsrv910\REPOSITORIES\CentralRepository\PlanEstrategico lib
* in
* WebContent/WEB-INF/lib

la informaci�n de los templates est� en
	* com/velocity

el bean que velocity usa en spring es el sgte:

<bean id="velocityEngine" class="org.springframework.ui.velocity.VelocityEngineFactoryBean">
	   <property name="velocityProperties">
	      <value>
	         resource.loader = class
	         class.resource.loader.class = org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader
	         class.resource.loader.cache=false
			 velocimacro.library.autoreload = true
	      </value>
	   </property>
<!-- 	   <property name="resourceLoaderPath" value="/WEB-INF/velocity/" /> -->
	</bean>

y los jars adicionales para usar velocity son:
* velocity-1.7.jar
* velocity-tools-2.0.jar
