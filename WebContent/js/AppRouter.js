// Filename: router.js
define([
  'bootstrap',
  'underscore',
  'backbone'
], function(
      _bootstrap,
      _,
      Backbone
  ) {

  var curLiSelector;
  var prevLiSelector;

  var AppRouter = Backbone.Router.extend({
    initialize: function() {
      this.listenTo(Backbone.history, 'routeNotFound', this.onRouteNotFound);
    },
    onRouteNotFound: function() {
      // Backbone.history.navigate('', {trigger: true});
      $.cookie('errorMessage', 'no existe o no tiene permiso para acceder al enlace anterior');
      Backbone.history.navigate('', true);
    },
    deselectMenu: function() {
      if (prevLiSelector) {
          $(prevLiSelector).closest('li').removeClass('active');
      }
    },

    selectMenu : function(hrefSegment) {
        var selector = this.prepareSelector(hrefSegment);
        this.deselectMenu();
        $(selector).closest('li').addClass('active');
        prevLiSelector = selector;
    },

    deselectItem: function() {
      if (prevLiSelector && prevLiSelector.size()) {
        prevLiSelector.removeClass('active');
        prevLiSelector = prevLiSelector.find('ul').find('li');
        this.deselectItem();
      }
    },
    selectItem : function(hrefSegment) {
        curLiSelector = $(this.prepareSelector(hrefSegment)).closest('li');
        // console.log('curLiSelector: ', curLiSelector);
        this.deselectItem();
        prevLiSelector = this.parentLiSelector(curLiSelector);
    },
    parentLiSelector: function(curLiSelector) {
        if (curLiSelector && curLiSelector.size()) {
            curLiSelector.addClass('active');
            var nextParentLiSelector = curLiSelector.closest('ul').closest('li');
            if (nextParentLiSelector.size())
                return this.parentLiSelector(nextParentLiSelector);
            else
                return curLiSelector;
        }
    },

    prepareSelector: function(hrefSegment) {
      return 'a[href="' + hrefSegment + '"]';
    }
  });

  return AppRouter;

});