var el;

var PATH_REST = '';

var REST_BASE_PATH_SERVICE = '';
var PATH_SERVICE_GERENCIA = '';
var PATH_SERVICE_SUPERINTENDENCIA = '';

var parametros;

var debug = false;

/* global temp variable to custom debug */
var gg;

define(
        [
            'jquery',
            'editinplace',
            'bootstrap',
            'underscore',
            'backbone',
            'bbnotfound',

            'cookiejs',
            'notify',
            'colorbox',
            'moment',
            'fullcalendar',
            'langfullcalendar',
            'bootbox',

            'AppRouterMappings',
            // 'collections/ParametrosCollection',

            'text!../../templates/menuTemplate.html',
         ],
		function(
            $,
            editinplace,
            _bootstrap,
            _,
            Backbone,
            bbnotfound,

            cookiejs,
            notify,
            colorbox,
            moment,
            fullcalendar,
            langfullcalendar,
            bootbox,

            AppRouterMappings,
            // ParametrosCollection,

            menuTemplate
        ) {

    Backbone.View.prototype.close = function() {
        if (this.onClose) this.onClose();
        /* this was before bb 1.1.0 */
        /*
        this.remove();
        this.unbind();
        */
        /* from bb 1.1.0 and above */
        // When the view is displayed again, a new instance of it is used.
        // New event listeners are ths added. Hence, stop delegating events for the current view.
        // This ensures the listeners in this view are no longer triggered.
        // We don't invoke 'this.remove()' since the View must not remove elements from DOM.
        this.undelegateEvents();
    }

    window.ViewManager = {
        // A property to store the current view being displayed.
        currentView: null,

        // Display a Backbone View. Closes the previously displayed view gracefully.
        handle: function(view) {
            // gg = view;
            // Close the previous view
            if (this.currentView != null) {
                // Invoke the close method on the view.
                // All views have this method, defined via the 'Backbone.View.prototype.close' method.
                this.currentView.close();
            }
            // Display the current view
            this.currentView = view;
            // return this.currentView.render();
        }
    }

    var initialize = function() {
        AppRouterMappings.initialize();
    }

	return {
		initialize: initialize
	};
});