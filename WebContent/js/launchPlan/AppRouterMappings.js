define([
    'AppRouter',
    '../views/SelectPlanAnualView',
], function(
    AppRouter,
    SelectPlanAnualView
    ) {

    var appRouter = null;

    var initialize = function() {
        appRouter = new AppRouter;
        /* mappings :) */
        // var user = parametros.empleado;
        // appRouter.route('generar-cronograma-360', 'showCustomView', showCustomView);
        // appRouter.route('levantamiento(/:id)', 'showLevantamiento', showLevantamiento);
        /* for home */
        appRouter.route('', 'showSelectPlanAnualView', showSelectPlanAnualView);
        // appRouter.route('*actions', 'homeAction', homeAction);
        Backbone.history.start();

    }

    var showSelectPlanAnualView = function() {
        ViewManager.handle(new SelectPlanAnualView({}));
    }

    var homeAction = function(actions) {
        console.log('entra a home action');
        // console.log('actions: ', actions);
        ViewManager.handle(new HomeView());
        // We have no matching route, lets display the home page
        this.selectItem('#' + actions);
        // appRouter.navigate('/', true);
    }

    return {
    initialize: initialize
    };
});