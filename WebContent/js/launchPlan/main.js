/*from: http://www.anujgakhar.com/2012/11/23/loading-bootstrap-with-requirejs/*/
requirejs.config({
	// urlArgs: "dev=" + (new Date()).getTime(),
	waitSeconds: 30,
	paths : {
		text : "../text",
		jquery : "../lib/jquery.min",
		jquerymigrate : '../lib/jquery-migrate-1.2.1',
		bootstrap : "../lib/bootstrap.min",
		underscore: "../lib/underscore-min",
		backbone: "../lib/backbone-min",
		bbnotfound: "../lib/backbone.routeNotFound",
		handsontable: "../plugins/handsontable/dist/handsontable.full",
		handsontableremove: "../plugins/handsontable/dist/plugins/removeRow/handsontable.removeRow",
		handsamples: "../plugins/handsontable/js/samples",
		numeral: "../plugins/handsontable/lib/numeral",
		conflanguage: "../plugins/handsontable/lib/numeral.de-de",
		datepicker: "../plugins/datepicker/bootstrap-datepicker.min",
		langdatepicker: "../plugins/datepicker/locales/bootstrap-datepicker.es",
		chosen: "../plugins/chosen/chosen.jquery",
		datatables: "../../DataTables-1.10.10/media/js/jquery.dataTables.min",
		datatablesBootstrap: "../../DataTables-1.10.10/media/js/dataTables.bootstrap.min",
		linqjs: "../lib/linq.min",
		// datatablesColumnFilter: "../../DataTables-1.10.7/extensions/js/jquery.dataTables.columnFilter",
		utils: "../lib/utils",
        templates: '../templates',
		cookiejs: "../lib/jquery.cookie",
		notify: "../lib/notify.min",
		colorbox: "../lib/jquery.colorbox-min",
		moment: "../lib/fullcalendar-2.4.0/lib/moment.min",
		fullcalendar: "../lib/fullcalendar-2.4.0/fullcalendar.min",
		langfullcalendar: "../lib/fullcalendar-2.4.0/lang/es",
		bootbox: "../lib/bootbox/bootbox.min",
		kinetic: "../lib/kinetic-v5.1.0.min",
		editinplace: "../lib/jquery.editinplace",
	},
	shim : {
		bootstrap : {
			deps : [ 'jquery' ]
		},
		bootbox : {
			deps : [ 'bootstrap' ]
		},
		backbone: {
            'deps': ['jquery', 'underscore'],
            'exports': 'Backbone'
        },
        underscore: {
            exports: '_'
        },
        bbnotfound: {
        	'deps': ['backbone', 'underscore'],
        },
		utils : {
			deps : [ 'jquery' ]
		},
		notify : {
			deps : [ 'jquery' ]
		},
		colorbox : {
			deps : [ 'jquery' ]
		},
		handsamples : {
			deps : [ 'handsontable' ]
		},
		handsontableremove : {
			deps : [ 'handsontable' ]
		},

		conflanguage : {
			deps : [ 'handsontable' ]
		},
		conflanguage : {
			deps : [ 'numeral' ]
		},
		datepicker : {
			deps : [ 'jquery', 'bootstrap'],
			// exports: 'jQuery.fn.datepicker'
		},
		langdatepicker : {
			deps : ['datepicker']
		},
		fullcalendar: {
			deps: ['jquery', 'moment']
		},
		langfullcalendar : {
			deps : ['fullcalendar']
		},

		chosen : {
			deps: ['jquery'],
			exports: 'jQuery.fn.chosen'
		},
		datatables : {
			deps: ['jquery'],
			// exports: 'jQuery.fn.chosen'
		},
		datatablesBootstrap : {
			deps: ['datatables', 'bootstrap'],
			// exports: 'jQuery.fn.chosen'
		},
		editinplace : {
			deps: ['jquerymigrate'],
			// exports: 'jQuery.fn.chosen'
		},
		jquerymigrate : {
			deps: ['jquery'],
			// exports: 'jQuery.fn.chosen'
		},
		/*datatablesColumnFilter : {
			deps: ['datatables'],
			// exports: 'jQuery.fn.chosen'
		},
*/
	}
});
requirejs(['app'], function(App) {
	App.initialize();
});