define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ParametroCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/utils/parametros",
            // model: GerenciaModel
            fetchIniciativas : function(options) {
                if (options)
                    options.url = 'rest/utils/iniciativas/iddependencia/' + options.idDependencia +
                '/' + options.queryType + '/' + options.param;
                // console.log('options.url: ', BASE_URL + options.url);
                return Backbone.Collection.prototype.fetch.call(this, options);
            },
        });
        // You don't usually return a collection instantiated
        return ParametroCollection;
})