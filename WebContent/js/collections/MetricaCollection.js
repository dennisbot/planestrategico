define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var MetricaCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/metrica_kpi",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return MetricaCollection;
})