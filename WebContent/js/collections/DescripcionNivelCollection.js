define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var DescripcionNivelCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/descripcionNivel",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return DescripcionNivelCollection;
})