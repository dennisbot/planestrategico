define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var CategoriaCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/categoria",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return CategoriaCollection;
})