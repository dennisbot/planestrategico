define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var VistaCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/vista",
            // model: GerenciaModel
            initialize : function(models, options) {
            }
        });
        // You don't usually return a collection instantiated
        return VistaCollection;
})