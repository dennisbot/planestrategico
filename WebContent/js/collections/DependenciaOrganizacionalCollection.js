define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var DependenciaOrganizacionalCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/dependenciaOrganizacional",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return DependenciaOrganizacionalCollection;
})