define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var EmpleadoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/empleado",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return EmpleadoCollection;
})