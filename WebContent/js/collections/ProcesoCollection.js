define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ProcesoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/proceso",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return ProcesoCollection;
})