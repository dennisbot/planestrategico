define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var PeriodoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/periodo",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return PeriodoCollection;
})