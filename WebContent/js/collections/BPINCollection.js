define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var BPINCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/bpin",
            // model: GerenciaModel
            initialize : function(models, options) {
                // console.log('options: ', options);
                if (options && options.filter) {
                    this.url += '/' + options.filter.name + '/' + options.filter.type +
                    ((options.filter.param) ? '/' + options.filter.param : '');
                    // console.log('options: ', options);
                    // console.log('this.url: ', this.url);
                }
                // console.log('this.url: ', BASE_URL + this.url);
            },
            fetchIniciativas : function(options) {
                if (options)
                    options.url = 'rest/bpin/iniciativas/iddependencia/' +
                    options.idDependenciaOrganizacional +
                '/' + options.queryType + ((options.param) ? '/' + options.param : '');
                // console.log('options.url: ', BASE_URL + options.url);
                return Backbone.Collection.prototype.fetch.call(this, options);
            },
            fetchObjetivosEspecificos : function(options) {
                if (options)
                    options.url = 'rest/bpin/gerencias/iddependencia/' +
                    options.idDependenciaOrganizacional +
                '/' + options.queryType + ((options.param) ? '/' + options.param : '');
                // console.log('options.url: ', BASE_URL + options.url);
                return Backbone.Collection.prototype.fetch.call(this, options);
            },
            fetchObjetivosClaves : function(options) {
                if (options)
                    options.url = 'rest/bpin/vps/iddependencia/' +
                    options.idDependenciaOrganizacional +
                '/' + options.queryType + ((options.param) ? '/' + options.param : '');
                // console.log('options.url: ', BASE_URL + options.url);
                return Backbone.Collection.prototype.fetch.call(this, options);
            }

        });
        // You don't usually return a collection instantiated
        return BPINCollection;
})