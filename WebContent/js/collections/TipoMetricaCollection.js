define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var TipoMetricaCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/tipoMetrica",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return TipoMetricaCollection;
})