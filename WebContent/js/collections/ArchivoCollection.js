define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ArchivoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/archivo",
            // model: GerenciaModel
            initialize : function(models, options) {
                // console.log('options: ', options);
                if (options && options.idItemEstrategico) {
                    if (options.idItemEstrategico)
                        this.url += '/bymeta/' + options.idItemEstrategico;
                }
                // console.log('this.url: ', this.url);
            }
        });
        // You don't usually return a collection instantiated
        return ArchivoCollection;
})