define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ItemEstrategicoCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/itemEstrategico",
            // model: GerenciaModel
            initialize: function(models, options) {
                if (options && options.nivel) {
                    if (options.includeHierachy)
                        this.url += '/bylevelHierachy/' + options.nivel;
                    else
                        this.url += '/bylevel/' + options.nivel;
                }
                // console.log('this.url: ', BASE_URL + this.url);
            }
        });
        // You don't usually return a collection instantiated
        return ItemEstrategicoCollection;
})