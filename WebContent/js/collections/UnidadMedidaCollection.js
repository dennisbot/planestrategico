define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var UnidadMedidaCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/unidadMedida",
            // model: GerenciaModel
        });
        // You don't usually return a collection instantiated
        return UnidadMedidaCollection;
})