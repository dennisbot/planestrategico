define(function() {

    var init = function() {
    	var val;
    	if (window.location.origin.indexOf(910) != -1 || window.location.origin.indexOf('localhost') != -1)
    		val = "http://pekbamsrv910:8080/ReportServer?%2fPlanEstrategico&rs:Command=ListChildren";
    	else
    		val = "http://pekbamsrv443/ReportServer?%2fPlanEstrategico&rs:Command=ListChildren";
    	$('#ruta_reportes').attr('href', val);
    }
    var ReportServerPath = function() {
        if (window.location.origin.indexOf('localhost') != -1) {
            return "http://matrix/ReportServer";
        }
        if (window.location.origin.indexOf(910) != -1) {
            return "http://pekbamsrv910:8080/ReportServer"
        }
        return "http://pekbamsrv443/ReportServer"
    }
    return {
        init : init,
        ReportServerPath : ReportServerPath,
    };
});