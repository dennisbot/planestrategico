define(
    [
        'text!templates/PlanEstrategico/itemAction/cartillaResumenFinalizarListado.html',
        './modUtils',
        './modAjax',
    ],
    function(
            cartillaResumenFinalizarListadoTpl,
            utils,
            modAjax
    ) {

    var todelsource;

    var showGrid = function(config) {
        /*console.log('container: ', container);
        console.log('data: ', data);*/
        hot = new Handsontable(config.container, {
          data: config.data,
          startRows: config.data.length,
          startCols: 2,
          // minSpareRows: 1,
          rowHeaders: true,
          removeRowPlugin: true,
          outsideClickDeselects: false,
          columnSorting: false,
          sortIndicator: true,
          colHeaders: config.hot.col_headers,
          contextMenu: false,
          columns: config.hot.columns,
        });

        gg = hot;

        table = document.querySelector('table');
        Handsontable.Dom.addClass(table, 'table');
        Handsontable.Dom.addClass(table, 'table-hover');
        Handsontable.Dom.addClass(table, 'table-bordered');
        // Handsontable.Dom.addClass(table, 'table-striped');
        // Handsontable.Dom.addClass(table, 'table-condensed');

        /* hooks para controlar algunas cosas adicionales */
        hot.addHook('beforeRemoveRow', function(row, amount) {
            /*console.log('la fila va a ser eliminada');
            console.log('row before: ', row);
            console.log('amount before: ', amount);*/
            // console.log('lo cancelamos!');
            // return false;

            // todel = hot.getDataAtRow(row);
            todelsource = hot.getSourceDataAtRow(row);

        });

        hot.addHook('afterRemoveRow', function(row , amount) {
            /*console.log('la fila ha sido eliminada');
            console.log('row after: ', row);
            console.log('amount after: ', amount);
            console.log('todel: ', todel);
            console.log('todelsource: ', todelsource);*/

            var url = PATH_REST + config.path_rest_action_remove;
            var jsonVars = todelsource;
            console.log('jsonVars: ', jsonVars);
            modAjax.callAJAXPostJSON(
                    {
                        url: url,
                        jsonVars: jsonVars,
                        callback: function(){ /* alert("cartilla eliminada con éxito") */ }
                    }
            );
            /* limpiamos el panel de contenido */
            var len = hot.countRows();
            if (len > 0)
                hot.selectCell(0, 0);
            else
                $('#contenedor-datatable').empty();
        });

        hot.addHook('afterSelection', function(row , col, rowend, colend) {
            var len = hot.countRows();
            console.log('len: ', len);
            /*console.log('row: ', row);
            console.log('col: ', col);
            console.log('rowend: ', rowend);
            console.log('colend: ', colend);*/
            // console.log('ObjObservacion: ', ObjObservacion);

            ObjObservacion = hot.getSourceDataAtRow(row);

            var resumen = utils.prepareResumenListado(ObjObservacion.comportamientos);

            var n = 2;

            var lists = _.groupBy(resumen.laskeys, function(element, index) {
                  return Math.floor(index / n);
            });

            $('#contenedor-datatable').html(_.template(cartillaResumenFinalizarListadoTpl,
                {
                    o : ObjObservacion,
                    mapaCategorias : resumen.mapaCategorias,
                    lascats: lists,
                    laskeys: resumen.laskeys,
                    _ : _,
                }
            ));
        });

        hot.selectCell(0, 0);

        /*hot.addHook('afterChange', function(changes, source) {
            console.log('changes: ', changes);
            console.log('source: ', source);
        });*/
    }
    return {
        showGrid : showGrid,
    };
});