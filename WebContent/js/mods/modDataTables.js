define(
    [
        'datatables',
        'datatablesBootstrap',
        './modUtils',
        './modAjax',
        './modCamposDinamicos',
        './modLang',
    ],
    function(
        datatables,
        datatablesBootstrap,
        utils,
        modAjax,
        modCamposDinamicos,
        modLang
    ) {

    var config;
    var detailRows;
    var datatable;

    var showGrid = function(response, config) {

        var self = this;
        this.config = config;

        detailRows = [];
        this.datatable = config.$container.DataTable({
            // destroy: true,
            rowId: config.rowId,
            dom : "<'row mostrar-por'><'row'<'col-sm-4'l><'col-sm-4 add-item text-center'><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            // ajax: 'itemAction.do?id=mostrarConNombres',
            data: response,
            language: modLang.getLanEs(),
            /* no funciona con on, off, sino con live pero rompe otras cosas */
             // language: {
             //    url: 'js/localisation/es_ES.json'
             // },
             order: config.order,
             columns: config.columns,
             'bAutoWidth': true,
             'bAutoWidth': true,
             "columnDefs": config.columnDefs,
        });
        // console.log('this.datatable: ', this.datatable);
        this.prepararEdiciones();

        /*
        $('.dataTables_paginate').click(function() {
            self.prepararEdiciones();
        });
        $('.dataTables_filter').change(function() {
            self.prepararEdiciones();
        });
        $('.dataTables_length').change(function() {
            self.prepararEdiciones();
        });
        */

        $('.dataTables_paginate').off('click.datatables_click').on('click.datatables_click', function() {
            self.prepararEdiciones();
        })
        $('.dataTables_filter').off('change.datatables_filter').on('change.datatables_filter', function() {
            self.prepararEdiciones();
        })
        $('.dataTables_length').off('change.datatables_length').on('change.datatables_length', function() {
            self.prepararEdiciones();
        })

        this.datatable.columns.adjust().draw();

        if (this.config.hasDetails) {
            $(this.config.container + ' tbody').on( 'click', 'tr td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = self.datatable.row( tr );
                var idx = $.inArray( tr.attr('id'), detailRows );

                if ( row.child.isShown() ) {
                    tr.removeClass( 'details' );
                    row.child.hide();

                    // Remove from the 'open' array
                    detailRows.splice( idx, 1 );
                }
                else {
                    tr.addClass( 'details' );
                    row.child( self.config.formatDataTableDetails( row.data() ) ).show();
                    if (self.config.onChildRowRendered)
                        self.config.onChildRowRendered(row.data());
                    // Add to the 'open' array
                    if ( idx === -1 ) {
                        detailRows.push( tr.attr('id') );
                    }
                }
            } );

            // On each draw, loop over the `detailRows` array and show any child rows
            this.datatable.on( 'draw', function () {
                $.each( detailRows, function ( i, id ) {
                    $('#'+id+' td.details-control').trigger( 'click' );
                } );
            } );
        }
        // console.log('entro a adjust');
        // gg = hot;
    }
    var prepararEdiciones = function() {
        var self = this;
        $(this.config.container + ' a.obj-edit').off('click.doedit').on('click.doedit', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            // var row = self.datatable.row(curRow);
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doEdit)
                self.config.doEdit(curObj);
            /*var idProgramacion = $(this).attr('data-programacion-id');
            console.log('idProgramacion: ', idProgramacion);*/
            // return false;
        })

        // $(this.config.container + ' a.obj-edit').live('click.doedit', function(e) {
        //     e.preventDefault();
        //     console.log('entra en doedit');
        //     var curRow = $(this).closest('tr');
        //     // var row = self.datatable.row(curRow);
        //     var curObj = self.datatable.row(curRow).data();
        //     if (self.config.doEdit)
        //         self.config.doEdit(curObj);
        //     /*var idProgramacion = $(this).attr('data-programacion-id');
        //     console.log('idProgramacion: ', idProgramacion);*/
        //     // return false;
        // })


        $(this.config.container + ' a.obj-delete').off('click.dodelete').on('click.dodelete', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            // console.log('curRow: ', curRow);
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doDelete)
                self.config.doDelete(curObj);
            /*var idProgramacion = $(this).attr('data-programacion-id');
            console.log('idProgramacion: ', idProgramacion);*/
        })

        $(this.config.container + ' a.obj-go').off('click.dogo').on('click.dogo', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            // console.log('curRow: ', curRow);
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doGo)
                self.config.doGo(curObj);
            /*var idProgramacion = $(this).attr('data-programacion-id');
            console.log('idProgramacion: ', idProgramacion);*/
        })

        $(this.config.container + ' a.obj-select').off('click.doselect').on('click.doselect', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            // console.log('curRow: ', curRow);
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doSelect)
                self.config.doSelect(curObj);
            /*var idProgramacion = $(this).attr('data-programacion-id');
            console.log('idProgramacion: ', idProgramacion);*/
        })

    }

    return {
        config : config,
        showGrid : showGrid,
        datatable: datatable,
        prepararEdiciones: prepararEdiciones,
    };
});
