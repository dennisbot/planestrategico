define(function() {
    var sleep = function(seconds) {
       var currentTime = new Date().getTime();

       while (currentTime + seconds * 1000 >= new Date().getTime()) {
       }
    }
    var processCustomMessages = function() {
        if ($.cookie('successMessage')) {
            $.notify($.cookie('successMessage'), "success");
            $.removeCookie('successMessage');
        }
        if ($.cookie('errorMessage')) {
            $.notify($.cookie('errorMessage'), "error");
            $.removeCookie('errorMessage');
        }
        if ($.cookie('warnMessage')) {
            $.notify($.cookie('warnMessage'), "warn");
            $.removeCookie('warnMessage');
        }
        if ($.cookie('infoMessage')) {
            $.notify($.cookie('infoMessage'), "info");
            $.removeCookie('infoMessage');
        }

    }
    var createCookie = function(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    var readCookie = function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length).replace(/"/g, '');
        }
        return null;
    }

    var getMonths = function() {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];
        return monthNames;
    }

    var getTwoYearAbove = function() {
        var curYear = (new Date()).getFullYear();
        return [curYear, curYear + 1];
    }
    var getThreeYearsCurMid = function() {
        var curYear = (new Date()).getFullYear();
        return [curYear - 1, curYear, curYear + 1];
    }

    var generateConsecutiveArray = function(cantInspecciones) {
        var indexes = [];
        for (var i = 0; i < cantInspecciones; i++) {
            indexes.push(i + 1);
        }
        return indexes;
    }

    var getMonth = function(index) {
            var months = getMonths();
            return months[parseInt(index) - 1];
    }

    var getPanelClass = function(index) {
        var arr = ['panel-success', 'panel-warning', 'panel-danger'];
        return arr[index - 1];
    }
    var getNivelRiesgo = function(index) {
        var arr = ['BAJO', 'MEDIO', 'ALTO'];
        return arr[index - 1];
    }
    /* no es muy util, use the next instead */
    var uniqueID2 = function() {
        var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
        var uniqid = randLetter + Date.now();
        return uniqid;
    }

    var uniqueID = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    var fecha = function (date) {
        this.day = date.getDate();
        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
        this.toString = function() {
            return this.day + "/" + this.pad(this.month, 2, '0') + "/" + this.year;
        };
        this.pad = function(n, width, z) {
          z = z || '0';
          n = n + '';
          return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }

    var pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    // Validates that the input string is a valid date formatted as "mm/dd/yyyy"
    var isValidDate = function (dateString) {
        // First check for the pattern
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if(year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    };

    var descripcionCategorias = {};

    var prepareResumen = function(comportamientos) {
        var c = comportamientos;
        mapaCategorias = {};
        descripcionCategorias = {};
        laskeys = [];
        for (var key in c) {
            if (c.hasOwnProperty(key)) {
                catid = "categoria-" + c[key].ID_CATEGORIA.toString();
                /* inicializamos si no existía previamente */
                if (typeof mapaCategorias[catid] == "undefined") {
                    mapaCategorias[catid] = [];
                    laskeys.push(catid);
                }
                mapaCategorias[catid].push(c[key]);
                /* lo ordenamos */
                mapaCategorias[catid].sort(by('CODLETRA_COMPORTAMIENTO'));
            }
        }
        laskeys.sort();
        return mapaCategorias;
    }
    var by = function(name) {
        return function(o, p) {
            var a, b;
            if (typeof o === 'object' && typeof p === 'object' && o && p) {
                a = o[name].value;
                b = p[name].value;
                if (a === b) {
                    return 0;
                }
                if (typeof a === typeof b)
                    return a < b ? -1 : 1;
                return typeof a < typeof b ? -1 : 1;
            }
            else {
                throw {
                    name : 'Error',
                    message: 'Se esperaba un objeto cuando se ordenaba por ' + name
                };
            }
        }
    }
     var prepareResumenListado = function(comportamientos) {
        var c = comportamientos;
        var mapaCategorias = {};
        var laskeys = [];

        for (var i = 0; i < c.length; i++) {
            catid = "categoria-" + c[i].comportamiento.categoria.idCategoria.toString();
            /* inicializamos si no existía previamente */
            if (typeof mapaCategorias[catid] == "undefined") {
                mapaCategorias[catid] = [];
                laskeys.push(catid);
            }
            mapaCategorias[catid].push(c[i]);
            /* lo ordenamos */
            mapaCategorias[catid].sort(byListado('codLetra'))
        }
        laskeys.sort();
        return {
            mapaCategorias : mapaCategorias,
            laskeys: laskeys,
        };
    }
    var byListado = function(name) {
        return function(o, p) {
            var a, b;
            if (typeof o === 'object' && typeof p === 'object' && o && p) {
                a = o.comportamiento[name];
                b = p.comportamiento[name];
                if (a === b) {
                    return 0;
                }
                if (typeof a === typeof b)
                    return a < b ? -1 : 1;
                return typeof a < typeof b ? -1 : 1;
            }
            else {
                throw {
                    name : 'Error',
                    message: 'Se esperaba un objeto cuando se ordenaba por ' + name
                };
            }
        }
    }
    var getEtiquetaComportamiento = function(comportamientoID) {
        for (var i = 0; i < parametros.comportamientos.length; i++) {
            if (parametros.comportamientos[i].idComportamiento == comportamientoID)
                return parametros.comportamientos[i].codLetra +
            parametros.comportamientos[i].categoria.orden.toString() + " " +
            parametros.comportamientos[i].descripcion;
        };
        return "XYZ - ERROR";
    }
    var getTextID = function(selectorID) {
        /*$($0).is('input')
        $($0).is('textarea')
        $($0).attr('type')*/
        var el = $(selectorID);
        var esinput = el.is('input');
        var inputType = ['radio', 'checkbox', 'text', 'hidden'];
        if (esinput) {
            var index = -1;
            for (var i = 0; i < inputType.length; i++) {
                if (el.attr("type") == inputType[i]) {
                    index = i;
                    break;
                }
            };
            switch(inputType[index]) {
                case 'radio':
                case 'checkbox':
                    return el.is(':checked') ? 1 : 0;
                case 'text':
                case 'hidden':
                    return el.val();
            }
        }
        else {
            /* puede ser select o textarea */
            if (el.is('textarea'))
                return el.val();
            else {
                /* es select */
                var selected = el.find(':selected');
                if (selected.length > 0)
                    return el.find(':selected').text().trim();
                else
                    /* es un elemento html */
                    return el.text();
            }
        }
    }

    var getObservacionPreparada = function() {
        /* vamos a preparar la observación para guardarla en el servidor */
        var observacion = {
            fecha: cartilla.observacion.FECHA.key,
            idObservador: cartilla.observacion.ID_OBSERVADOR.key,
            idEspecialidadCategoria: cartilla.observacion.ID_ESPECIALIDAD_CATEGORIA.key,
            idActividadTarea: cartilla.observacion.ID_ACTIVIDAD_TAREA.key,
            idRangoTiempoEnProyecto: cartilla.observacion.ID_RANGO_TIEMPO_EN_PROYECTO.key,
            idRangoHorario: cartilla.observacion.ID_RANGO_HORARIO.key,
            idRangoEdad: cartilla.observacion.ID_RANGO_EDAD.key,
            idDisciplinaObservada: cartilla.observacion.ID_DISCIPLINA.key,
            idGuardia: cartilla.observacion.ID_GUARDIA.key,
            idEmpresaEspecializada: cartilla.observacion.ID_EMPRESA_ESPECIALIZADA.key,
            idCuadrilla: cartilla.observacion.ID_CUADRILLA.key,
            idAreaFrente: cartilla.observacion.ID_AREA_FRENTE.key,
        };

        /* la cartilla de observación tiene muchos comportamientos observados */
        var loscomportamientos = [];

        var cc = cartilla.comportamientos;

        for (var key in cc) {
            if (cc.hasOwnProperty(key)) {

                var el_comportamiento = {
                    comportamiento: {
                        idComportamiento : cc[key].ID_COMPORTAMIENTO,
                    },
                    esSeguro : cc[key].ES_SEGURO,
                    idPCELesion : cc[key].ES_SEGURO ? null : cc[key].ID_PCE_LESION.key,
                    idDetalleTipoCausaObstaculo : cc[key].ES_SEGURO ? null : cc[key].ID_DETALLE_TIPO_CAUSA_OBSTACULO.key,
                };

                /* creamos los comentarios adicionales a esta observación
                (SI ES QUE HUBIERAN) normalmente habrá si fue un comportamiento inseguro */
                if (!cc[key].ES_SEGURO || (cc[key].COMENTARIO_OBSERVADOR.key != "SIN COMENTARIO" &&
                    cc[key].COMENTARIO_RESPUESTA_OBSERVADO.key != "SIN COMENTARIO")) {
                    var comentario_obs_del_comportamiento = {
                        comentarioObservador: cc[key].COMENTARIO_OBSERVADOR.key,
                        comentarioRespuestaObservado: cc[key].COMENTARIO_RESPUESTA_OBSERVADO.key,
                        idLogro: cc[key].ID_LOGRO == undefined ? 0 : cc[key].ID_LOGRO.key,
                    };

                    /* establecemos la propiedad comentarioObservacionCompt del comportamiento observado */
                    el_comportamiento.comentarioObservacionComportamiento = comentario_obs_del_comportamiento;
                }
                loscomportamientos.push(el_comportamiento);
            }
        }
        observacion.comportamientos = loscomportamientos;
        return observacion;
    }
    var getItemActionsPreparados = function() {

        var curItemActions = [];
        for (var key in itemActions) {
            console.log('key: ', key);
            if (itemActions.hasOwnProperty(key)) {
                var itemAction = {};
                itemAction.priority = itemActions[key].PRIORITY.key;
                itemAction.title = itemActions[key].TITLE.key;
                itemAction.description = itemActions[key].DESCRIPTION.value;

                itemAction.mainResponsibility = itemActions[key].MAIN_RESPONSIBILITY.key;
                itemAction.mainResponsibilityName = itemActions[key].MAIN_RESPONSIBILITY.value;

                itemAction.secondResponsibility = itemActions[key].SECOND_RESPONSIBILITY.key !== undefined ? itemActions[key].SECOND_RESPONSIBILITY.key : 0;
                itemAction.secondResponsibilityName = itemActions[key].SECOND_RESPONSIBILITY.value;

                itemAction.raisedDate = itemActions[key].RAISED_DATE.key;
                itemAction.source = itemActions[key].SOURCE.key;
                itemAction.plannedDate = itemActions[key].PLANNED_DATE.key;
                itemAction.forecastDate = itemActions[key].FORECAST_DATE.key;
                itemAction.comments = itemActions[key].COMMENTS.value;
                // itemAction.gerencia = { id : itemActions[key].GERENCIA.key };
                itemAction.superintendencia = {
                    id: itemActions[key].SUPERINTENDENCIA.key,
                    nombre: itemActions[key].SUPERINTENDENCIA.value,
                    gerencia : {
                        id: itemActions[key].GERENCIA.key,
                        nombre: itemActions[key].GERENCIA.value,
                    }
                 };
                curItemActions.push(itemAction);
            }
        }
        return curItemActions;
    }
    var getItemActionPreparado = function(clave) {
        var itemAction = {};
        itemAction.idItemAction = clave;
        itemAction.priority = itemActions[clave].PRIORITY.key;
        itemAction.title = itemActions[clave].TITLE.key;
        itemAction.description = itemActions[clave].DESCRIPTION.value;
        itemAction.mainResponsibility = itemActions[clave].MAIN_RESPONSIBILITY.key;
        itemAction.secondResponsibility = itemActions[clave].SECOND_RESPONSIBILITY.key;
        itemAction.raisedDate = itemActions[clave].RAISED_DATE.key;
        itemAction.source = itemActions[clave].SOURCE.key;
        itemAction.plannedDate = itemActions[clave].PLANNED_DATE.key;
        itemAction.forecastDate = itemActions[clave].FORECAST_DATE.key;
        itemAction.closedDate = itemActions[clave].CLOSED_DATE.key == '' ? null : itemActions[clave].CLOSED_DATE.key;
        itemAction.comments = itemActions[clave].COMMENTS.value;
        // itemAction.gerencia = { id : itemActions[clave].GERENCIA.key };
        itemAction.superintendencia = { id: itemActions[clave].SUPERINTENDENCIA.key };
        return itemAction;
    }


    var getObservacionTest = function() {
        var observacion = {
            fecha: '26/05/2015',
            idObservador: 1,
            idEspecialidadCategoria: 1,
            idActividadTarea: 1,
            idRangoTiempoEnProyecto: 1,
            idRangoHorario: 1,
            idRangoEdad: 1,
            idDisciplinaObservada: 1,
            idGuardia: 1,
            idEmpresaEspecializada: 1,
            idCuadrilla: 1,
            idAreaFrente: 1,
        };

        /* la cartilla de observación tiene muchos comportamientos observados */
        var loscomportamientos = [];

        var el_comportamiento = {
            idComportamiento : 1,
            esSeguro : false,
            idPCELesion : 1,
            idDetalleTipoCausaObstaculo : 1,
        };
        var comentario_obs_del_comportamiento;
        comentario_obs_del_comportamiento = {
            comentarioObservador: "comentario del observador",
            comentarioRespuestaObservado: 'comentario respuesta observador',
            idLogro: 1,
        };
        /* establecemos la propiedad el comentario del comportamiento observado */
        el_comportamiento.comentarioObservacionComportamiento = comentario_obs_del_comportamiento;

        /* pusheamos el primer comportamiento */
        loscomportamientos.push(el_comportamiento);


        var el_comportamiento2 = {
            idComportamiento : 1,
            esSeguro : true,
        };
        /*var comentario_obs_del_comportamiento2;
        comentario_obs_del_comportamiento2 = {
            comentarioObservador: "comentario del observador 2",
            comentarioRespuestaObservado: 'comentario respuesta observador 2',
            idLogro: 1,
        };*/
        /* establecemos la propiedad el comentario del comportamiento observado */
        // el_comportamiento2.comentarioObservacionComportamiento = comentario_obs_del_comportamiento2;

        /* pusheamos el sgte comportamiento */
        loscomportamientos.push(el_comportamiento2);


        observacion.comportamientos = loscomportamientos;
        return observacion;
    }
    var setSelectAllOnFocus = function(selector) {
        $(selector).focus(function() {

            var $this = $(this);

            $this.select();

            window.setTimeout(function() {
                $this.select();
            }, 1);

            // Work around WebKit's little problem
            function mouseUpHandler() {
                // Prevent further mouseup intervention
                $this.off("mouseup", mouseUpHandler);
                return false;
            }

            $this.mouseup(mouseUpHandler);

        });
    }
    /*Enteros*/
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
           return false;
        return true;
    }
    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    var esGerente = function() {
        return parametros &&
        parametros.empleado &&
        parametros.empleado.cargo &&
        parametros.empleado.cargo.dependenciaOrganizacional &&
        parametros.empleado.cargo.dependenciaOrganizacional.idNivelJerarquico == 3;
    }

    var deepClone = function (item) {
      if (Array.isArray(item)) {
        var newArr = [];

        for (var i = item.length; i-- !== 0;) {
          newArr[i] = deepClone(item[i]);
        }

        return newArr;
      }
      else if (typeof item === 'function') {
        eval('var temp = ' + item.toString());
        return temp;
      }
      else if (typeof item === 'object')
        return Object.create(item);
      else
        return item;
    }

    var periodos = function() {
        return [
            {
                id : 1,
                periodo: 'Enero',
                abrev: 'Ene',
            },
            {
                id : 2,
                periodo: 'Febrero',
                abrev: 'Feb',
            },
            {
                id : 3,
                periodo: 'Marzo',
                abrev: 'Mar',
            },
            {
                id : 4,
                periodo: 'Abril',
                abrev: 'Abr',
            },
            {
                id : 5,
                periodo: 'Mayo',
                abrev: 'May',
            },
            {
                id : 6,
                periodo: 'Junio',
                abrev: 'Jun',
            },
            {
                id : 7,
                periodo: 'Julio',
                abrev: 'Jul',
            },
            {
                id : 8,
                periodo: 'Agosto',
                abrev: 'Ago',
            },
            {
                id : 9,
                periodo: 'Setiembre',
                abrev: 'Set',
            },
            {
                id : 10,
                periodo: 'Octubre',
                abrev: 'Oct',
            },
            {
                id : 11,
                periodo: 'Noviembre',
                abrev: 'Nov',
            },
            {
                id : 12,
                periodo: 'Diciembre',
                abrev: 'Dic',
            },
        ];
    }

    var formatBytes = function (bytes, decimals) {
       if(bytes == 0) return '0 Byte';
       var k = 1024;
       var dm = decimals + 1 || 3;
       var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
       var i = Math.floor(Math.log(bytes) / Math.log(k));
       return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
    }

    var extractToContext = function(options, context) {
        for (prop in options) {
            if (options.hasOwnProperty(prop)) {
                context[prop] = options[prop];
            }
        }
    }

    /*Fin enteros*/
    return {
        extractToContext : extractToContext,
        formatBytes : formatBytes,
        deepClone : deepClone,
        periodos : periodos,
        getThreeYearsCurMid : getThreeYearsCurMid,
        readCookie : readCookie,
        getMonths : getMonths,
        getTwoYearAbove : getTwoYearAbove,
        generateConsecutiveArray : generateConsecutiveArray,
        getMonth : getMonth,
        isNumeric : isNumeric,
        processCustomMessages : processCustomMessages,
        pad : pad,
        getItemActionsPreparados : getItemActionsPreparados,
        getItemActionPreparado : getItemActionPreparado,
        getPanelClass : getPanelClass,
        getNivelRiesgo : getNivelRiesgo,
        uniqueID : uniqueID,
        fecha: fecha,
        isValidDate : isValidDate,
        prepareResumen: prepareResumen,
        prepareResumenListado: prepareResumenListado,
        getTextID : getTextID,
        getObservacionTest: getObservacionTest,
        getObservacionPreparada: getObservacionPreparada,
        setSelectAllOnFocus: setSelectAllOnFocus,
        esGerente: esGerente,
        sleep: sleep,
    };
});
