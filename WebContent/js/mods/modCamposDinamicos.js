define([
    'chosen',
    'datepicker',
    'langdatepicker',
    ], function(chosen, datepicker, langdatepicker) {

    var iniciarDatePicker = function(options) {
        // $.datepicker.setDefaults( $.datepicker.regional["it"] );
        // $($0).data('datepicker').getFormattedDate('dd/mm/yyyy')
        //
        var dp = $(options.selector).datepicker({
            format: 'dd/mm/yyyy (DD)',
            // format: 'dd/mm/yyyy',
            autoclose: true,
            startDate: '-1d',
            language: 'es',
            todayHighlight: true,
        });

        if (options.setCurDate != null &&
            options.setCurDate !== undefined &&
            options.setCurDate)
            dp.datepicker('setDate', options.setCurDate)
        /*else
            dp.datepicker('setDate', new Date())*/

        dp.datepicker('update');

        if (options.onchange) {
            dp.on('changeDate', options.onchange);
        }
    }

    var iniciarChosen = function (options) {
      return $(options.selector).chosen({
            search_contains:  typeof options.search_contains  == "undefined" ? false : true,
            no_results_text: 'No se encontraron coincidencias',
            placeholder_text_single: options.placeholder ? options.placeholder : 'Seleccione una opción ...',
            placeholder_text_multiple: 'Seleccione opciones ...',
            width: '100%',
      });
    }

    var iniciarToolTips = function() {
        /* se inicializan algunos controles */
        $('[data-toggle="tooltip"]').tooltip();
    }

    var iniciarEditInPlace = function(options) {
        options.selector
        ? $(options.selector).editInPlace(options.args)
        : $('.editinplace').editInPlace(options.args);
        if (options && options.inputInplaceFieldSelector && options.onfocus) {
            $(options.inputInplaceFieldSelector).live('focus', options.onfocus);
        }
        if (options && options.inputInplaceFieldSelector && options.onblur) {
            $(options.inputInplaceFieldSelector).live('blur', options.onblur);
        }
    }

    var iniciarUploader = function(options) {
        // console.log('existe QQ?');
        // console.log('qq: ', qq);
        var uploader = new qq.FineUploader({
            // element: options.element,
            element: $(options.selector).get(0),
            debug: false,
            multiple: false,
            request: {
            // endpoint: '/handle_uploads/recibir_stream_foto',
                endpoint: options.endpoint,
            },
            text: options.text,
            template:
            '<div class="qq-uploader span12">' +
            '<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>' +
            '<div class="qq-upload-button btn btn-warning" style="width: auto;">{uploadButtonText}</div>' +
            '<span class="qq-drop-processing"><span>{dropProcessingText}</span>\
            <span class="qq-drop-processing-spinner"></span></span>' +
            '<ul class="qq-upload-list" style="margin-top: 10px; text-align: center;"></ul>' +
            '</div>',
            failedUploadTextDisplay: {
                mode : 'custom',
                maxChars : 200
            },
            classes: {
                success: 'alert alert-success',
                fail: 'alert alert-error'
            },
            validation : options.validation,
            messages : {
                typeError: "{file} tiene una extensión inválida. Extensión válida(s): {extensions}.",
                sizeError: "{file} is es muy grande, el tamaño máximo de archivo es de {sizeLimit}."
            },
            callbacks : {
                onSubmit : function(id, name) {
                    if ($('.qq-upload-list').children('li').length == 1) {
                        $('.qq-upload-list li:first-child').remove();
                    }
                },
                onComplete: function(id, name, response) {
                    if (options.callback)
                        options.callback();
                }
            }
        });
    }
    var iniciarUploaderFoto = function(options) {
        // console.log('existe QQ?');
        // console.log('qq: ', qq);
        var uploader = new qq.FineUploader({
            // element: options.element,
            element: $(options.selector).get(0),
            debug: false,
            multiple: false,
            request: {
            // endpoint: '/handle_uploads/recibir_stream_foto',
                endpoint: options.endpoint,
            },
            validation : {
                allowedExtensions : ['png', 'jpg'],
            },
            text: options.text,
            template:
            '<div class="qq-uploader span12">' +
            '<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>' +
            '<div class="qq-upload-button btn btn-warning" style="width: auto;">{uploadButtonText}</div>' +
            '<span class="qq-drop-processing"><span>{dropProcessingText}</span>\
            <span class="qq-drop-processing-spinner"></span></span>' +
            '<ul class="qq-upload-list" style="margin-top: 10px; text-align: center;"></ul>' +
            '</div>',
            failedUploadTextDisplay: {
                mode : 'custom',
                maxChars : 200
            },
            classes: {
                success: 'alert alert-success',
                fail: 'alert alert-error'
            },
            callbacks : {
                onSubmit : function(id, name) {
                    if ($('.qq-upload-list').children('li').length == 1) {
                        $('.qq-upload-list li:first-child').remove();
                    }
                },
                onComplete: function(id, name, response) {
                    parametros.empleado.uuidFoto = response.uuid;
                    $('#imagen-perfil').attr('src', 'uploadFoto/' + response.uuid);
                    // console.log('id: ', id);
                    // console.log('name: ', name);
                    // console.log('response: ', response);
                    // options.showGrid();
                }
            }
        });
    }


    var iniciarColorBox = function(options) {
        $(options.selector).colorbox({
            /* para hacer grupos de galeria  */
            // rel: 'color-box',
            photo: true,
            innerWidth:'80%',
            innerHeight:'80%'
        });
    }

    var init = function () {
        // console.log('init del modulo modCamposDinamicos.js');
        iniciarDatePicker({
                selector : '#fechaDatosGenerales'
        });
        iniciarChosen({
                selector : '.chosen-select',
                search_contains: true
        });
        iniciarToolTips();
    }

    var mensaje = "mensaje default";

    return {
        init : init,
        iniciarEditInPlace : iniciarEditInPlace,
        iniciarColorBox : iniciarColorBox,
        iniciarUploader : iniciarUploader,
        iniciarUploaderFoto : iniciarUploaderFoto,
        iniciarDatePicker : iniciarDatePicker,
        iniciarChosen : iniciarChosen,
        iniciarToolTips : iniciarToolTips,
        mensaje : mensaje,
    };
});