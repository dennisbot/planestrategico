define(
    [
        'underscore',
        '../ProgressBar',
        'text!templates/gestion/niveles/iniciativas/control/avanceProgressBarTemplate.html'
    ], function(
        _,
        ProgressBar,
        avanceProgressBarTemplate
    ) {

    function ProgressBarIniciativa(itemEstrategico, curPeriodo) {
        ProgressBar.call(this);
        this.itemEstrategico = itemEstrategico;
        this.curPeriodo = curPeriodo;
        this.isYTD = (curPeriodo) ? true : false;
        this.avance = this.getAvanceIniciativa();
        this.tolerancia = this.itemEstrategico.tolerancia;
    }

    ProgressBarIniciativa.prototype = Object.create(ProgressBar.prototype);

    ProgressBarIniciativa.prototype.getAvance = function() {
        return this.avance;
    }

    ProgressBarIniciativa.prototype.getAvanceIniciativa = function() {
        var self = this, res = 0, totalPlaneado = 0, totalReal = 0;
        _.each(this.itemEstrategico.itemEstrategicos, function(meta) {
            if (self.isYTD && meta.planEstrategico.anio == (new Date).getFullYear()) {
                if (meta.periodo.mes <= self.curPeriodo) {
                    totalReal += meta.valorReal;
                    totalPlaneado += meta.valorPlaneado;
                }
            }
            else {
                totalReal += meta.valorReal;
                totalPlaneado += meta.valorPlaneado;
            }
        })
        res = totalPlaneado == 0 ? 0 : Math.round(totalReal / totalPlaneado * 1000) / 10;
        return res;
        // return res.toFixed(2);
    }

    ProgressBarIniciativa.prototype.draw = function() {
        var backcolor = 'background-color:' + this.getBackColor();
        var forecolor = 'color:' + this.getForeColor();
        var style = backcolor + ';' + forecolor + (this.isBold() ? ';font-weight:bold' : '');
        return _.template(avanceProgressBarTemplate, {
            avance : this.avance,
            style: style
        });
    }

    return ProgressBarIniciativa;
});