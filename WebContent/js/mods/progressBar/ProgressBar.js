define(function() {

    function ProgressBar() {
        this.backcolor = '';
        this.forecolor = '';
        this.tolerancia = 0;
        this.isYTD = false;
    }

    ProgressBar.prototype = {
        getBackColor : function() {
            switch(true) {
                case (0 <= this.avance && this.avance <= 15):
                    this.backcolor = '#FF0000';
                    break;
                case (15 < this.avance && this.avance < 50):
                    this.backcolor = '#FF8C00';
                    break;
                case (this.avance + this.tolerancia >= 100):
                    this.backcolor = '#7FFF00';
                    break;
                default :
                    this.backcolor = '#FFFF00';
                    break;
            }
            return this.backcolor;
        },
        getForeColor : function() {
            switch(true) {
                case (0 <= this.avance && this.avance <= 15):
                    this.forecolor = 'white';
                    break;
                case (15 < this.avance && this.avance < 50):
                    this.forecolor = 'black';
                    break;
                case (this.avance + this.tolerancia >= 100):
                    this.forecolor = 'black';
                    break;
                default :
                    this.forecolor = 'black';
                    break;
            }
            return this.forecolor;
        },
        isBold : function() {
            return this.avance + this.tolerancia >= 100;
        },
        setBold : function($el) {
            this.isBold() ? $el.css('font-weight', 'bold') : $el.css('font-weight', 'normal');
        },
        draw : function() {
            console.log('default draw action, do nothing');
        }
    };
    return ProgressBar;
});