define(function() {
    return {
        canView : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canView : false;
        },
        canEdit : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canEdit : false;
        },
        canDelete : function(urlDescripcion) {
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canDelete : false;
        },
        canCreate : function(urlDescripcion) {
            var res = parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canCreate : false;
            // console.log('can create?:');
            // console.log('res: ', res);
            return res;
        },
        canUpload : function(urlDescripcion) {
            // console.log('can upload was called!');
            return parametros.permisos[urlDescripcion] ?
            parametros.permisos[urlDescripcion].canUpload : false;
        }
    }
});