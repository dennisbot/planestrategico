define([
    ], function() {

    var CategoriaValidator = function(modelInstance) {

        this.propertyValidator = {
            'descripcionCategoria': {
                'requerido' : {
                    'errorMessage' : 'el Campo "DESCRIPCIÓN DE LA CATEGORÍA" es REQUERIDO',
                    'modelInstance': null,
                },
            },
            'abreviaturaCategoria': {
                'requerido' : {
                    'errorMessage' : 'el Campo "ABREVIATURA DE LA CATEGORÍA" es REQUERIDO',
                    'modelInstance' : null,
                }
            },
        },
        this.customValidator = function (categoriaModel) {
            var subcategorias = categoriaModel.subcategorias;
            var msg = "necesitas especificar todos los pesos de esta categoria";
            // console.log('subcategorias: ', subcategorias);
            if (subcategorias.length == 0) return msg;
            for (var i = 0; i < subcategorias.length; i++) {
                if (subcategorias[i].get("peso").toString().trim() == "") {
                    $('#categoria-' + subcategorias[i].get('idCategoria')).focus();
                    return 'necesita añadir el peso de la significancia ' + subcategorias[i].get('significancia').toLowerCase();
                }
            }
            return false;
        }
    }

    return CategoriaValidator;
});