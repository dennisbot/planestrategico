define([
    '../DataTable/DataTable',
    'text!templates/gestion/buttonSaveTemplate.html',
    ], function(
        DataTable,
        buttonSaveTemplate
        ) {

    function ItemEstrategico() {
        this.curPeriods  = [],
        this.curItem = null,
        this.periodos = null,
        this.metricas = null,
        this.dataTable = new DataTable();
    }

    ItemEstrategico.prototype = {
        getCompiledSaveItemEstTemplate : function() {
        },

        getFocusableSelector : function() {
        },
        getButtonSaveSelector : function() {
        },
        getItemDescription : function() {
        },
        getInstanceValidator : function() {
        },
        getDiscriminator : function() {
        },
        getItemEstrategicoModel : function(options, context) {
            this.itemEstrategicoModel = new ItemEstrategicoModel({
                discriminator: this.getDiscriminator(options.level),
                descripcionNivel: {
                    idNivel: options.level,
                },
                planEstrategico : {
                    idPlanEstrategico : parametros.curPlanEstrategico.idPlanEstrategico,
                },
                idParentItem: options.idParentItem,
            });
            this._getResponsables = _.bind(this._getResponsables, context);
            this.itemEstrategicoModel.set('responsables', this._getResponsables());
        },
        getTableTemplate : function() {
        },
        getCurButtonMessage : function() {
        },
        attachTemplateParamsByLevel : function() {
        },
        customActionsBeforeHtmlTemplate : function() {
        },
        customActionsAfterHtmlTemplate : function(curItem, context) {
            $('#modal-area #add-responsable').click(_.bind(context.doAddResponsable, context));
        },
        getButtonSaveTemplate : function() {
            return _.template(buttonSaveTemplate, {
                level : this.level,
                curItem : this.curItem,
                newItemText : this.getCurButtonMessage(),
            });
        },
        _getResponsables : function() {
            var responsables = [];
            $.each(this.curResponsables, function(index, element) {
                responsables.push({
                    idResponsable : element,
                })
            })
            return responsables;
        }
    };
    return ItemEstrategico;
});