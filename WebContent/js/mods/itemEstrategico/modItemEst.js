    define([
    'models/ItemEstrategicoModel',
    'mods/modSecurity',

    'config/itemEstrategico/oestrategicoConfig',
    'mods/validaciones/ItemEstrategico/OEstrategicoValidator',
    'text!templates/gestion/niveles/oestrategicos/oestrategicoTableTemplate.html',
    'text!templates/gestion/niveles/oestrategicos/saveOEstrategicoTemplate.html',

    'config/itemEstrategico/oclaveConfig',
    'mods/validaciones/ItemEstrategico/OClaveValidator',
    'text!templates/gestion/niveles/oclaves/oclaveTableTemplate.html',
    'text!templates/gestion/niveles/oclaves/saveOClaveTemplate.html',

    'config/itemEstrategico/oespecificoConfig',
    'mods/validaciones/ItemEstrategico/OEspecificoValidator',
    'text!templates/gestion/niveles/oespecificos/oespecificoTableTemplate.html',
    'text!templates/gestion/niveles/oespecificos/saveOEspecificoTemplate.html',

    'config/itemEstrategico/iniciativaConfig',
    'mods/validaciones/ItemEstrategico/IniciativaValidator',
    'text!templates/gestion/niveles/iniciativas/iniciativaTableTemplate.html',
    'text!templates/gestion/niveles/iniciativas/periodoTemplate.html',
    'text!templates/gestion/niveles/iniciativas/selectPeriodoTemplate.html',
    'text!templates/gestion/niveles/iniciativas/saveIniciativaTemplate.html',
    'text!templates/gestion/niveles/iniciativas/subcategoriasIniciativaTemplate.html',

    'config/itemEstrategico/metaConfig',
    'mods/validaciones/ItemEstrategico/MetaValidator',
    'text!templates/gestion/niveles/metas/metaTableTemplate.html',
    'text!templates/gestion/niveles/metas/saveMetaTemplate.html',

    'config/itemEstrategico/metricaConfig',
    'text!templates/gestion/buttonSaveTemplate.html',

    'collections/MetricaCollection',
    'text!templates/kpi/metricaTableTemplate.html',
    'text!templates/kpi/columnaSelectMetricaTemplate.html',

    'text!templates/gestion/mostrarPorTemplate.html',

    'mods/modCamposDinamicos',
    'mods/modDataTables',
    'mods/modDataTablesModal',
    'mods/modUtils',
    'bootbox',

    'views/gestion/ManageKpiView',
    'text!templates/kpi/modal/filtroMetricaTableTableTemplate.html',
    ], function (
        ItemEstrategicoModel,
        modSecurity,

        oestrategicoConfig,
        OEstrategicoValidator,
        oestrategicoTableTemplate,
        saveOEstrategicoTemplate,

        oclaveConfig,
        OClaveValidator,
        oclaveTableTemplate,
        saveOClaveTemplate,

        oespecificoConfig,
        OEspecificoValidator,
        oespecificoTableTemplate,
        saveOEspecificoTemplate,

        iniciativaConfig,
        IniciativaValidator,
        iniciativaTableTemplate,
        periodoTemplate,
        selectPeriodoTemplate,
        saveIniciativaTemplate,
        subcategoriasIniciativaTemplate,

        metaConfig,
        MetaValidator,
        metaTableTemplate,
        saveMetaTemplate,

        metricaConfig,
        buttonSaveTemplate,

        MetricaCollection,
        metricaTableTemplate,
        columnaSelectMetricaTemplate,

        mostrarPorTemplate,

        modCamposDinamicos,
        modDataTables,
        modDataTablesModal,
        modUtils,
        bootbox,

        ManageKpiView,
        filtroMetricaTableTableTemplate
    ) {

    var modItemEst = {
        getSaveItemEstTemplate : function(level) {
            // console.log('entra al modulo get save item est template');
            switch(level) {
                case 1: return saveOEstrategicoTemplate;
                case 2: return saveOClaveTemplate;
                case 3: return saveOEspecificoTemplate;
                case 4: return saveIniciativaTemplate;
                case 5: return saveMetaTemplate;
            }
            return null;
        },

        getFocusableSelector: function(level) {
            switch(level) {
                case 1: return '#descripcion-oestrategico';
                case 2: return '#descripcion-oclave';
                case 3: return '#descripcion-oespecifico';
                case 4: return '#descripcion-iniciativa';
                case 5: return '#descripcion-meta';
            }
            return '';
        },

        getButtonSaveSelector: function (level) {
            switch(level) {
                case 1: return '#registrar-oestrategico';
                case 2: return '#registrar-oclave';
                case 3: return '#registrar-oespecifico';
                case 4: return '#registrar-iniciativa';
                case 5: return '#registrar-meta';
            }
            return '';
        },

        getItemDescription: function (level) {
            switch(level) {
                case 1: return 'el objetivo estratégico';
                case 2: return 'el objetivo clave';
                case 3: return 'el objetivo específico';
                case 4: return 'la iniciativa';
                case 5: return 'la meta';
            }
            return '';
        },

        getInstanceValidator: function(level) {
            switch(level) {
                case 1: return new OEstrategicoValidator();
                case 2: return new OClaveValidator();
                case 3: return new OEspecificoValidator();
                case 4: return new IniciativaValidator();
                case 5: return new MetaValidator();
            }
            return null;
        },

        getDiscriminator: function(level) {
            switch(level) {
                case 1: return 'OBJETIVO_ESTRATEGICO';
                case 2: return 'OBJETIVO_CLAVE';
                case 3: return 'OBJETIVO_ESPECIFICO';
                case 4: return 'INICIATIVA';
                case 5: return 'META';
            }
            return '';
        },

        getMostrarPorCompiledTemplate : function(level, picked) {
            return _.template(mostrarPorTemplate, {
                level : level,
                picked : picked,
                nivelDependencia : parametros.empleado.cargo.dependenciaOrganizacional.idNivelJerarquico
            })
        },

        attachEventHandlersMostrarPor : function(level, context, compiledTemplate, tableTemplateParams, back) {
            var self = this;
            $('#mostrar-por button').off('click.mostrar').on('click.mostrar', function() {
                context.$('#item-estrategico-container').html(compiledTemplate);
                var config = self.getDataTableConfig(level, context);
                var resCollection, picked = 'todos';
                switch($(this).attr('id')) {
                    case 'todos':
                        resCollection = context.curCollection;
                        break;
                    case 'mi-gerencia':
                        picked = 'mi-gerencia';
                        // console.log('context.curCollection: ', context.curCollection);
                        // console.log('level: ', level);
                        resCollection = _.filter(context.curCollection, function(itemEstrategico) {
                                return parametros.curGerencia.idDependenciaOrganizacional ==
                                (
                                (level == 3)
                                ? itemEstrategico.dependenciaOrganizacional.idDependenciaOrganizacional
                                : itemEstrategico.dependenciaOrganizacional.parentDependencia.idDependenciaOrganizacional
                                )
                                /* si nivel 3 es gerencia, sino sup o lo que sigue -> */
                            })
                        break;
                    case 'mi-superintendencia':
                        picked = 'mi-superintendencia';
                        resCollection = _.filter(context.curCollection, function(itemEstrategico) {
                            return itemEstrategico.dependenciaOrganizacional.idDependenciaOrganizacional ==
                            parametros.curSuperintendencia.idDependenciaOrganizacional;
                        })
                        break;
                }
                modDataTables.showGrid(resCollection, config);
                context.$('.mostrar-por').html(
                    self.getMostrarPorCompiledTemplate(level, picked)
                );
                if ((!back && context.curItem && level != 5) || level == 1) {
                    if (modSecurity.canCreate(context.curUrlDescripcion)) {
                        context.$('.add-item').html(
                            self.getButtonSaveTemplate(
                                level,
                                tableTemplateParams
                            )
                        );
                    }
                }
                self.attachEventHandlersMostrarPor(level, context, compiledTemplate, tableTemplateParams, back);
            })
        },
        getItemEstrategicoModel: function(options, context, isNew) {
            var itemEstrategicoModel = new ItemEstrategicoModel({
                discriminator: this.getDiscriminator(options.level),
                descripcionNivel: {
                    idNivel: options.level,
                },
                planEstrategico : {
                    idPlanEstrategico : parametros.curPlanEstrategico.idPlanEstrategico,
                },
                idParentItem: options.idParentItem,
            });
            this._getResponsables = _.bind(this._getResponsables, context);
            itemEstrategicoModel.set('responsables', this._getResponsables());
            switch(options.level) {
                case 1:
                    itemEstrategicoModel.set('descripcionItem', $('#descripcion-oestrategico').val().trim());
                    itemEstrategicoModel.set('dependenciaOrganizacional', { idDependenciaOrganizacional : 1 });
                    return itemEstrategicoModel;
                case 2:
                    itemEstrategicoModel.set('descripcionItem', $('#descripcion-oclave').val().trim());
                    itemEstrategicoModel.set('dependenciaOrganizacional', { idDependenciaOrganizacional : 2 });
                    return itemEstrategicoModel;
                case 3:
                    itemEstrategicoModel.set('descripcionItem', $('#descripcion-oespecifico').val().trim());
                    itemEstrategicoModel.set('dependenciaOrganizacional', {
                        idDependenciaOrganizacional : $('#select-gerencia').val()
                    });
                    return itemEstrategicoModel;
                case 4:
                    itemEstrategicoModel.set('descripcionItem', $('#descripcion-iniciativa').val().trim());
                    itemEstrategicoModel.set('dependenciaOrganizacional', {
                        idDependenciaOrganizacional : $('#select-superintendencia').val()
                    });
                    itemEstrategicoModel.set('categoria', { idCategoria : $('[name="subcategoria"]:checked').val() });
                    itemEstrategicoModel.set('metrica', { idMetrica : $('#idMetrica').val().trim() } );
                    itemEstrategicoModel.set('tolerancia', $('#tolerancia').val().trim());
                    itemEstrategicoModel.set('comentarios', $('#comentarios-iniciativa').val().trim());
                    itemEstrategicoModel.set('itemEstrategicos', this._getMetas(options, isNew));
                    itemEstrategicoModel.set('idKeyuser', $('#select-keyuser').val());
                    itemEstrategicoModel.set('idSupervisor', $('#select-supervisor').val());
                    return itemEstrategicoModel;
                case 5:
                    itemEstrategicoModel.set('descripcionItem', $('#descripcion-meta').val().trim());
                    return itemEstrategicoModel;
            }
        },

        getTableTemplate: function (level) {
            switch(level) {
                case 1: return oestrategicoTableTemplate;
                case 2: return oclaveTableTemplate;
                case 3: return oespecificoTableTemplate;
                case 4: return iniciativaTableTemplate;
                case 5: return metaTableTemplate;
            }
            return null;
        },

        getCurButtonMessage: function (level) {
            switch(level) {
                case 1: return 'AÑADIR EJE ESTRATÉGICO';
                case 2: return 'AÑADIR OBJETIVO CLAVE';
                case 3: return 'AÑADIR OBJETIVO ESPECÍFICO';
                case 4: return 'AÑADIR INICIATIVA';
                case 5: return 'AÑADIR META';
            }
            return '';
        },

        getDataTableConfig: function (level, context) {
            // console.log('entra a getDataTableConfig');
            var curConfig;
            switch(level) {
                case 1: curConfig = oestrategicoConfig; break;
                case 2: curConfig = oclaveConfig; break;
                case 3: curConfig = oespecificoConfig; break;
                case 4: curConfig = iniciativaConfig; break;
                case 5: curConfig = metaConfig; break;
            }
            return curConfig.getConfig(context);
        },

        attachTemplateParamsByLevel: function(level, genericParam, context) {
            switch(level) {
                case 3: genericParam.gerencias = context.getGerencias(); break;
                case 4:
                        genericParam.superintendencias = context.getSuperintendencias();
                        genericParam.curYear = parametros.curPlanEstrategico.anio;
                        genericParam.periodos = context.periodos.toJSON();
                        genericParam.categorias = context.categorias.toJSON();
                        break;
            }
        },
        _getResponsables : function() {
            var responsables = [];
            $.each(this.curResponsables, function(index, element) {
                responsables.push({
                    idResponsable : element,
                })
            })
            return responsables;
        },
        curPeriods : [],
        curItem: null,
        periodos: null,
        metricas: null,
        _getMetas: function(options, isNew) {
            // console.log('entra en _getmetas');
            var self = this;
            var metas = [];
            var selector = '#periodos-container>div.form-group';
            if (!isNew) selector += '.can-update';
            $(selector).each(function(index, element) {
                var $element = $(element);
                itemEstrategicoModel = new ItemEstrategicoModel({
                    idItemEstrategico : $element.attr('data-item-estrategico-id'),
                    discriminator: self.getDiscriminator(5),
                    descripcionNivel: {
                        idNivel: 5,
                    },
                    planEstrategico : {
                        idPlanEstrategico : parametros.curPlanEstrategico.idPlanEstrategico,
                    },
                    // idParentItem: options.idParentItem,
                    valorPlaneado: $element.find('input').val(),
                    periodo : {
                        idPeriodo : $element.find('.fa-remove').attr('data-periodo-id'),
                    },
                });
                metas.push(itemEstrategicoModel);

            });
            // console.log('en _getMetas');
            // console.log('metas: ', metas);
            return metas;
        },
        _reloadSelectPeriodo : function() {
            var diff = _.difference(_.pluck(this.periodos, 'idPeriodo'), this.curPeriods);
            var periodos = _.filter(this.periodos, function (periodo) {
                return _.find(diff, function(id) { return id == periodo.idPeriodo }) !== undefined;
            });
            $('#select-periodo-container').html(_.template(selectPeriodoTemplate, {
                periodos : periodos,
                curYear: parametros.curPlanEstrategico.anio,
            }));
            this._selectPeriodoChange();
        },
        _selectPeriodoChange: function() {
            var self = this;
            modCamposDinamicos.iniciarChosen({
                selector: '#select-periodo',
                search_contains: true,
                placeholder: 'Elige un periodo'
             });

            $('#select-periodo').change(function() {
                var periodoId = parseInt($(this).val());

                var selectedPeriodo =  _.find(self.periodos, function(periodo) {
                        return periodo.idPeriodo == periodoId;
                })

                var itemEstrategicoTempId = modUtils.uniqueID();

                var compiledTemplate = _.template(periodoTemplate, {
                    curYear: parametros.curPlanEstrategico.anio,
                    selectedPeriodo : selectedPeriodo,
                    itemEstrategicoTempId : itemEstrategicoTempId,
                })

                self.curPeriods.push(periodoId);
                self._reloadSelectPeriodo();

                $('#periodos-container').append(compiledTemplate);

                var divs = $('#periodos-container>div.form-group');
                divs.sort(function(a, b) {
                    var pIdA = parseInt($(a).find('.fa-remove').attr('data-periodo-id'));
                    var pIdB = parseInt($(b).find('.fa-remove').attr('data-periodo-id'));
                    return pIdA > pIdB;
                });

                $('#periodos-container').html(divs);

                self._attachRemoveHandler();

                $('#item-estrategico-' + itemEstrategicoTempId + ' input').focus();
            })
        },
        _attachRemoveHandler : function() {
            var self = this;
            $('#periodos-container .fa-remove').click(function() {
                var $this = $(this);
                var periodoId = parseInt($this.attr('data-periodo-id'));
                var itemEstrategicoId = $this.attr('data-item-estrategico-id');

                if (modUtils.isNumeric(itemEstrategicoId)) {
                    bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTE PERIODO?</strong></h4>',
                    function(ok) {
                        if (!ok) return;
                        var itemEstrategicoModel = new ItemEstrategicoModel({ idItemEstrategico : itemEstrategicoId });
                        itemEstrategicoModel.destroy({
                            success: function(model, response, options) {
                                $.notify('se eliminó la meta exitosamente', 'success');

                                self.itemEstrategicos = _.filter(self.itemEstrategicos, function(itemEstrategico) {
                                    return itemEstrategico.idItemEstrategico != itemEstrategicoId;
                                })

                                self.curPeriods = _.without(self.curPeriods, periodoId);
                                self._reloadSelectPeriodo();
                                $this.closest('.form-group').remove();

                                /* reload the datatable */
                                self.context.loadItemsByLevel(4);

                                // console.log('response: ', response);
                            },
                            error: function() {
                                $.notify('ERROR conectandose con el servidor', 'error');
                                console.log('response: ', response);
                            },
                        })
                    })
                }
                else {
                    self.curPeriods = _.without(self.curPeriods, periodoId);
                    self._reloadSelectPeriodo();
                    $this.closest('.form-group').remove();
                }
            });
        },
        _loadSubcategorias: function(categoria) {
            var idCategoriaParent;
            if (typeof categoria == 'object')
                idCategoriaParent = categoria.idCategoriaParent;
            else
                idCategoriaParent = categoria;
            var pesos = _.sortBy(_.find(this.categorias, function(categoria) {
                return categoria.idCategoria == idCategoriaParent
            }).subcategorias, function(subcategoria) {
                return subcategoria.peso;
            });
            $('.subcategorias').html(_.template(subcategoriasIniciativaTemplate, {
                pesos : pesos,
                categoria: (typeof categoria == 'object') ? categoria : null,
            }));
        },
        customActionsBeforeHtmlTemplate : function(level, curItem, context) {
            switch(level) {
                case 4:
                    // console.log('===================================================');
                    // console.log('curItem: ', curItem);
                    // console.log('curItem.itemEstrategicos: ', curItem.itemEstrategicos);
                    if (curItem && curItem.itemEstrategicos) {
                        curItem.itemEstrategicos = $(curItem.itemEstrategicos).sort(function(a, b) {
                            return a.periodo.mes > b.periodo.mes
                        })
                    }
                    break;
            }
        },
        customActionsAfterHtmlTemplate: function(level, curItem, context) {
            $('#modal-area #add-responsable').click(_.bind(context.doAddResponsable, context));
            switch(level) {
                case 3:
                    modCamposDinamicos.iniciarChosen({
                        selector: '#select-gerencia',
                        search_contains: true,
                        placeholder: 'Elige una gerencia'
                    });
                    break;
                case 4:
                    $('#modal-area #add-keyuser').click(_.bind(context.doAddKeyuser, context));
                    modCamposDinamicos.iniciarChosen({
                        selector: '.keyuser #select-keyuser',
                        search_contains: true,
                        placeholder: 'Elige un Keyuser',
                    });
                    $('#modal-area #add-supervisor').click(_.bind(context.doAddSupervisor, context));

                    modCamposDinamicos.iniciarChosen({
                        selector: '.supervisor #select-supervisor',
                        search_contains: true,
                        placeholder: 'Elige un Supervisor',
                    });
                    context._addEventHandleRemoveKeyUser();
                    context._addEventHandleRemoveSupervisor();


                    this.context = context;
                    this.curPeriods = [];
                    this.curItem = curItem;
                    this.categorias = context.categorias.toJSON();

                    if (curItem) {
                        this.curPeriods = _.pluck(_.pluck(curItem.itemEstrategicos, 'periodo'), 'idPeriodo');
                        this._loadSubcategorias(curItem.categoria);
                    }

                    this.periodos = context.periodos.toJSON();

                    modCamposDinamicos.iniciarChosen({
                        selector: '#select-superintendencia',
                        search_contains: true,
                        placeholder: 'Elige una superintendencia'
                    });
                    modCamposDinamicos.iniciarChosen({
                        selector: '#select-categoria',
                        search_contains: true,
                        placeholder: 'Elige una categoria'
                    });

                    this._attachRemoveHandler();
                    this._reloadSelectPeriodo();

                    var self = this;
                    $('#select-categoria').change(function() {
                        var me = $(this);
                        // var text = me.find(':selected').text();
                        self._loadSubcategorias(me.val());
                    });
                    $('#select-metrica').click(function(e) {
                        // console.log('entra a doSelectMetrica dd');
                        e.preventDefault();
                        self.metricas = new MetricaCollection();
                        var complete = _.invoke([self.metricas], 'fetch');
                        $.when.apply($, complete)
                        .done(function() {
                            self.manageKpiView = new ManageKpiView({
                                el : '#modal-area-2',
                                mainTemplate : filtroMetricaTableTableTemplate,
                                isModal : true
                            });
                        })
                        .fail(function(response) {
                            $.notify('ERROR conectandose con el servidor', 'error');
                            console.log('response: ', response);
                        });
                    });
                    break;

            }
        },

        getButtonSaveTemplate : function(level, tableTemplateParams) {
            return _.template(buttonSaveTemplate, tableTemplateParams);
        }
    }
    return modItemEst;
});