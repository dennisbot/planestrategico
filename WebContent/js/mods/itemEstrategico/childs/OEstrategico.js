define(
    [
        'underscore',
        '../ItemEstrategico',
        'models/ItemEstrategicoModel',
        'config/itemEstrategico/oestrategicoConfig',

        'mods/validaciones/ItemEstrategico/OEstrategicoValidator',

        'text!templates/gestion/buttonSaveTemplate.html',
        'text!templates/gestion/niveles/oestrategicos/saveOEstrategicoTemplate.html',
        'text!templates/gestion/niveles/oestrategicos/oestrategicoTableTemplate.html',
    ], function(
        _,
        ItemEstrategico,
        ItemEstrategicoModel,
        oestrategicoConfig,
        OEstrategicoValidator,

        buttonSaveTemplate,
        saveOEstrategicoTemplate,
        oestrategicoTableTemplate
    ) {

    function OEstrategico() {
        ItemEstrategico.call(this);
        /* init additional properties */
        this.level = 1;
    }
    OEstrategico.prototype = Object.create(ItemEstrategico.prototype);

    OEstrategico.prototype.getCompiledSaveItemEstTemplate = function() {
        return _.template(saveOEstrategicoTemplate, {
        });
    }

    OEstrategico.prototype.getFocusableSelector = function() {
        return '#descripcion-oestrategico';
    }

    OEstrategico.prototype.getButtonSaveSelector = function() {
        return '#registrar-oestrategico';
    }

    OEstrategico.prototype.getItemDescription = function() {
        return 'el objetivo estratégico';
    }

    OEstrategico.prototype.getInstanceValidator = function() {
        return new OEstrategicoValidator();
    }

    OEstrategico.prototype.getDiscriminator = function() {
        return 'OBJETIVO_ESTRATEGICO';
    }

    OEstrategico.prototype.getItemEstrategicoModel = function(options, context) {
        itemEstrategicoModel.set('descripcionItem', $('#descripcion-oestrategico').val().trim());
        itemEstrategicoModel.set('dependenciaOrganizacional', { idDependenciaOrganizacional : 1 });
        return itemEstrategicoModel;
    }

    OEstrategico.prototype.getTableTemplate = function() {
        return oestrategicoTableTemplate;
    }

    OEstrategico.prototype.getCompiledTableTemplate = function() {
        return _.template(oestrategicoTableTemplate, {
        });
    }

    OEstrategico.prototype.getCurButtonMessage = function() {
        return 'AÑADIR OBJETIVO ESTRATÉGICO';
    }

    OEstrategico.prototype.showGrid = function(response, context) {
        this.dataTable.showGrid(response, oestrategicoConfig.getConfig(context))
    }

    OEstrategico.prototype.attachTemplateParamsByLevel = function(genericParam, context) {
    }
    OEstrategico.prototype.customActionsBeforeHtmlTemplate = function(curItem, context) {
    }
    OEstrategico.prototype.customActionsAfterHtmlTemplate = function(curItem, context) {
    }

    return OEstrategico;
});