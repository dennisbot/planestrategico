define(
    [
        'underscore',
        '../ItemEstrategico',
        'models/ItemEstrategicoModel',
        'config/itemEstrategico/oespecificoConfig',

        'mods/validaciones/ItemEstrategico/OEspecificoValidator',

        'text!templates/gestion/buttonSaveTemplate.html',
        'text!templates/gestion/niveles/oespecificos/saveOEspecificoTemplate.html',
        'text!templates/gestion/niveles/oespecificos/oespecificoTableTemplate.html',
    ], function(
        _,
        ItemEstrategico,
        ItemEstrategicoModel,
        oespecificoConfig,

        OEspecificoValidator,

        buttonSaveTemplate,
        saveOEspecificoTemplate,
        oespecificoTableTemplate
    ) {

    function OEspecifico() {
        ItemEstrategico.call(this);
        /* init additional properties */
        this.level = 3;
    }
    OEspecifico.prototype = Object.create(ItemEstrategico.prototype);

    OEspecifico.prototype.getCompiledSaveItemEstTemplate = function() {
        return _.template(saveOEspecificoTemplate, {
        });
    }
    OEspecifico.prototype.getFocusableSelector = function() {
        return '#descripcion-oespecifico';
    }
    OEspecifico.prototype.getButtonSaveSelector = function() {
        return '#registrar-oespecifico';
    }
    OEspecifico.prototype.getItemDescription = function() {
        return 'el objetivo específico';
    }
    OEspecifico.prototype.getInstanceValidator = function() {
        return new OEspecificoValidator();
    }
    OEspecifico.prototype.getDiscriminator = function() {
        return 'OBJETIVO_ESPECIFICO';
    }
    OEspecifico.prototype.getTableTemplate = function() {
        return oespecificoTableTemplate;
    }
    OEspecifico.prototype.getCurButtonMessage = function() {
        return 'AÑADIR OBJETIVO ESPECÍFICO';
    }

    OEspecifico.prototype.showGrid = function(response, context) {
        // this.dataTable.showGrid(response, oestrategicoConfig.getConfig(context))
    }

    OEspecifico.prototype.attachTemplateParamsByLevel = function(genericParam, context) {
        genericParam.gerencias = context.getGerencias();
    }
    OEspecifico.prototype.customActionsBeforeHtmlTemplate = function(curItem, context) {
    }
    OEspecifico.prototype.customActionsAfterHtmlTemplate = function(curItem, context) {
        /* call parent */
        modCamposDinamicos.iniciarChosen({
            selector: '#select-gerencia',
            search_contains: true,
            placeholder: 'Elige una gerencia'
        });
        break;
    }
    OEspecifico.prototype.getItemEstrategicoModel = function(options, context) {
        /* call parent */
        this.itemEstrategicoModel.set('descripcionItem', $('#descripcion-oespecifico').val().trim());
        this.itemEstrategicoModel.set('dependenciaOrganizacional', {
            idDependenciaOrganizacional : $('#select-gerencia').val()
        });
        return this.itemEstrategicoModel;
    }
    return OEspecifico;
});