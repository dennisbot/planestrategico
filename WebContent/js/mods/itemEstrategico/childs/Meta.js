define(
    [
        'underscore',
        '../ItemEstrategico',
        'models/ItemEstrategicoModel',
        'config/itemEstrategico/metaConfig',

        'mods/validaciones/ItemEstrategico/MetaValidator',

        'text!templates/gestion/buttonSaveTemplate.html',
        'text!templates/gestion/niveles/metas/saveMetaTemplate.html',
        'text!templates/gestion/niveles/metas/metaTableTemplate.html',
    ], function(
        _,
        ItemEstrategico,
        ItemEstrategicoModel,
        metaConfig,

        MetaValidator,

        buttonSaveTemplate,
        saveMetaTemplate,
        metaTableTemplate
    ) {

    function Meta() {
        ItemEstrategico.call(this);
        /* init additional properties */
        this.level = 5;
    }
    Meta.prototype = Object.create(ItemEstrategico.prototype);

    Meta.prototype.getCompiledSaveItemEstTemplate = function() {
        return _.template(saveMetaTemplate, {
        });
    }
    Meta.prototype.getFocusableSelector = function() {
        return '#descripcion-meta';
    }
    Meta.prototype.getButtonSaveSelector = function() {
        return '#registrar-meta';
    }
    Meta.prototype.getItemDescription = function() {
        return 'la meta';
    }
    Meta.prototype.getInstanceValidator = function() {
        return new MetaValidator();
    }
    Meta.prototype.getDiscriminator = function() {
        return 'META';
    }
    Meta.prototype.getItemEstrategicoModel = function(options, context) {
        /* call parent */
        this.itemEstrategicoModel.set('descripcionItem', $('#descripcion-meta').val().trim());
        return this.itemEstrategicoModel;
    }
    Meta.prototype.getTableTemplate = function() {
       return metaTableTemplate;
    }
    Meta.prototype.getCurButtonMessage = function() {
        return 'AÑADIR META';
    }
    Meta.prototype.showGrid = function(response, context) {
        // this.dataTable.showGrid(response, oestrategicoConfig.getConfig(context))
    }
    Meta.prototype.attachTemplateParamsByLevel = function(genericParam, context) {
        genericParam.gerencias = context.getGerencias();
    }
    Meta.prototype.customActionsBeforeHtmlTemplate = function(curItem, context) {
    }
    Meta.prototype.customActionsAfterHtmlTemplate = function(curItem, context) {
        /* call parent */
    }
    return Meta;
});