define(
    [
        'underscore',
        '../ItemEstrategico',
        'models/ItemEstrategicoModel',
        'config/itemEstrategico/iniciativaConfig',

        'mods/validaciones/ItemEstrategico/IniciativaValidator',

        'text!templates/gestion/buttonSaveTemplate.html',
        'text!templates/gestion/niveles/iniciativas/saveIniciativaTemplate.html',
        'text!templates/gestion/niveles/iniciativas/iniciativaTableTemplate.html',

        'text!templates/gestion/niveles/iniciativas/periodoTemplate.html',
        'text!templates/gestion/niveles/iniciativas/selectPeriodoTemplate.html',
        'text!templates/gestion/niveles/iniciativas/subcategoriasIniciativaTemplate.html',

        'config/itemEstrategico/metricaConfig',
        'collections/MetricaCollection',
        'text!templates/kpi/metricaTableTemplate.html',

        'mods/modCamposDinamicos',
        'mods/modDataTablesModal',
        'mods/modUtils',
        'bootbox',
    ], function(
        _,
        ItemEstrategico,
        ItemEstrategicoModel,
        iniciativaConfig,

        IniciativaValidator,

        buttonSaveTemplate,
        saveIniciativaTemplate,
        iniciativaTableTemplate,

        periodoTemplate,
        selectPeriodoTemplate,
        subcategoriasIniciativaTemplate,

        metricaConfig,
        MetricaCollection,
        metricaTableTemplate,

        modCamposDinamicos,
        modDataTablesModal,
        modUtils,
        bootbox
    ) {

    function Iniciativa() {
        ItemEstrategico.call(this);
        /* init additional properties */
        this.level = 4;
    }
    Iniciativa.prototype = Object.create(ItemEstrategico.prototype);

    Iniciativa.prototype.getCompiledSaveItemEstTemplate = function() {
        return _.template(saveIniciativaTemplate, {
        });
    }
    Iniciativa.prototype.getFocusableSelector = function() {
        return '#descripcion-Iniciativa';
    }
    Iniciativa.prototype.getButtonSaveSelector = function() {
        return '#registrar-iniciativa';
    }
    Iniciativa.prototype.getItemDescription = function() {
        return 'la iniciativa';
    }
    Iniciativa.prototype.getInstanceValidator = function() {
        return new IniciativaValidator();
    }
    Iniciativa.prototype.getDiscriminator = function() {
        return 'INICIATIVA';
    }
    Iniciativa.prototype.getTableTemplate = function() {
        return iniciativaTableTemplate;
    }
    Iniciativa.prototype.getCurButtonMessage = function() {
        return 'AÑADIR INICIATIVA';
    }
    Iniciativa.prototype.showGrid = function(response, context) {
        // this.dataTable.showGrid(response, oestrategicoConfig.getConfig(context))
    }
    Iniciativa.prototype.getItemEstrategicoModel = function(options, context) {
        /* call parent */
        this.itemEstrategicoModel.set('descripcionItem', $('#descripcion-iniciativa').val().trim());
        this.itemEstrategicoModel.set('dependenciaOrganizacional', {
            idDependenciaOrganizacional : $('#select-superintendencia').val()
        });
        this.itemEstrategicoModel.set('categoria', { idCategoria : $('[name="subcategoria"]:checked').val() });
        this.itemEstrategicoModel.set('metrica', { idMetrica : $('#idMetrica').val().trim() } );
        this.itemEstrategicoModel.set('tolerancia', $('#tolerancia').val().trim());
        this.itemEstrategicoModel.set('comentarios', $('#comentarios-iniciativa').val().trim());
        this.itemEstrategicoModel.set('itemEstrategicos', this._getMetas(options));
        return this.itemEstrategicoModel;
    }

    Iniciativa.prototype._getMetas = function(options) {
        // console.log('entra en _getmetas');
        var self = this;
        var metas = [];

        $('#periodos-container>div.form-group').each(function(index, element) {

            var $element = $(element);

            itemEstrategicoModel = new ItemEstrategicoModel({
                idItemEstrategico : $element.attr('data-item-estrategico-id'),
                discriminator: self.getDiscriminator(5),
                descripcionNivel: {
                    idNivel: 5,
                },
                planEstrategico : {
                    idPlanEstrategico : parametros.curPlanEstrategico.idPlanEstrategico,
                },
                // idParentItem: options.idParentItem,
                valorPlaneado: $element.find('input').val(),
                periodo : {
                    idPeriodo : $element.find('.fa-remove').attr('data-periodo-id'),
                },
            });
            metas.push(itemEstrategicoModel);

        });
        // console.log('en _getMetas');
        // console.log('metas: ', metas);
        return metas;
    }


    Iniciativa.prototype.attachTemplateParamsByLevel = function(genericParam, context) {
        genericParam.superintendencias = context.getSuperintendencias();
        genericParam.curYear = parametros.curPlanEstrategico.anio;
        genericParam.periodos = context.periodos.toJSON();
        genericParam.categorias = context.categorias.toJSON();
    }
    Iniciativa.prototype.customActionsBeforeHtmlTemplate = function(curItem, context) {
        if (curItem && curItem.itemEstrategicos) {
            curItem.itemEstrategicos = $(curItem.itemEstrategicos).sort(function(a, b) {
                return a.periodo.mes > b.periodo.mes
            })
        }
    }
    Iniciativa.prototype.customActionsAfterHtmlTemplate = function(curItem, context) {
        /* call parent */
        this.context = context;
        this.curPeriods = [];
        this.curItem = curItem;
        this.categorias = context.categorias.toJSON();

        if (curItem) {
            this.curPeriods = _.pluck(_.pluck(curItem.itemEstrategicos, 'periodo'), 'idPeriodo');
            this._loadSubcategorias(curItem.categoria);
        }

        this.periodos = context.periodos.toJSON();

        modCamposDinamicos.iniciarChosen({
            selector: '#select-superintendencia',
            search_contains: true,
            placeholder: 'Elige una superintendencia'
        });
        modCamposDinamicos.iniciarChosen({
            selector: '#select-categoria',
            search_contains: true,
            placeholder: 'Elige una categoria'
        });

        this._attachRemoveHandler();
        this._reloadSelectPeriodo();

        var self = this;
        $('#select-categoria').change(function() {
            var me = $(this);
            // var text = me.find(':selected').text();
            self._loadSubcategorias(me.val());
        });
        $('#select-metrica').click(function(e) {
            // console.log('entra a doSelectMetrica dd');
            e.preventDefault();
            self.metricas = new MetricaCollection();
            var complete = _.invoke([self.metricas], 'fetch');
            $.when.apply($, complete)
            .done(function() {
                var compiledTemplate = _.template(metricaTableTemplate, {});

                $('#modal-area-2').html(compiledTemplate);
                var config = metricaConfig.getConfig(context);
                modDataTablesModal.showGrid(self.metricas.toJSON(), config);
                $('#myModal2').modal().on('shown.bs.modal', function() {
                    $('#myModal2 input').focus();
                    modDataTablesModal.datatable.columns.adjust().draw();
                    config.$container.css('visibility', 'visible');
                }).on('hidden.bs.modal', function() {
                    $('#myModal').focus();
                    $('body').addClass('modal-open');
                });

            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        });
    }

    Iniciativa.prototype._reloadSelectPeriodo = function() {
        var diff = _.difference(_.pluck(this.periodos, 'idPeriodo'), this.curPeriods);
        var periodos = _.filter(this.periodos, function (periodo) {
            return _.find(diff, function(id) { return id == periodo.idPeriodo }) !== undefined;
        });
        $('#select-periodo-container').html(_.template(selectPeriodoTemplate, {
            periodos : periodos,
            curYear: parametros.curPlanEstrategico.anio,
        }));
        this._selectPeriodoChange();
    }
    Iniciativa.prototype._selectPeriodoChange =  function() {
        var self = this;
        modCamposDinamicos.iniciarChosen({
            selector: '#select-periodo',
            search_contains: true,
            placeholder: 'Elige un periodo'
         });

        $('#select-periodo').change(function() {
            var periodoId = parseInt($(this).val());

            var selectedPeriodo =  _.find(self.periodos, function(periodo) {
                    return periodo.idPeriodo == periodoId;
            })

            var itemEstrategicoTempId = modUtils.uniqueID();

            var compiledTemplate = _.template(periodoTemplate, {
                curYear: parametros.curPlanEstrategico.anio,
                selectedPeriodo : selectedPeriodo,
                itemEstrategicoTempId : itemEstrategicoTempId,
            })

            self.curPeriods.push(periodoId);
            self._reloadSelectPeriodo();

            $('#periodos-container').append(compiledTemplate);

            var divs = $('#periodos-container>div.form-group');
            divs.sort(function(a, b) {
                var pIdA = parseInt($(a).find('.fa-remove').attr('data-periodo-id'));
                var pIdB = parseInt($(b).find('.fa-remove').attr('data-periodo-id'));
                return pIdA > pIdB;
            });

            $('#periodos-container').html(divs);

            self._attachRemoveHandler();

            $('#item-estrategico-' + itemEstrategicoTempId + ' input').focus();
        })
    }

    Iniciativa.prototype._attachRemoveHandler = function() {
        var self = this;
        $('#periodos-container .fa-remove').click(function() {
            var $this = $(this);
            var periodoId = parseInt($this.attr('data-periodo-id'));
            var itemEstrategicoId = $this.attr('data-item-estrategico-id');

            if (modUtils.isNumeric(itemEstrategicoId)) {
                bootbox.confirm('<h4 class="text-center">' +
                '<strong>¿ESTÁS SEGURO DE QUITAR ESTE PERIODO?</strong></h4>',
                function(ok) {
                    if (!ok) return;
                    var itemEstrategicoModel = new ItemEstrategicoModel({ idItemEstrategico : itemEstrategicoId });
                    itemEstrategicoModel.destroy({
                        success: function(model, response, options) {
                            $.notify('se eliminó la meta exitosamente', 'success');

                            self.itemEstrategicos = _.filter(self.itemEstrategicos, function(itemEstrategico) {
                                return itemEstrategico.idItemEstrategico != itemEstrategicoId;
                            })

                            self.curPeriods = _.without(self.curPeriods, periodoId);
                            self._reloadSelectPeriodo();
                            $this.closest('.form-group').remove();

                            /* reload the datatable */
                            self.context.loadItemsByLevel(4);

                            console.log('response: ', response);
                        },
                        error: function() {
                            $.notify('ERROR conectandose con el servidor', 'error');
                            console.log('response: ', response);
                        },
                    })
                })
            }
            else {
                self.curPeriods = _.without(self.curPeriods, periodoId);
                self._reloadSelectPeriodo();
                $this.closest('.form-group').remove();
            }
        });
    }

    Iniciativa.prototype._loadSubcategorias = function(categoria) {
        var idCategoriaParent;
        if (typeof categoria == 'object')
            idCategoriaParent = categoria.idCategoriaParent;
        else
            idCategoriaParent = categoria;
        var pesos = _.sortBy(_.find(this.categorias, function(categoria) {
            return categoria.idCategoria == idCategoriaParent
        }).subcategorias, function(subcategoria) {
            return subcategoria.peso;
        });
        $('.subcategorias').html(_.template(subcategoriasIniciativaTemplate, {
            pesos : pesos,
            categoria: (typeof categoria == 'object') ? categoria : null,
        }));
    }

    return Iniciativa;
});