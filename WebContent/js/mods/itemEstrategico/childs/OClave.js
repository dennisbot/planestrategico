define(
    [
        'underscore',
        '../ItemEstrategico',
        'models/ItemEstrategicoModel',
        'config/itemEstrategico/oclaveConfig',

        'mods/validaciones/ItemEstrategico/OClaveValidator',

        'text!templates/gestion/buttonSaveTemplate.html',
        'text!templates/gestion/niveles/oclaves/saveOClaveTemplate.html',
        'text!templates/gestion/niveles/oclaves/oclaveTableTemplate.html',
    ], function(
        _,
        ItemEstrategico,
        ItemEstrategicoModel,
        oclaveConfig,

        OClaveValidator,

        buttonSaveTemplate,
        saveOClaveTemplate,
        oclaveTableTemplate
    ) {

    function OClave() {
        ItemEstrategico.call(this);
        /* init additional properties */
        this.level = 2;
    }
    OClave.prototype = Object.create(ItemEstrategico.prototype);
    OClave.prototype.getCompiledSaveItemEstTemplate = function() {
        return _.template(saveOClaveTemplate, {
        });
    }
    OClave.prototype.getFocusableSelector = function() {
        return '#descripcion-oclave';
    }
    OClave.prototype.getButtonSaveSelector = function() {
        return '#registrar-oclave';
    }
    OClave.prototype.getItemDescription = function() {
        return 'el objetivo clave';
    }
    OClave.prototype.getInstanceValidator = function() {
        return new OClaveValidator();
    }
    OClave.prototype.getDiscriminator = function() {
        return 'OBJETIVO_CLAVE';
    }
    OClave.prototype.getItemEstrategicoModel = function(options, context) {
        /* call parent */
        this.itemEstrategicoModel.set('descripcionItem', $('#descripcion-oclave').val().trim());
        this.itemEstrategicoModel.set('dependenciaOrganizacional', { idDependenciaOrganizacional : 1 });
        return this.itemEstrategicoModel;
    }
    OClave.prototype.getTableTemplate = function() {
        return oclaveTableTemplate;
    }
    OClave.prototype.getCurButtonMessage = function() {
        return 'AÑADIR OBJETIVO CLAVE';
    }
    OClave.prototype.showGrid = function(response, context) {
        // this.dataTable.showGrid(response, oestrategicoConfig.getConfig(context))
    }
    OClave.prototype.attachTemplateParamsByLevel = function(genericParam, context) {
    }
    OClave.prototype.customActionsBeforeHtmlTemplate = function(curItem, context) {
    }
    OClave.prototype.customActionsAfterHtmlTemplate = function(curItem, context) {
    }

    return OClave;
});