define(
    [
        'datatables',
        'datatables.net',
        'datatablesBootstrap',
        '../modUtils',
        '../modAjax',
        '../modCamposDinamicos',
        '../modLang',
    ],
    function(
        datatables,
        datatables_select,
        datatablesBootstrap,
        utils,
        modAjax,
        modCamposDinamicos,
        modLang
    ) {

    function DataTable () {
        this.config = null;
        this.detailRows = null;
        this.datatable = null;
    }
    DataTable.prototype.showGrid = function(response, config) {
        var self = this;
        this.config = config;
        this.detailRows = [];
        this.datatable = config.$container.DataTable({
            // destroy: true,
            rowId: config.rowId,
            dom : "<'row'<'col-sm-4'l><'col-sm-4 add-item text-center'><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            data: response,
            language: modLang.getLanEs(),
            order: config.order,
            columns: config.columns,
            'bAutoWidth': true,
            'bAutoWidth': true,
            "columnDefs": config.columnDefs,
            'lengthMenu' : config.lengthMenu ? config.lengthMenu : [10, 25, 50, 100],
            'iDisplayLength' : config.iDisplayLength ? config.iDisplayLength : 10,
            select : config.select ? config.select : null
        });
        this.prepararEdiciones();
        $('.dataTables_paginate').off('click.datatables_click').on('click.datatables_click', function() {
            self.prepararEdiciones();
        })
        $('.dataTables_filter').off('change.datatables_filter').on('change.datatables_filter', function() {
            self.prepararEdiciones();
        })
        $('.dataTables_length').off('change.datatables_length').on('change.datatables_length', function() {
            self.prepararEdiciones();
        })

        this.datatable.columns.adjust().draw();

        if (this.config.hasDetails) {
            $(this.config.container + ' tbody').on( 'click', 'tr td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = self.datatable.row( tr );
                var idx = $.inArray( tr.attr('id'), self.detailRows );
                if ( row.child.isShown() ) {
                    tr.removeClass( 'details' );
                    row.child.hide();
                    // Remove from the 'open' array
                    self.detailRows.splice( idx, 1 );
                }
                else {
                    tr.addClass( 'details' );
                    row.child( self.config.formatDataTableDetails( row.data() ) ).show();
                    if (self.config.onChildRowRendered)
                        self.config.onChildRowRendered(row.data());
                    // Add to the 'open' array
                    if ( idx === -1 ) {
                        self.detailRows.push( tr.attr('id') );
                    }
                }
            } );
            // On each draw, loop over the `self.detailRows` array and show any child rows
            // this.datatable.on( 'draw', function () {
            //     $.each( self.detailRows, function ( i, id ) {
            //         $('#'+id+' td.details-control').trigger( 'click' );
            //     } );
            // } );
        }
    }

    DataTable.prototype.prepararEdiciones = function() {
        var self = this;
        $(this.config.container + ' a.obj-edit').off('click.doedit').on('click.doedit', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doEdit)
                self.config.doEdit(curObj);
        })


        $(this.config.container + ' a.obj-delete').off('click.dodelete').on('click.dodelete', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doDelete)
                self.config.doDelete(curObj);
        })

        $(this.config.container + ' a.obj-go').off('click.dogo').on('click.dogo', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doGo)
                self.config.doGo(curObj);
        })

        $(this.config.container + ' a.obj-select').off('click.doselect').on('click.doselect', function(e) {
            e.preventDefault();
            var curRow = $(this).closest('tr');
            var curObj = self.datatable.row(curRow).data();
            if (self.config.doSelect)
                self.config.doSelect(curObj);
        })

    }

    return DataTable;
});
