define([
    'AppRouter',
    'mods/modSecurity',
    'config/confRutas',
    'views/gestion/ManageProcesosView',
    'views/gestion/ManageLevelsView',
    'views/gestion/ManageUnidadMedidaView',
    'views/gestion/ManageTipoMetricaView',
    'views/gestion/ManageKpiView',
    'views/gestion/ManageCategoriaView',
    'views/gestion/JerarquiaView',
    'views/gestion/ControlIniciativasView',

    'views/reportes/GradoAvanceSuperintendenciasView',
    'views/reportes/GradoAvanceGerenciasView',
    'views/reportes/GradoAvanceVicepresidenciasView',

    'views/reportes/Top10SuperintendenciasView',
    'views/reportes/Top10GerenciasView',
    'views/reportes/Top10VicepresidenciasView',

    'views/admin/MiPerfilView',
    'views/admin/ManagePermisosView',
    'views/admin/AsignacionRolesView',

    'views/HomeView',
], function(
    AppRouter,
    modSecurity,
    confRutas,
    ManageProcesosView,
    ManageLevelsView,
    ManageUnidadMedidaView,
    ManageTipoMetricaView,
    ManageKpiView,
    ManageCategoriaView,
    JerarquiaView,
    ControlIniciativasView,

    GradoAvanceSuperintendenciasView,
    GradoAvanceGerenciasView,
    GradoAvanceVicepresidenciasView,

    Top10SuperintendenciasView,
    Top10GerenciasView,
    Top10VicepresidenciasView,

    MiPerfilView,
    ManagePermisosView,
    AsignacionRolesView,

    HomeView
    ) {
    return {
        appRouter : null,
        rutas : confRutas.rutas,
        initialize : function() {
            // console.log('parametros: ', parametros);
            // console.log('parametros.empleado.cargo.dependenciaOrganizacional.idNivelJerarquico: ',
                // parametros.empleado.cargo.dependenciaOrganizacional.idNivelJerarquico);
            // console.log('this.rutas: ', this.rutas);
            // console.log('como objeto:');
            var self = this;
            /* mappings :) */
            // var user = parametros.empleado;
            this.appRouter = new AppRouter;
            this.rutas.forEach(function(ruta) {
                if (modSecurity.canView('#' + ruta.urlDescription)
                    || ruta.urlDescription == '' || ruta.optional)
                    self.appRouter.route(ruta.urlDescription, ruta.showMethod, self[ruta.showMethod]);
            })
            Backbone.history.start();
        },
        showManageReports : function() {
            /* con esta forma no se guarda el enlace originador en el history back */
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
            /* con esta segunda forma se guarda el history back, no quiero eso */
            // window.location.href = DOMAIN + 'GestionPersonal/';
        },
        showManagePersonal : function() {
            /* con esta forma no se guarda el enlace originador en el history back */
            window.location.replace(DOMAIN + 'GestionPersonal/');
            /* con esta segunda forma se guarda el history back, no quiero eso */
            // window.location.href = DOMAIN + 'GestionPersonal/';
        },
        showManageProcesos : function() {
            ViewManager.handle(new ManageProcesosView({}));
            this.selectItem('#manage-procesos');
            // this.appRouter.navigate('/', true);
        },
        showManageLevels : function() {
            ViewManager.handle(new ManageLevelsView({}));
            this.selectItem('#manage-levels');
            // this.appRouter.navigate('/', true);
        },
        showManageUnidadMedida : function() {
            ViewManager.handle(new ManageUnidadMedidaView({}));
            this.selectItem('#manage-unidad-medida');
            // this.appRouter.navigate('/', true);
        },
        showManageTipoMetrica : function() {
            ViewManager.handle(new ManageTipoMetricaView({}));
            this.selectItem('#manage-tipo-kpi');
            // this.appRouter.navigate('/', true);
        },
        showManageMetrica : function() {
            ViewManager.handle(new ManageKpiView({el : '#container'}));
            this.selectItem('#manage-kpi');
            // this.appRouter.navigate('/', true);
        },
        showManageCategoria : function() {
            ViewManager.handle(new ManageCategoriaView({}));
            this.selectItem('#manage-categoria');
            // this.appRouter.navigate('/', true);
        },
        showObjetivosEstrategicos : function() {
            ViewManager.handle(new JerarquiaView({level : 1, appRouter : this}));
            this.selectItem('#objetivos-estrategicos');
        },
        showObjetivosClave : function() {
            ViewManager.handle(new JerarquiaView({level: 2, appRouter : this}));
            this.selectItem('#objetivos-clave');
        },
        showObjetivosEspecificos : function() {
            ViewManager.handle(new JerarquiaView({level: 3, appRouter : this}));
            this.selectItem('#objetivos-especificos');
        },
        showIniciativas : function() {
            ViewManager.handle(new JerarquiaView({level: 4, appRouter : this}));
            this.selectItem('#iniciativas');
        },
        showControlIniciativas : function() {
            ViewManager.handle(new ControlIniciativasView());
            this.selectItem('#control-iniciativas');
        },
        showGradoAvanceSuperintendencias : function() {
            // ViewManager.handle(new GradoAvanceSuperintendenciasView());
            // this.selectItem('#grado-avance-superintendencias');
            /* con esta forma no se guarda el enlace originador en el history back */
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
            /* con esta segunda forma se guarda el history back, no quiero eso */
            // window.location.href = DOMAIN + 'GestionPersonal/';
        },
        showGradoAvanceGerencias : function() {
            // ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoAvanceVicepresidencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoEjecucionSuperintendencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoEjecucionGerencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoEjecucionVicepresidencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoEficaciaSuperintendencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoEficaciaGerencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showGradoEficaciaVicepresidencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showBPINSuperintendencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showBPINGerencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },
        showBPINVicepresidencias : function() {
        	// ViewManager.handle(new GradoAvanceGerenciasView());
        	// para que hugo cambie, acá tienes que poner la ruta correcta del reporte de reporting services
            window.location.replace(REPORT_SERVER + 'ReportServer?%2fReportes_PlanEstrategico&rs:Command=ListChildren/');
        },        
        showMiPerfil : function() {
            var miPerfilView = new MiPerfilView();
            ViewManager.handle(miPerfilView);
            miPerfilView.render();
            this.selectItem('#mi-perfil');
        },
        showManagePermisos : function() {
            var misPermisosView = new ManagePermisosView();
            ViewManager.handle(misPermisosView);
            this.selectItem('#manage-permisos');
        },
        showAsignacionRoles : function() {
            var asignacionRolesView = new AsignacionRolesView();
            ViewManager.handle(asignacionRolesView);
            this.selectItem('#asignacion-roles');
        },
        homeAction : function(actions) {
            // console.log('actions: ', actions);
            ViewManager.handle(new HomeView());
            // We have no matching route, lets display the home page
            // this.selectItem('#' + actions);
            // this.appRouter.navigate('/', true);
        }
    };
});