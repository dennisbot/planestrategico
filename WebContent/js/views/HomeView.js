define(
    [
        'underscore',
        'backbone',

        'mods/modUtils',

        'text!templates/homeTemplate.html',
    ]
    , function(
        _,
        Backbone,

        modUtils,

        homeTemplate
    ) {
    var HomeView = Backbone.View.extend({
        el: '#container',
        initialize: function() {
            $('#custom-css').empty();
            this.render();
        },
        render: function() {
            /* procesamos notificaciones si hubiera */
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(homeTemplate, {
                curyear: parametros.curPlanEstrategico.anio,
            });
            this.$el.html(compiledTemplate);
        }
    })
    return HomeView;
})