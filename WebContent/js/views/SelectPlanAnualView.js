define(
    [
        'underscore',
        'backbone',
        '../mods/modAjax',

        '../mods/modUtils',
        '../mods/modCamposDinamicos',
        'editinplace',

        '../mods/validaciones/modValidator',
        '../mods/validaciones/PlanEstrategico/PlanEstrategicoValidator',

        '../models/PlanEstrategicoModel',

        'text!../../templates/planAnual/selectPlanTemplate.html',
        'text!../../templates/planAnual/newPlanTemplate.html',
        'text!../../templates/planAnual/showErrorMessage.html',
    ]
    , function(
        _,
        Backbone,
        modAjax,

        modUtils,
        modCamposDinamicos,
        editinplace,

        modValidator,
        PlanEstrategicoValidator,

        PlanEstrategicoModel,

        selectPlanTemplate,
        newPlanTemplate,
        showErrorMessage
    ) {
    var SelectPlanAnualView = Backbone.View.extend({
        el: '#form-plan-anual',
        initialize: function(options) {
            $('#custom-css').empty();
            modUtils.extractToContext(options);
            var self = this;
            this.fetchPlanes();
        },
        fetchPlanes: function() {
            var self = this;
            (new PlanEstrategicoModel()).fetch({
                // async: false,
                success: function(model, response, options) {
                    // console.log('response: ', response);
                    self.planes = _.sortBy(response, function(plan) { return plan.anio } );
                    self.render();
                },
                error: function(model, response, options) {
                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('error');
                    console.log('response: ', response);
                },
            })
        },
        iniciarEditInplaceDescripcionPlanEstrategico : function () {
            var self = this;
            modCamposDinamicos.iniciarEditInPlace({
                args : {
                    error_sink: function (idOfEditor, errorString) {
                        console.log('hubo un error: ', idOfEditor);
                        console.log(errorString);
                    },
                    callback: function (element_id, update_value, original_value, settings) {
                        // console.log('settings.default_text: ', settings.default_text);
                        // console.log('element_id: ', element_id);
                        // console.log('update_value: ', update_value);
                        // console.log('original_value: ', original_value);
                        // console.log('meta.idItemEstrategico: ', meta.idItemEstrategico);

                        console.log('settings.default_text: ', settings.default_text);
                        console.log('update_value: ', update_value);

                        if (update_value == settings.default_text) return original_value;

                        var idPlanEstrategico = $('#select-year').find('option:selected').attr('data-plan-id');
                        var planEstrategicoModel = new PlanEstrategicoModel({
                            idPlanEstrategico : idPlanEstrategico,
                        });

                        planEstrategicoModel.fetch({
                            async: false,
                            success : function (model, response, options) {
                                planEstrategicoModel.set('nombrePlan', update_value)
                                planEstrategicoModel.save({}, {
                                    async: false,
                                    success: function (model, response, options) {
                                        update_value = update_value.toUpperCase();
                                        var curPlan = planEstrategicoModel.toJSON();
                                        // console.log('curPlan: ', curPlan);
                                        var myPlan = _.find(self.planes, function(plan) {
                                            console.log('plan.idPlanEstrategico: ', plan.idPlanEstrategico);
                                            return plan.idPlanEstrategico == curPlan.idPlanEstrategico
                                        })
                                        console.log('myPlan: ', myPlan);
                                        myPlan.nombrePlan = curPlan.nombrePlan;

                                        self.$('#nombre_plan').notify('guardado!', {
                                            className : 'success',
                                            position: 'top right',
                                        });
                                    },
                                    error : function (model, response, options) {
                                        update_value = original_value;
                                        self.$('#nombre_plan').notify(
                                            'ERROR conectándose con el servidor', {
                                                className : 'error',
                                                position: 'top right',
                                        });
                                        console.log('response: ', response);
                                    }
                                })
                            }
                        })
                        return update_value;
                    },
                    default_text: '(NINGUNO)',
                    value_required: true,
                    field_type: 'textarea',
                    textarea_rows : 2,
                    inputNameAndClass : 'name="inplace_value" ' +
                    'style="margin: 0;padding: 5px;text-transform:uppercase" class="form-control inplace_field" '
                },
                selector : '#nombre_plan',
                inputInplaceFieldSelector : 'textarea.inplace_field',
                onfocus: function() {
                    $(this).closest('p').css({padding: 0});
                    // $(this).css({
                    //     'border-style': 'none',
                    //     'border-width': '0',
                    //     // 'background-color': '#f9f9f9',
                    //     'background-color': '#ffffdd',
                    //     'box-shadow': 'inset 0 1px 2px rgba(0,0,0,.3)',
                    //     '-webkit-box-shadow': 'inset 0 1px 2px rgba(0,0,0,.3)',
                    //     'padding': '7px 10px',
                    //     'margin': '0',
                    // });
                },
                onblur: function() {
                    $(this).closest('p').css({
                        'padding-top': '7px',
                        'padding-bottom': '7px'
                    });
                }
            })
        },
        render: function() {
            // console.log('this.planes: ', this.planes);
            /* procesamos notificaciones si hubiera */
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(selectPlanTemplate, {
                planes : this.planes,
            });
            this.$el.html(compiledTemplate);
            this.iniciarEditInplaceDescripcionPlanEstrategico();
            this.startCamposDinamicos();
            /* comentar esto para que no sea automático */
            // this.doSelectPlan();
        },
        events: {
            'click .add-new-plan': 'doAddNewPlan',
            'click .button-select-plan': 'doSelectPlan',
            'change #select-year': 'doChangeYear',
        },
        doChangeYear: function(e) {
            var curSelect = $(e.target);
            var curYear = curSelect.val();
            var curPlan = _.find(this.planes,
                                function(plan) {
                                    return plan.anio == curYear
                            });
            this.$('#nombre_plan').text(curPlan.nombrePlan);
        },
        doAddNewPlan: function() {

            var self = this;
            var compiledTemplate = _.template(newPlanTemplate, {
                years: _.difference(modUtils.getThreeYearsCurMid(), _.pluck(this.planes, 'anio')),
            });
            $('#modal-area').html(compiledTemplate);
            this.startCamposDinamicos('#select-anio');
            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#select-anio').trigger('chosen:activate')
            });
            $('#modal-area #registrar-plan').click(function() {
                var curPlan = new PlanEstrategicoModel({
                    anio : $('#select-anio').val(),
                    nombrePlan: $('#nombre-plan').val(),
                });

                var hasMessages = modValidator.validar(new PlanEstrategicoValidator(), curPlan.toJSON());

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $('#myModal #registrar-plan').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    /* está validado entonces guardamos los cambios */
                    curPlan.save({}, {
                        success: function (model, response, options) {
                            console.log('despues de guardar');
                            console.log('curPlan.toJSON(): ', curPlan.toJSON());
                            console.log('recargamos los datos');
                            $.notify('se ha creado el plan anual exitosamente', 'success');
                            self.fetchPlanes();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doSelectPlan: function(e) {
            var year = $('#select-year').val();
            if (year) {
                // $('#show-error-message').addClass('hidden');
                modAjax.callAJAX({
                    url: 'rest/utils/setSelectedPlanEstrategico/' + year,
                    callback: function(response) {
                        console.log('response: ', response);
                        // return false;
                        window.location = BASE_URL +  "home";
                    },
                    onerror: function(response) {
                        console.log('response: ', response);
                    }
                });
            }
            else {
                this.$('#show-error-message').html(_.template(showErrorMessage, {}));
            }
            return false;
        },
        startCamposDinamicos: function(selector) {
            modCamposDinamicos.iniciarChosen({
                selector: selector ? selector : '#select-year',
                search_contains: true,
                placeholder: 'Elige Año'
            });
            selector ? $(selector).trigger('chosen:activate')
            : $('#select-year').trigger('chosen:activate');
        }
    })
    return SelectPlanAnualView;
})