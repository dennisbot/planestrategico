define([
    'underscore',
    'backbone',
    'collections/RolCollection',
    'collections/PEmpleadoCollection',

    'mods/modAjax',
    'mods/modSecurity',
    'mods/modCamposDinamicos',

    'text!templates/admin/partials/rolesTemplate.html',
    'text!templates/admin/asignarRolesTemplate.html',
    /* para los partials */
    'text!templates/admin/partials/vistasTemplate.html',
    'text!templates/admin/partials/permisosTemplate.html',
], function(
    _,
    Backbone,
    RolCollection,
    PEmpleadoCollection,

    modAjax,
    modSecurity,
    modCamposDinamicos,

    rolesTemplate,
    asignarRolesTemplate,

    vistasTemplate,
    permisosTemplate
    ) {

    var AsignacionRolesView = Backbone.View.extend({
        el : '#container',
        initialize : function() {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            var self = this;
            this.empRolList = [];
            this.roles = new RolCollection();
            this.pempleados = new PEmpleadoCollection();
            var complete = _.invoke ([
                this.pempleados,
                this.roles
            ], 'fetch');
            $.when.apply($, complete)
            .done(function() {
                self.pempleados = self.pempleados.toJSON()
                self.roles = self.roles.toJSON()
                // console.log('self.roles: ', self.roles);
                // console.log('self.pempleados: ', self.pempleados);

                self.roles = _.filter(self.roles, function(rol) {
                    return rol.abreviatura != 'SUPER_ADMIN';
                })
                /* finalmente mostramos */
                self.render();
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            })
            this.initPermisos();
        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .guardar-roles'] = 'doGuardarRoles';
        },
        events : {
            // 'click .guardar-roles' : 'doGuardarRoles',
            'click .item-tick' : 'doTick',
            'change #select-empleado' : 'doChangeRenderRoles',
        },
        doGuardarRoles : function(e) {
            var self = this;
            $('#roles-loading').show();
            $(this).addClass('disabled');

            // console.log('this.empRolList: ', this.empRolList);
            modAjax.callAJAXPostJSON({
                url : 'rest/empleado/batch',
                type : 'POST',
                jsonVars : this.empRolList,
                callback : function(response) {
                    $('#roles-loading').hide();
                    $(this).removeClass('disabled');

                    $.notify('Los cambios se realizaron exitosamente', 'success');
                    // console.log('response: ', response);
                },
                onerror : function(response) {
                    $('#roles-loading').hide();
                    $(this).removeClass('disabled');
                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('response: ', response);
                }
            })
        },
        doChangeRenderRoles : function(e) {
            var self = this, curIdRol;

            this.curIdEmpleado = this.$(e.currentTarget).val();

            var curEmpRol = _.find(this.empRolList, function(empRol) {
                return empRol.idEmpleado == self.curIdEmpleado
            })

            if (!curEmpRol) {
                curIdRol = _.find(this.pempleados, function(pempleado) {
                    return pempleado.idEmpleado == self.curIdEmpleado
                }).rol.idParametroDetalle;
                curEmpRol = {};
                curEmpRol.idEmpleado = self.curIdEmpleado;
                curEmpRol.idRol = curIdRol;
                this.empRolList.push(curEmpRol);
            }
            else curIdRol = curEmpRol.idRol;
            this.$('#roles-container').html(_.template(rolesTemplate, {
                roles : this.roles,
                curIdRol : curIdRol
            }));
            this.bindOnTickEvent();
        },
        bindOnTickEvent : function() {
            var self = this;
            this.$('#roles-container').find('.item-tick').click(function() {
                self.clearYellowBackground();
                var curIdRol = parseInt($(this).attr('id'));
                var curEmpRol = _.find(self.empRolList, function(empRol) {
                    return empRol.idEmpleado == self.curIdEmpleado
                })
                curEmpRol.idRol = curIdRol;
                $(this).addClass('do-tick');
            })
        },
        clearYellowBackground : function(parentContainerSelector, $curDiv) {
            this.$('#roles-container').find('.item-tick').removeClass('do-tick');
        },
        template : _.template(asignarRolesTemplate),
        render : function() {
            // console.log('this.template(): ', this.template());
            this.$el.html(this.template({
                pempleados : this.pempleados,
                roles : this.roles,
                modSecurity : modSecurity,
                curUrlDescripcion : this.curUrlDescripcion,
            }));
            modCamposDinamicos.iniciarChosen({
                selector: '#select-empleado',
                search_contains: true,
                placeholder: 'Elige un empleado'
            });
            this.$('#select-empleado').trigger('chosen:activate');
        }
    })

    return AsignacionRolesView;
});