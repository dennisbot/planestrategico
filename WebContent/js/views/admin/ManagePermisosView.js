define([
    'underscore',
    'backbone',
    'collections/RolCollection',
    'collections/PermisoCollection',
    'collections/VistaCollection',
    'collections/RolVistaPermisoCollection',

    'mods/modAjax',
    'mods/modSecurity',

    'text!templates/admin/managePermisosTemplate.html',
    /* para los partials */
    'text!templates/admin/partials/vistasTemplate.html',
    'text!templates/admin/partials/permisosTemplate.html',
], function(
    _,
    Backbone,
    RolCollection,
    PermisoCollection,
    VistaCollection,
    RolVistaPermisoCollection,

    modAjax,
    modSecurity,

    managePermisosTemplate,

    vistasTemplate,
    permisosTemplate
    ) {

    var ManagePermisosView = Backbone.View.extend({
        el : '#container',
        initialize : function() {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            var self = this;
            this.roles = new RolCollection();
            this.permisos = new PermisoCollection();
            this.vistas = new VistaCollection();
            this.rolVistaPermisos = new RolVistaPermisoCollection();
            var complete = _.invoke ([
                this.roles,
                this.permisos,
                this.vistas,
                this.rolVistaPermisos
            ], 'fetch');
            $.when.apply($, complete)
            .done(function() {
                self.rolVistaPermisos = self.rolVistaPermisos.toJSON()
                self.roles = self.roles.toJSON()
                self.permisos = self.permisos.toJSON()
                self.vistas = self.vistas.toJSON()
                self.roles = _.filter(self.roles, function(rol) {
                    return rol.abreviatura != 'SUPER_ADMIN';
                })
                /* finalmente mostramos */
                self.render();
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            })
            this.initPermisos();
        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .guardar-permisos'] = 'doGuardarPermisos';
        },
        events : {
            // 'click .guardar-permisos' : 'doGuardarPermisos',
            'click .item-tick' : 'doTick',
        },
        doGuardarPermisos : function(e) {
            $('#permisos-loading').show();
            $(this).addClass('disabled');

            // console.log('guardar permisos was clicked!');
            // console.log('this.rolVistaPermisos: ', this.rolVistaPermisos);
            // console.log('se queda acá, no guarda');
            // return;
            modAjax.callAJAXPostJSON({
                url : 'rest/rolVistaPermiso/batch',
                type : 'POST',
                jsonVars : this.rolVistaPermisos,
                callback : function(response) {
                    $('#permisos-loading').hide();
                    $(this).removeClass('disabled');

                    $.notify('Los cambios se realizaron exitosamente', 'success');
                    // console.log('response: ', response);
                },
                onerror : function(response) {
                    $('#permisos-loading').hide();
                    $(this).removeClass('disabled');

                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('response: ', response);
                }
            })
        },
        renderVistas : function() {
            var self = this;
            // console.log('this.rolVistaPermisos: ', this.rolVistaPermisos);
            /* seleccionar todos los # en base al rol seleccionado */
            var misVistas = _.chain(this.rolVistaPermisos)
                .filter(function(rvp) {
                    return rvp.idRol == self.idRol
                 })
                .map(function(rvpRol) {
                    return rvpRol.urlDescripcion
                }).value();
            var vistas = _.map(this.vistas, function(vista) {
                return {
                    urlDescripcion : vista.urlDescripcion,
                    tick : _.contains(misVistas, vista.urlDescripcion)
                }
            })
            /* restart the this.urlDescripcion value */
            this.urlDescripcion = null;
            if (misVistas.length) {
                this.urlDescripcion = misVistas[0];
            }
            /* we render anyways, maybe there is a focused item to be cleared */
            this.renderPermisos();
            this.$('#vistas-container').html(_.template(vistasTemplate, {
                vistas : vistas,
                idToFocus : this.urlDescripcion
            }));
        },
        renderPermisos : function() {
            var self = this;
            var misPermisos = _.chain(this.rolVistaPermisos)
                    .filter(function(rvp) {
                        return rvp.idRol == self.idRol &&
                            rvp.urlDescripcion == self.urlDescripcion
                    })
                    .map(function(rvpUrlDescripcion) {
                        return rvpUrlDescripcion.idPermiso
                    }).value();

           var permisos = _.map(this.permisos, function(permiso) {
                return {
                    idPermiso : permiso.idParametroDetalle,
                    nombre : permiso.nombre,
                    tick : _.contains(misPermisos, permiso.idParametroDetalle)
                }
            })
            this.$('#permisos-container').html(_.template(permisosTemplate, {
                permisos : permisos
            }))
            // this.$('#permisos-container').html('')
        },
        doTick : function(e) {
            var $curDiv = this.$(e.currentTarget);

            var panel = $curDiv.attr('data-panel');
            // console.log('panel: ', panel);
            switch(panel) {
                case 'roles':
                    this.idRol = parseInt($curDiv.attr('data-rol-id'));
                    this.renderVistas();
                    this.clearYellowBackground('#roles-container', $curDiv);
                    // console.log('this.idRol: ', this.idRol);
                    break;
                case 'vistas':
                    this.urlDescripcion = $curDiv.html();
                    this.clearYellowBackground('#vistas-container', $curDiv);
                    this.renderPermisos();
                    // this.showCurIDs();
                    break;
                case 'permisos':
                    this.idPermiso = parseInt($curDiv.attr('data-permiso-id'));
                    $curDiv.toggleClass('do-tick');
                    if ($curDiv.hasClass('do-tick'))
                        this.addRolVistaPermisoItem();
                    else
                        this.removeRolVistaPermisoItem();
                    this.toggleVistaParent();
                    break;
            }
            if (panel != 'permisos')
                $curDiv.addClass('item-tick-clicked');
        },
        toggleVistaParent : function() {
            if (this.$('#permisos-container').find('.item-tick.do-tick').size() > 0)
                this.$(this.urlDescripcion).addClass('do-tick');
            else
                this.$(this.urlDescripcion).removeClass('do-tick');
        },
        showCurIDs : function() {
            console.log('this.idRol: ', this.idRol);
            console.log('this.urlDescripcion: ', this.urlDescripcion);
            console.log('this.idPermiso: ', this.idPermiso);
        },
        clearYellowBackground : function(parentContainerSelector, $curDiv) {
            $curDiv.closest(parentContainerSelector).find('.item-tick')
            .removeClass('item-tick-clicked');
        },
        addRolVistaPermisoItem : function() {
            var self = this;
            var rvp = _.find(this.rolVistaPermisos, function(rvp) {
                return self.idRol == rvp.idRol &&
                self.urlDescripcion == rvp.urlDescripcion &&
                self.idPermiso == rvp.idPermiso
            });
            if (!rvp) {
                this.rolVistaPermisos.push({
                    idRol : this.idRol,
                    urlDescripcion : this.urlDescripcion,
                    idPermiso : this.idPermiso
                });
            }
        },
        removeRolVistaPermisoItem : function() {
            var self = this;
            var rvp = _.find(this.rolVistaPermisos, function(rvp) {
                return self.idRol == rvp.idRol &&
                self.urlDescripcion == rvp.urlDescripcion &&
                self.idPermiso == rvp.idPermiso
            });
            if (rvp) {
                // this.showCurIDs();
                this.rolVistaPermisos = _.filter(this.rolVistaPermisos, function(rvp) {
                    return  self.idRol != rvp.idRol ||
                    self.urlDescripcion != rvp.urlDescripcion ||
                    self.idPermiso != rvp.idPermiso
                })
                // console.log('this.rolVistaPermisos: ', this.rolVistaPermisos);
            }
        },
        template : _.template(managePermisosTemplate),
        render : function() {
            // console.log('this.roles: ', this.roles);
            // console.log('this.vistas: ', this.vistas);
            // console.log('this.permisos: ', this.permisos);
            // console.log('this.rolVistaPermisos: ', this.rolVistaPermisos);

            var tpl = this.template({
                roles : this.roles,
                vistas : this.vistas,
                permisos : this.permisos,
                modSecurity : modSecurity,
                curUrlDescripcion : this.curUrlDescripcion,
            });
            this.$el.html(tpl);
            /* trigger the first role itme */
            this.$('#roles-container .item-tick').first().click();
        }
    })

    return ManagePermisosView;
});