define([
    'underscore',
    'backbone',
    'mods/DataTable/DataTable',
    'mods/modSecurity',
    'text!templates/kpi/modal/filtroMetricaTableTableTemplate.html',
    'text!templates/kpi/dataTableKpisTemplate.html'
    ], function(
        _,
        Backbone,
        DataTable,
        modSecurity,
        filtroMetricaTableTableTemplate,
        dataTableKpisTemplate
    ) {

    var SelectKpiView = Backbone.View.extend({
        el : '#modal-area-2',
        initialize : function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            modUtils.extractToContext(options, this);
        },
        template : _.template(filtroMetricaTableTableTemplate),
        render : function() {
            // console.log('this.procesos.toJSON(): ', this.procesos.toJSON());
            var self = this;
            modUtils.processCustomMessages();
            this.$el.html(manageMetricaTableTemplate);
            this.prepareFields();
            this.showGrid(this.metricas.toJSON());
        },
        showGrid : function() {
            this.$('#kpis-container').html(_.template(dataTableKpisTemplate, {
                modSecurity : modSecurity,
                curUrlDescripcion : this.curUrlDescripcion
            }));
            this.datatable = new DataTable();
            this.datatable.showGrid(kpis, kpisConfig.getConfig(this));
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveMetricaTemplate, {}));
        }
    })

    return SelectKpiView;
});