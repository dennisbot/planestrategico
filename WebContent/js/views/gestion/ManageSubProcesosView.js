define([
        'underscore',
        'backbone',

        'mods/modUtils',
        'mods/DataTable/DataTable',
        'mods/validaciones/modValidator',
        'mods/validaciones/ProcesoValidator',

        'bootbox',
        'mods/modSecurity',

        'models/ProcesoModel',

        'text!templates/proceso/subproceso/buttonSaveSubProcesoTemplate.html',
        'text!templates/proceso/subproceso/manageSubProcesosTemplate.html',
        'text!templates/proceso/subproceso/saveSubProcesoTemplate.html',
        'text!templates/proceso/subproceso/columnaOpsSubProcesosTemplate.html',
    ], function(
        _,
        Backbone,

        modUtils,
        DataTable,
        modValidator,
        ProcesoValidator,

        bootbox,
        modSecurity,

        ProcesoModel,

        buttonSaveSubProcesoTemplate,
        manageSubProcesosTemplate,
        saveSubProcesoTemplate,
        columnaOpsSubProcesosTemplate
    ) {

    var ManageSubProcesosView = Backbone.View.extend({
        initialize: function(options) {
            // console.log('options: ', options);
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            this.options = options || {};
            if (!options || !options.idProcesoParent)
                throw "idProcesoParentRequired Exception";
            this.idProcesoParent = options.idProcesoParent;
            this.fetchSubProcesos();
            this.initPermisos();
        },
        fetchSubProcesos: function() {
            var self = this;
            var subprocesos = new ProcesoModel();
            subprocesos.fetchSubProcesos({
                idProcesoParent : this.idProcesoParent,
                success: function(model, response, options) {
                    self.procesos = response;
                    self.render();
                },
                error: function (model, response, options) {
                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('response: ', response);
                }
            })

        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-subproceso'] = 'doSaveSubProceso';
        },
        events: {
        },
        doSaveSubProceso: function(e, curSubProceso) {
            var update = e ? false : true;
            var self = this;
            var compiledTemplate = _.template(saveSubProcesoTemplate, {
                curSubProceso: curSubProceso,
            });

            $('#modal-area').html(compiledTemplate);
            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#descripcion-subproceso').focus();
            });

            $('#modal-area #registrar-subproceso').click(function() {

                var curSubProceso = new ProcesoModel({
                    idProceso : $('#idProceso').val().trim(),
                    codProceso : $('#codProceso').val().trim(),
                    descripcion: $('#descripcion-subproceso').val(),
                    idProcesoParent : self.idProcesoParent
                });

                var hasMessages = modValidator.validar(
                    new ProcesoValidator(
                        update,
                        curSubProceso.urlRoot,
                        curSubProceso.get('idProceso')
                    ),
                    curSubProceso.toJSON()
                );

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $('#myModal #registrar-subproceso').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    if (!update)
                        curSubProceso.unset('idProceso');
                    /* está validado entonces guardamos los cambios */
                    curSubProceso.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' el subproceso exitosamente', 'success');
                            self.fetchSubProcesos();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curSubProceso) {
            this.doSaveSubProceso(null, curSubProceso);
            return false;
        },
        doDelete: function(curSubProceso) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTA DESCRIPCIÓN DEL subPROCESO?</strong></h4>',
            function(ok) {

                if (!ok) return;
                var procesoModel = new ProcesoModel({
                    idProceso : curSubProceso.idProceso,
                });
                procesoModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó la descripción del subproceso exitosamente', 'success');
                        self.fetchSubProcesos();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        render: function() {
            var self = this;
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(manageSubProcesosTemplate, {
                curUrlDescripcion : this.curUrlDescripcion,
                modSecurity : modSecurity
            });
            this.$el.html(compiledTemplate);

            var config = {
                container: '#tabla-subproceso',
                $container: this.$('#tabla-subproceso'),
                order : [[0, 'asc']],
                doEdit: _.bind(this.doEdit, this),
                doDelete: _.bind(this.doDelete, this),
                columns: [
                    {
                        data : 'descripcion',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                ],
            }
            if (modSecurity.canEdit(this.curUrlDescripcion) ||
                modSecurity.canDelete(this.curUrlDescripcion)) {
                config.columns.push({
                        data : 'idProceso',
                        width : '20%',
                        orderable : false,
                        sClass : (modSecurity.canEdit(this.curUrlDescripcion) &&
                        !modSecurity.canDelete(this.curUrlDescripcion)) ||
                        (!modSecurity.canEdit(this.curUrlDescripcion) &&
                        modSecurity.canDelete(this.curUrlDescripcion)) ? 'text-center' : ''
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsSubProcesosTemplate, {
                                modSecurity : modSecurity,
                                curUrlDescripcion : self.curUrlDescripcion,
                                idProceso : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                });
            }
            this.datatable = new DataTable();
            this.datatable.showGrid(this.procesos, config);
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveSubProcesoTemplate, {}));
        }
    });

    return ManageSubProcesosView;
});