define([
        'underscore',
        'backbone',

        'collections/CategoriaCollection',

        'mods/modUtils',
        'mods/modSecurity',
        'mods/modDataTables',
        'mods/validaciones/modValidator',
        'mods/validaciones/CategoriaValidator',

        'bootbox',
        'mods/modCamposDinamicos',

        'models/CategoriaModel',

        'text!templates/categoria/manageCategoriaTableTemplate.html',
        'text!templates/categoria/saveCategoriaTemplate.html',
        'text!templates/categoria/columnaOpsCategoriaTemplate.html',
        'text!templates/categoria/buttonSaveCategoriaTemplate.html',
        'text!templates/categoria/categoriaDetailTemplate.html',
    ], function(
        _,
        Backbone,

        CategoriaCollection,

        modUtils,
        modSecurity,
        modDataTables,
        modValidator,
        CategoriaValidator,

        bootbox,
        modCamposDinamicos,

        CategoriaModel,

        manageCategoriaTableTemplate,
        saveCategoriaTemplate,
        columnaOpsCategoriaTemplate,
        buttonSaveCategoriaTemplate,
        categoriaDetailTemplate

    ) {

    var ManageCategoriaView = Backbone.View.extend({
        el: '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options || {};
            this.loadCategorias();
            this.initPermisos();
        },
        loadCategorias: function() {
            var self = this;
            this.categorias = new CategoriaCollection();
            var complete = _.invoke([this.categorias], 'fetch');
            $.when.apply($, complete)
            .done(function() {
                self.render();
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-categoria'] = 'doSaveCategoria';
        },
        events: {
            'click #download-params': 'doDownloadParams',
        },
        doDownloadParams : function(e) {
            e.preventDefault();
            window.location.replace(BASE_URL + 'parametros');
        },
        doSaveCategoria: function(e, curCategoria) {
            var self = this;
            var compiledTemplate = _.template(saveCategoriaTemplate, {
                curCategoria: curCategoria,
                modUtils: modUtils,
                subcategorias: [
                    {
                        clase : 'col-sm-2 col-sm-offset-3',
                        significancia: 'BAJO',
                        placeholder: '10',
                        uniqueID : modUtils.uniqueID(),
                    },
                    {
                        clase : 'col-sm-2',
                        significancia: 'MEDIO',
                        placeholder: '12',
                        uniqueID : modUtils.uniqueID(),
                    },
                    {
                        clase : 'col-sm-2',
                        significancia: 'ALTO',
                        placeholder: '14',
                        uniqueID : modUtils.uniqueID(),
                    },
                ],
            });

            $('#modal-area').html(compiledTemplate);

            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#categoria-descripcion').focus();
            });

            $('#modal-area #registrar-categoria').click(function() {
                $('#categoria-loading').show();
                $(this).addClass('disabled');

                var curCategoria = new CategoriaModel({
                    descripcionCategoria: $('#categoria-descripcion').val(),
                    abreviaturaCategoria: $('#categoria-abreviatura').val(),
                    nivel: 1,
                    subcategorias : self.getSubcategoriasModels(),
                });

                var update = false;
                if ($('#id-categoria').val().trim() != "") {
                    curCategoria.set('idCategoria', $('#id-categoria').val().trim());
                    update = true;
                }

                var hasMessages = modValidator.validar(
                    new CategoriaValidator(curCategoria),
                    curCategoria.toJSON()
                );

                if (curCategoria.get('subcategorias') != null) {
                    curCategoria.set('subcategorias', _.map(curCategoria.get('subcategorias'), function(subcategoria) {
                        if (!modUtils.isNumeric(subcategoria.get('idCategoria'))) {
                            subcategoria.unset('idCategoria');
                        }
                        return subcategoria;
                    }));
                }

                var valid = (hasMessages == true) ? hasMessages : false;
                // console.log('valid: ', valid);
                // console.log('se detiene acá');
                // return false;
                if (!valid) {
                    $('#categoria-loading').hide();
                    $(this).removeClass('disabled');

                    $('#myModal #registrar-categoria').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    $(this).off('click');
                    var el = this;
                    /* está validado entonces guardamos los cambios */
                    curCategoria.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' la categoría exitosamente', 'success');
                            self.loadCategorias();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $('#categoria-loading').hide();
                            $(el).removeClass('disabled');

                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curCategoria) {
            this.doSaveCategoria(null, curCategoria);
            return false;
        },
        doDelete: function(curCategoria) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTA CATEGORÍA?</strong></h4>',
            function(ok) {

                if (!ok) return;

                var categoriaModel = new CategoriaModel({
                    idCategoria : curCategoria.idCategoria,
                });

                // console.log('categoriaModel: ', categoriaModel);

                categoriaModel.destroy({
                    success: function(model, response, options) {
                        console.log('response: ', response);
                        $.notify('Se eliminó la categoría exitosamente', 'success');
                        self.loadCategorias();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        render: function() {
            var self = this;
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(manageCategoriaTableTemplate, {
                modSecurity : modSecurity,
                curUrlDescripcion : this.curUrlDescripcion,
            });
            this.$el.html(compiledTemplate);
            // console.log('this.categorias.toJSON(): ', this.categorias.toJSON());
            var config = {
                rowId: 'idCategoria',
                // hasDetails: true,
                formatDataTableDetails: _.bind(this.formatDataTableDetails, this),
                order: [[1, 'asc']],
                container: '.data-table-container',
                $container: this.$('.data-table-container'),
                doEdit: _.bind(this.doEdit, this),
                doDelete: _.bind(this.doDelete, this),
                columns: [
                    {
                        data : 'descripcionCategoria',
                        sClass: 'alinear-vertical',
                        // width : '20%',
                    },
                    {
                        data : 'abreviaturaCategoria',
                        sClass: 'alinear-vertical',
                        // width : '40%',
                    },
                    {
                        data : 'significancias',
                        sClass: 'alinear-vertical',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                ],
            }
            if (modSecurity.canEdit(this.curUrlDescripcion) ||
                modSecurity.canDelete(this.curUrlDescripcion)) {
                config.columns.push(
                {
                        data : 'idCategoria',
                        width : '17%',
                        orderable: false,
                        sClass : 'alinear-vertical' + ((modSecurity.canEdit(this.curUrlDescripcion) &&
                        !modSecurity.canDelete(this.curUrlDescripcion)) ||
                        (!modSecurity.canEdit(this.curUrlDescripcion) &&
                        modSecurity.canDelete(this.curUrlDescripcion)) ? ' text-center' : '')
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsCategoriaTemplate, {
                                modSecurity : modSecurity,
                                curUrlDescripcion : self.curUrlDescripcion,
                                idCategoria : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     });
            }
            modDataTables.showGrid(this.categorias.toJSON(), config);
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveCategoriaTemplate, {}));
        },
        formatDataTableDetails: function(curCategoria) {
            return _.template(categoriaDetailTemplate, {curCategoria : curCategoria});
        },
        getSubcategoriasModels: function() {
            var subcategorias = [];
            $('.subcategorias input').each(function(index, element) {
                var $element = $(element);
                categoriaModel = new CategoriaModel({
                    idCategoria: $element.attr('data-categoria-id'),
                    peso: $element.val(),
                    nivel : 2,
                    significancia: $element.attr('data-significancia'),
                });
                subcategorias.push(categoriaModel);
            })
            return subcategorias;
        },
    });

    return ManageCategoriaView;
});