define([
        'underscore',
        'backbone',

        'collections/TipoMetricaCollection',

        'mods/modUtils',
        'mods/modSecurity',
        'mods/modDataTables',
        'mods/validaciones/modValidator',
        'mods/validaciones/TipoMetricaValidator',

        'bootbox',
        'mods/modCamposDinamicos',

        'models/TipoMetricaModel',

        'text!templates/tipoMetrica/manageTipoMetricaTableTemplate.html',
        'text!templates/tipoMetrica/saveTipoMetricaTemplate.html',
        'text!templates/tipoMetrica/columnaOpsTipoMetricaTemplate.html',
        'text!templates/tipoMetrica/buttonSaveTipoMetricaTemplate.html',
    ], function(
        _,
        Backbone,

        TipoMetricaCollection,

        modUtils,
        modSecurity,
        modDataTables,
        modValidator,
        TipoMetricaValidator,

        bootbox,
        modCamposDinamicos,

        TipoMetricaModel,

        manageTipoMetricaTableTemplate,
        saveTipoMetricaTemplate,
        columnaOpsTipoMetricaTemplate,
        buttonSaveTipoMetricaTemplate

    ) {

    var ManageTipoMetricaView = Backbone.View.extend({
        el: '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options || {};
            this.loadTipoMetricas();
            this.initPermisos();
        },
        loadTipoMetricas: function() {
            var self = this;
            this.tiposMetricas = new TipoMetricaCollection();
            var complete = _.invoke([this.tiposMetricas], 'fetch');
            $.when.apply($, complete)
            .done(function() {
                self.render();
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-metrica'] = 'doSaveTipoMetrica';
        },
        events: {
            // 'click .add-new-metrica': 'doSaveTipoMetrica',
        },
        doSaveTipoMetrica: function(e, curTipoMetrica) {

            var self = this;
            // console.log('this.unidadesMedida.toJSON(): ', this.unidadesMedida.toJSON());
            var compiledTemplate = _.template(saveTipoMetricaTemplate, {
                curTipoMetrica: curTipoMetrica,
            });

            $('#modal-area').html(compiledTemplate);

            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#descripcion-tipo-metrica').focus();
            });
            $('#modal-area #registrar-metrica').click(function() {
                $('#tipo-kpi-loading').show();
                $(this).addClass('disabled');

                var curTipoMetrica = new TipoMetricaModel({
                    descripcionTipoMetrica: $('#descripcion-tipo-metrica').val(),
                });

                var update = false;
                if ($('#id-tipo-metrica').val().trim() != "") {
                    curTipoMetrica.set('idTipoMetrica', $('#id-tipo-metrica').val().trim());
                    update = true;
                }

                var hasMessages = modValidator.validar(
                    new TipoMetricaValidator(curTipoMetrica),
                    curTipoMetrica.toJSON()
                );

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $('#tipo-kpi-loading').hide();
                    $(this).removeClass('disabled');

                    $('#myModal #registrar-metrica').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    $(this).off('click');
                    var el = this;
                    /* está validado entonces guardamos los cambios */
                    curTipoMetrica.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' el KPI exitosamente', 'success');
                            self.loadTipoMetricas();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $('#tipo-kpi-loading').hide();
                            $(el).removeClass('disabled');

                            console.log('response: ', response);
                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curTipoMetrica) {
            this.doSaveTipoMetrica(null, curTipoMetrica);
            return false;
        },
        doDelete: function(curTipoMetrica) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTE TIPO DE KPI?</strong></h4>',
            function(ok) {

                if (!ok) return;

                var tipoMetricaModel = new TipoMetricaModel({
                    idTipoMetrica : curTipoMetrica.idTipoMetrica,
                });

                tipoMetricaModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó el KPI exitosamente', 'success');
                        self.loadTipoMetricas();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        render: function() {
            var self = this;
            modUtils.processCustomMessages();

            var compiledTemplate = _.template(manageTipoMetricaTableTemplate, {
                curUrlDescripcion : this.curUrlDescripcion,
                modSecurity : modSecurity,
            });

            this.$el.html(compiledTemplate);

            var config = {
                container: '.data-table-container',
                $container: this.$('.data-table-container'),
                doEdit: _.bind(this.doEdit, this),
                doDelete: _.bind(this.doDelete, this),
                columns: [
                    {
                        data : 'descripcionTipoMetrica',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                 ],
            }
            if (modSecurity.canEdit(this.curUrlDescripcion) ||
                modSecurity.canDelete(this.curUrlDescripcion)) {
                config.columns.push({
                        data : 'idTipoMetrica',
                        width : '20%',
                        orderable : false,
                        sClass : (modSecurity.canEdit(this.curUrlDescripcion) &&
                        !modSecurity.canDelete(this.curUrlDescripcion)) ||
                        (!modSecurity.canEdit(this.curUrlDescripcion) &&
                        modSecurity.canDelete(this.curUrlDescripcion)) ? 'text-center' : ''
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsTipoMetricaTemplate, {
                                modSecurity : modSecurity,
                                curUrlDescripcion : self.curUrlDescripcion,
                                idTipoMetrica : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     });
            }

            modDataTables.showGrid(this.tiposMetricas.toJSON(), config);
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveTipoMetricaTemplate, {}));
        }
    });

    return ManageTipoMetricaView;
});