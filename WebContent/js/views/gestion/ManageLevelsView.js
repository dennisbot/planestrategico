define([
        'underscore',
        'backbone',

        'mods/modUtils',
        'mods/modDataTables',
        'mods/validaciones/modValidator',
        'mods/validaciones/DescripcionNivelValidator',

        'bootbox',
        'mods/modSecurity',

        'models/DescripcionNivelModel',

        'text!templates/level/buttonSaveLevelTemplate.html',
        'text!templates/level/manageLevelsTemplate.html',
        'text!templates/level/saveLevelTemplate.html',
        'text!templates/level/columnaOpsLevelsTemplate.html',
    ], function(
        _,
        Backbone,

        modUtils,
        modDataTables,
        modValidator,
        DescripcionNivelValidator,

        bootbox,
        modSecurity,

        DescripcionNivelModel,

        buttonSaveLevelTemplate,
        manageLevelsTemplate,
        saveLevelTemplate,
        columnaOpsLevelsTemplate
    ) {

    var ManageLevelsView = Backbone.View.extend({
        el: '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options || {};
            this.fetchLevels();
            this.initPermisos();
        },
        fetchLevels: function() {
            var self = this;
            (new DescripcionNivelModel()).fetch({
                success: function (model, response, options) {
                    // console.log('response: ', response);
                    self.options.levels = response;
                    self.render();
                },
                error: function (model, response, options) {
                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('response: ', response);
                }
            })

        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-level'] = 'doSaveLevel';
        },
        events: {
        },
        doSaveLevel: function(e, curDescripcionNivel) {
            var update = e ? false : true;
            var self = this;

            var compiledTemplate = _.template(saveLevelTemplate, {
                curDescripcionNivel: curDescripcionNivel,
            });

            $('#modal-area').html(compiledTemplate);
            $('#myModal').modal().on('shown.bs.modal', function() {
                (update) ? $('#descripcion-nivel').focus() : $('#nivel').focus();
            });
            $('#modal-area #registrar-nivel').click(function() {
                $(this).addClass('disabled');
                $('#nivel-loading').show();

                var curDescripcionNivel = new DescripcionNivelModel({
                    idNivel : $('#nivel').val().trim(),
                    descripcionNivel: $('#descripcion-nivel').val(),
                    abreviatura: $('#abreviatura').val(),
                    nivel : $('#nivel').val().trim()
                });

                var hasMessages = modValidator.validar(
                    new DescripcionNivelValidator(
                        update,
                        curDescripcionNivel.urlRoot,
                        curDescripcionNivel.get('idNivel')
                    ),
                    curDescripcionNivel.toJSON()
                );

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $(this).removeClass('disabled');
                    $('#nivel-loading').hide();

                    $('#myModal #registrar-nivel').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    if (!update)
                        curDescripcionNivel.unset('idNivel');

                    $(this).off('click');
                    var el = this;
                    /* está validado entonces guardamos los cambios */
                    curDescripcionNivel.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' el nivel exitosamente', 'success');
                            self.fetchLevels();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $('#nivel-loading').hide();
                            $(el).removeClass('disabled');

                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curLevel) {
            this.doSaveLevel(null, curLevel);
            return false;
        },
        doDelete: function(curLevel) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTA DESCRIPCIÓN DEL NIVEL?</strong></h4>',
            function(ok) {

                if (!ok) return;

                var curDescripcionNivel = new DescripcionNivelModel({
                    idNivel : curLevel.idNivel,
                });
                curDescripcionNivel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó la descripción del nivel exitosamente', 'success');
                        self.fetchLevels();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        render: function() {
            var self = this;
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(manageLevelsTemplate, {
                curUrlDescripcion : this.curUrlDescripcion,
                modSecurity : modSecurity
            });
            this.$el.html(compiledTemplate);

            var config = {
                container: '.data-table-container',
                $container: this.$('.data-table-container'),
                order : [[0, 'asc']],
                doEdit: _.bind(this.doEdit, this),
                doDelete: _.bind(this.doDelete, this),
                columns: [
                    {
                        data : 'idNivel',
                        width: '5%',
                        sClass: 'text-center',
                        orderable : true,
                    },
                    {
                        data : 'descripcionNivel',
                        // width : '40%',
                    },
                    {
                        data : 'abreviatura',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                ],
            }
            if (modSecurity.canEdit(this.curUrlDescripcion) ||
                modSecurity.canDelete(this.curUrlDescripcion)) {
                config.columns.push({
                        data : 'idNivel',
                        width : '20%',
                        orderable : false,
                        sClass : (modSecurity.canEdit(this.curUrlDescripcion) &&
                        !modSecurity.canDelete(this.curUrlDescripcion)) ||
                        (!modSecurity.canEdit(this.curUrlDescripcion) &&
                        modSecurity.canDelete(this.curUrlDescripcion)) ? 'text-center' : ''
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsLevelsTemplate, {
                                modSecurity : modSecurity,
                                curUrlDescripcion : self.curUrlDescripcion,
                                idNivel : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                });
            }
            modDataTables.showGrid(this.options.levels, config);
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveLevelTemplate, {}));
        }
    });

    return ManageLevelsView;
});