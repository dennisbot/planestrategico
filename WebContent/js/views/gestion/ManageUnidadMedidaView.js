define([
        'underscore',
        'backbone',

        'collections/UnidadMedidaCollection',

        'mods/modUtils',
        'mods/modDataTables',
        'mods/validaciones/modValidator',
        'mods/validaciones/UnidadMedidaValidator',

        'bootbox',
        'mods/modSecurity',

        'models/UnidadMedidaModel',

        'text!templates/unidadMedida/manageUnidadMedidaTableTemplate.html',
        'text!templates/unidadMedida/saveUnidadMedidaTemplate.html',
        'text!templates/unidadMedida/columnaOpsUnidadesMedidaTemplate.html',
        'text!templates/unidadMedida/buttonSaveUnidadMedidaTemplate.html',
    ], function(
        _,
        Backbone,

        UnidadMedidaCollection,

        modUtils,
        modDataTables,
        modValidator,
        UnidadMedidaValidator,

        bootbox,
        modSecurity,

        UnidadMedidaModel,

        manageUnidadMedidaTableTemplate,
        saveUnidadMedidaTemplate,
        columnaOpsUnidadesMedidaTemplate,
        buttonSaveUnidadMedidaTemplate
    ) {

    var ManageUnidadMedidaView = Backbone.View.extend({
        el: '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options || {};
            this.loadUnidadesMedida();
            this.initPermisos();
        },
        loadUnidadesMedida: function() {
            var self = this;
            this.unidadesMedida = new UnidadMedidaCollection();
            var complete = _.invoke([this.unidadesMedida], 'fetch');
            $.when.apply($, complete)
            .done(function() {
                self.render();
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-unidadMedida'] = 'doSaveUnidadMedida';
        },
        events: {
            // 'click .add-new-unidadMedida': 'doSaveUnidadMedida',
        },
        doSaveUnidadMedida: function(e, curUnidadMedida) {

            var self = this;

            var compiledTemplate = _.template(saveUnidadMedidaTemplate, {
                curUnidadMedida: curUnidadMedida,
            });

            $('#modal-area').html(compiledTemplate);
            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#descripcion-unidad-medida').focus();
            });
            $('#modal-area #registrar-unidad-medida').click(function() {
                $('#unidad-medida-loading').show();
                $(this).addClass('disabled');

                var curUnidadMedida = new UnidadMedidaModel({
                    unidad: $('#descripcion-unidad-medida').val(),
                    abreviatura: $('#abreviatura').val(),
                });

                var update = false;
                if ($('#id-unidad-medida').val().trim() != "") {
                    curUnidadMedida.set('idUnidadMedida', $('#id-unidad-medida').val().trim());
                    update = true;
                }

                var hasMessages = modValidator.validar(
                    new UnidadMedidaValidator(curUnidadMedida),
                    curUnidadMedida.toJSON()
                );

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $('#unidad-medida-loading').hide();
                    $(this).removeClass('disabled');
                    $('#myModal #registrar-unidad-medida').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    $(this).off('click');
                    var el = this;
                    /* está validado entonces guardamos los cambios */
                    curUnidadMedida.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' la unidad de medida exitosamente', 'success');
                            self.loadUnidadesMedida();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $('#unidad-medida-loading').hide();
                            $(el).removeClass('disabled');
                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curUnidadMedida) {
            console.log('do edit was called!');
            this.doSaveUnidadMedida(null, curUnidadMedida);
            return false;
        },
        doDelete: function(curUnidadMedida) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTA UNIDAD DE MEDIDA?</strong></h4>',
            function(ok) {

                if (!ok) return;

                var unidadMedidaModel = new UnidadMedidaModel({
                    idUnidadMedida : curUnidadMedida.idUnidadMedida,
                });

                unidadMedidaModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó la unidad de medida exitosamente', 'success');
                        self.loadUnidadesMedida();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        render: function() {
            var self = this;
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(manageUnidadMedidaTableTemplate, {
                curUrlDescripcion : this.curUrlDescripcion,
                modSecurity : modSecurity
            });
            this.$el.html(compiledTemplate);
            var config = {
                container: '.data-table-container',
                $container: this.$('.data-table-container'),
                doEdit: _.bind(this.doEdit, this),
                doDelete: _.bind(this.doDelete, this),
                columns: [
                    {
                        data : 'unidad',
                        // width : '40%',
                    },
                    {
                        data : 'abreviatura',
                        sClas : 'text-center',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                ],
            }
            if (modSecurity.canEdit(this.curUrlDescripcion) ||
                modSecurity.canDelete(this.curUrlDescripcion)) {
                config.columns.push({
                        data : 'idUnidadMedida',
                        width : '20%',
                        orderable : false,
                        sClass : (modSecurity.canEdit(this.curUrlDescripcion) &&
                        !modSecurity.canDelete(this.curUrlDescripcion)) ||
                        (!modSecurity.canEdit(this.curUrlDescripcion) &&
                        modSecurity.canDelete(this.curUrlDescripcion)) ? 'text-center' : ''
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsUnidadesMedidaTemplate, {
                                modSecurity : modSecurity,
                                curUrlDescripcion : self.curUrlDescripcion,
                                idUnidadMedida : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                });
            }

            modDataTables.showGrid(this.unidadesMedida.toJSON(), config);
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveUnidadMedidaTemplate, {}));
        }
    });

    return ManageUnidadMedidaView;
});