define([
    'underscore',
    'backbone',

    'mods/modDataTables',
    'mods/modCamposDinamicos',
    'mods/modUtils',
    'mods/modProgressBar',
    'mods/modSecurity',

    'text!templates/gestion/niveles/iniciativas/control/detailIniciativaTemplate.html',
    'text!templates/gestion/niveles/iniciativas/control/controlIniciativasTableTemplate.html',
    'text!templates/gestion/niveles/iniciativas/control/dataTableIniciativasTemplate.html',

    'text!templates/gestion/niveles/iniciativas/control/selects/selectGerenciaTemplate.html',
    'text!templates/gestion/niveles/iniciativas/control/selects/selectSuperintendenciaTemplate.html',

    'config/iniciativaDetalleConfig',

    'models/ItemEstrategicoModel',
    'models/UtilsModel',

    'views/ArchivoView',

    'collections/ItemEstrategicoCollection',
    'collections/EmpleadoCollection',
    'collections/CategoriaCollection',
    'collections/DependenciaOrganizacionalCollection',
], function(
    _,
    Backbone,

    modDataTables,
    modCamposDinamicos,
    modUtils,
    modProgressBar,
    modSecurity,

    detailIniciativaTemplate,
    controlIniciativasTableTemplate,
    dataTableIniciativasTemplate,

    selectGerenciaTemplate,
    selectSuperintendenciaTemplate,

    iniciativaDetalleConfig,

    ItemEstrategicoModel,
    UtilsModel,

    ArchivoView,

    ItemEstrategicoCollection,
    EmpleadoCollection,
    CategoriaCollection,
    DependenciaOrganizacionalCollection
) {
    var ControlIniciativasView = Backbone.View.extend({
        el : '#container',
        initialize : function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options;
            this.level = 4;
            this.loadSeedData();
            this.initPermisos();
        },
        onClose : function() {
            if (this.archivoView)
                this.archivoView.close();
        },
        initPermisos : function() {

        },
        loadSeedData: function() {
            var self = this;

            this.serverDate = new UtilsModel({}, {
                util : '/serverDate',
            });
            this.dependencias = new DependenciaOrganizacionalCollection();
            this.categorias = new CategoriaCollection();
            this.empleados = new EmpleadoCollection();
            this.iniciativas = new ItemEstrategicoCollection([], {
                nivel : this.level,
                includeHierachy : true
            });

            var models = [this.serverDate, this.dependencias,
            this.categorias, this.iniciativas, this.empleados];

            var complete = _.invoke(models, 'fetch', {
                success: function(model, response, options) {
                    var isNumResponse = modUtils.isNumeric(response);
                    if (isNumResponse)
                        self.serverDate = new Date(response);
                },
                error: function(model, response, options) {
                    $.notify('ERROR conectandose con el servidor');
                    console.log('error:');
                    console.log('response: ', response);
                }
            });

            $.when.apply($, complete)
            .done(function() {
                self.render();
                // console.log('self.dependencias.toJSON(): ', self.dependencias.toJSON());
                // console.log('self.categorias.toJSON(): ', self.categorias.toJSON());
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            })
        },
        formatDataTableDetails: function(curItem) {
            /* seteamos el parentItem */
            modProgressBar.iniciativa = curItem;
            modProgressBar.curPeriodo = this.curPeriodo();
            return _.template(detailIniciativaTemplate, {
                curPeriodo : this.curPeriodo(),
                canEdit : this.canEditIniciativa(curItem.dependenciaOrganizacional.idDependenciaOrganizacional),
                curItem : curItem,
                modProgressBar : modProgressBar,
                modUtils : modUtils,
                categoria : this.getCategorias(curItem.categoria.idCategoriaParent),
                getNombreParaMostrar : _.bind(this.getNombreParaMostrar, this)
            });
        },
        canEditIniciativa : function(idDependenciaOrganizacional) {
            var canEdit = _.find(parametros.superintendencias, function(superintendencia) {
                return superintendencia.idDependenciaOrganizacional == idDependenciaOrganizacional
            })
            canEdit = canEdit ? true : false;
            canEdit = canEdit ||
            idDependenciaOrganizacional == parametros.curSuperintendencia.idDependenciaOrganizacional;
            return canEdit;
        },
        curPeriodo : function(curItem) {
            return 04;
            // return this.serverDate.getMonth() + 1
        },
        changeProgressBar : function(progressbar, instance) {
            progressbar.attr('aria-valuenow', instance.getAvance());
            progressbar.css({
                'width' : (instance.getAvance() > 100 ? 100 : instance.getAvance()) + '%',
                'background-color' : instance.getBackColor(),
                'color' : instance.getForeColor()
            });
            instance.setBold(progressbar);
            progressbar.html(instance.getAvance() + '%');
        },
        onChildRowRendered : function(curItem) {
            // console.log('conChildRowRendered');
            // console.log('curItem: ', curItem);
            var self = this;
            var args = {
                error_sink: function(idOfEditor, errorString) {
                    console.log('hubo un error: ', idOfEditor);
                    console.log(errorString);
                },
                callback: function(element_id, update_value, original_value) {
                    // console.log('update_value: ', update_value);
                    // console.log('original_value: ', original_value);
                    if (original_value == update_value) return original_value;
                    var curCell = $('#' + element_id);

                    var metaIdItemEstrategico = parseInt(curCell.attr('data-meta-id'));
                    var idIniciativaParent = parseInt(curCell.attr('data-iniciativa-id'));
                    var index = parseInt(curCell.attr('data-index'));

                    var meta = new ItemEstrategicoModel({
                        idItemEstrategico : metaIdItemEstrategico
                    });
                    var html_response = original_value;

                    meta.fetch({
                        async: false,
                        success: function(model, response, options) {
                            meta.set('valorReal', update_value);
                            meta.save({}, {
                                async: false,
                                success: function(model, response, options) {
                                    var curRow = $('#' + idIniciativaParent).get(0);
                                    var curIniciativa = modDataTables.datatable.row(curRow).data();
                                    curIniciativa.itemEstrategicos[index].valorReal = parseFloat(update_value);

                                    modProgressBar.curPeriodo = self.curPeriodo();
                                    var instanceYTD = modProgressBar.getInstance(4, curIniciativa);
                                    var progressBarYTD = $('#ytd-' + idIniciativaParent).find('div.progress-bar');

                                    self.changeProgressBar(progressBarYTD, instanceYTD);

                                    modProgressBar.curPeriodo = null;
                                    curIniciativa.valorReal = modProgressBar.getInstance(4, curIniciativa).getAvance();
                                    modDataTables.datatable.row(curRow).data(curIniciativa);

                                    html_response = update_value == 0 ? '-' : update_value;

                                    modProgressBar.iniciativa = curIniciativa;

                                    var instance = modProgressBar.getInstance(5, meta.toJSON());
                                    var progressbar = curCell.next().find('div.progress-bar');
                                    self.changeProgressBar(progressbar, instance);

                                    var idParentItem = meta.get('idParentItem');
                                    $('#metas-' + meta.get('idParentItem'))
                                    .notify('meta actualizada exitosamente', {
                                        position : 'right middle',
                                        className : 'success'
                                    })
                                },
                                error: function(model, response, options) {
                                    $.notify('ERROR conectándose con el servidor')
                                    console.log('response: ', response);
                                }
                            })
                        },
                        error: function(model, response, options) {
                            $.notify('ERROR conectándose con el servidor');
                            console.log('response: ', response);
                        }
                    });
                    return html_response;
                },
                default_text: '-',
                value_required: true,
                // show_buttons: true
            };
            var options = {
                args : args,
                inputInplaceFieldSelector : 'table tr td input.inplace_field',
                onfocus : function() {
                    if (curOuterWidth)
                        self.curOuterWidth = curOuterWidth;
                    if (curOuterHeight)
                        self.curOuterHeight = curOuterHeight;
                    $(this).closest('td').css({padding: 0});
                    $(this).closest('form').css({margin : '0px'});
                    $(this).css({
                        'height': self.curOuterHeight - 1,
                        'width': self.curOuterWidth - 1,
                        'text-align': 'center',
                        'border-style': 'none',
                        'border-width': '0',
                        'padding': '5px',
                        'margin': '0',
                    });

                },
                onblur : function() {
                    $(this).closest('td').css({padding: '5px'});
                }
            }
            modCamposDinamicos.iniciarEditInPlace(options);
            curItem.itemEstrategicos.forEach(function(meta) {
                $('#editar-ver-adjuntos-' + meta.idItemEstrategico).click(function(e) {
                    e.preventDefault();
                    var curRow = $('#' + meta.idParentItem).get(0);
                    var curIniciativa = modDataTables.datatable.row(curRow).data();

                    if (self.archivoView) {
                        var curMeta = _.find(curIniciativa.itemEstrategicos, function(itemEstrategico) {
                            return itemEstrategico.idItemEstrategico == self.archivoView.meta.idItemEstrategico
                        })
                        if (curMeta) {
                            curMeta.comentarios = self.archivoView.meta.comentarios;
                            modDataTables.datatable.row(curRow).data(curIniciativa);
                        }
                        self.archivoView.close();
                    }

                    // console.log('curIniciativa: ', curIniciativa);
                    self.archivoView = new ArchivoView({
                        meta : meta,
                        iniciativa : curIniciativa,
                        serverDate : self.serverDate,
                        parentContext : self
                    });
                })
            })
        },
        getCategorias : function(idCategoria) {
            // console.log('idCategoria: ', idCategoria);
            var res = _.find(this.categorias.toJSON(), function(categoria) {
                return categoria.idCategoria == idCategoria
            })
            // console.log('res: ', res);
            return res;
        },
        getNombreParaMostrar : function(idResponsable) {
            var empleado = _.find(this.empleados.toJSON(), function(empleado) {
                // console.log('empleado: ', empleado);
                return empleado.idEmpleado == idResponsable
            })
            return empleado.nombreParaMostrar;
        },
        render: function() {
            // console.log('this.dependencias.toJSON(): ', this.dependencias.toJSON());
            // console.log('this.iniciativas.toJSON(): ', this.iniciativas.toJSON());
            // console.log('this.serverDate: ', this.serverDate);
            this.$el.html(controlIniciativasTableTemplate);
            this.prepareFields();
            this.showGrid(this.iniciativas.toJSON());
        },
        showGrid: function(iniciativas) {
            this.$('#item-estrategico-container').html(dataTableIniciativasTemplate);
            modDataTables.showGrid(iniciativas, iniciativaDetalleConfig.getConfig(this));
        },
        showGridArchivos: function(iniciativas) {
            this.$('#item-estrategico-container').html(dataTableIniciativasTemplate);
            modDataTables.showGrid(iniciativas, iniciativaDetalleConfig.getConfig(this));
        },
        prepareFields: function() {
            var self = this;
            modCamposDinamicos.iniciarChosen({
                selector: '.chosen-select',
                search_contains: true
            });
            $('#mostrar-ini-por').change(function() {
                var mostrarx = $(this).val();
                self.clearContainers();
                switch(mostrarx) {
                    case '':
                        self.showGrid(self.iniciativas.toJSON());
                        break;
                    case 'xgerencias':
                        modDataTables.datatable.clear().draw();
                        var compiledTemplate = _.template(selectGerenciaTemplate, {
                            gerencias : self.getGerencias(),
                        });
                        self.$('#select-gerencias-sup-container').html(compiledTemplate);
                        modCamposDinamicos.iniciarChosen({
                            selector: '#select-gerencia',
                            search_contains: true
                        })
                        self.$('#select-gerencia').change(function() {
                            // console.log('entra en select-gerencia ' + Math.floor(Math.random() * 100));
                            var idGerencia = $(this).val();
                            // console.log('idGerencia: ', idGerencia);
                            var compiledTemplate = _.template(selectSuperintendenciaTemplate, {
                                superintendencias : self.getSuperintendencias(idGerencia),
                                byGerencia : true
                            })
                            self.$('#select-superintendencias-container').html(compiledTemplate);
                            modCamposDinamicos.iniciarChosen({
                                selector: '#select-superintendencia',
                                search_contains: true
                            });
                            self.showGrid(self.getIniciativasByGerencia(idGerencia));
                            self.addSuperintendenciaChangeEventHandler();
                        })
                        break;
                    case 'xsuperintendencias':
                        modDataTables.datatable.clear().draw();
                        var compiledTemplate = _.template(selectSuperintendenciaTemplate, {
                            superintendencias : self.getSuperintendencias(),
                            byGerencia : false
                        })
                        self.$('#select-gerencias-sup-container').html(compiledTemplate);
                        modCamposDinamicos.iniciarChosen({
                            selector: '#select-superintendencia',
                            search_contains: true
                        })
                        self.addSuperintendenciaChangeEventHandler();
                }
            })
        },
        addSuperintendenciaChangeEventHandler : function() {
            var self = this;
            this.$('#select-superintendencia').change(function() {
                var idSuperintendencia = $(this).val();
                if (modUtils.isNumeric(idSuperintendencia)) {
                    self.showGrid(self.getIniciativasBySuperintendencia(idSuperintendencia));
                }
                else {
                    var idGerencia = self.$('#select-gerencia').val();
                    self.showGrid(self.getIniciativasByGerencia(idGerencia));
                }
            })
        },
        clearContainers : function() {
            self.$('#select-gerencias-sup-container').empty();
            self.$('#select-superintendencias-container').empty();
        },
        getGerencias : function() {
            var gerencias = _.filter(this.dependencias.toJSON(), function(jerarquia) {
                return jerarquia.idNivelJerarquico == 4 ||
                jerarquia.descripcionDependencia.indexOf('GERENCIA') != -1
            })
            // console.log('gerencias: ', gerencias);
            return gerencias;
        },
        getSuperintendencias : function(idGerencia) {
            var superintendencias;
            if (idGerencia)
                superintendencias = _.filter(this.dependencias.toJSON(), function(jerarquia) {
                    return jerarquia.parentDependencia.idDependenciaOrganizacional == idGerencia
                })
            else
                superintendencias = _.filter(this.dependencias.toJSON(), function(jerarquia) {
                return jerarquia.idNivelJerarquico == 5 ||
                jerarquia.descripcionDependencia.indexOf('SUPERINTENDENCIA') != -1
            })
            // console.log('superintendencias: ', superintendencias);
            return superintendencias;
        },
        getIniciativasBySuperintendencia : function(idSuperintendencia) {
            var iniciativas = _.filter(this.iniciativas.toJSON(), function(iniciativa) {
                return iniciativa.dependenciaOrganizacional.idDependenciaOrganizacional == idSuperintendencia
            })
            // console.log('iniciativas: ', iniciativas);
            return iniciativas;
        },
        getIniciativasByGerencia : function(idGerencia) {
            var iniciativas = _.filter(this.iniciativas.toJSON(), function(iniciativa) {
                return iniciativa.hierachy[2].dependenciaOrganizacional.idDependenciaOrganizacional == idGerencia
            })
            // console.log('iniciativas: ', iniciativas);
            return iniciativas;
        }
    })

    return ControlIniciativasView;
});