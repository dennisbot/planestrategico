define([
        'underscore',
        'backbone',

        'collections/MetricaCollection',
        'collections/TipoMetricaCollection',
        'collections/UnidadMedidaCollection',
        'collections/ProcesoCollection',

        'mods/modUtils',
        'mods/modSecurity',
        'mods/DataTable/DataTable',
        'mods/validaciones/modValidator',
        'mods/validaciones/MetricaValidator',

        'bootbox',
        'mods/modCamposDinamicos',
        'mods/modAjax',

        'models/MetricaModel',

        'text!templates/kpi/manageMetricaTableTemplate.html',
        'text!templates/kpi/saveMetricaTemplate.html',

        'text!templates/kpi/buttonSaveMetricaTemplate.html',
        'text!templates/kpi/buttonRemoveMetricaTemplate.html',
        'text!templates/kpi/metricaDetailTemplate.html',
        'text!templates/kpi/selectSubProcesoTemplate.html',
        'text!templates/kpi/dataTableKpisTemplate.html',
        'text!templates/kpi/dataTableKpisBulkTemplate.html',
        'text!templates/kpi/filtros/selectProcesoTemplate.html',
        'text!templates/kpi/filtros/selectFiltroSubProcesoTemplate.html',

        'config/kpisConfig',
        'config/kpisBulkConfig',
    ], function(
        _,
        Backbone,

        MetricaCollection,
        TipoMetricaCollection,
        UnidadMedidaCollection,
        ProcesoCollection,

        modUtils,
        modSecurity,
        DataTable,
        modValidator,
        MetricaValidator,

        bootbox,
        modCamposDinamicos,
        modAjax,

        MetricaModel,

        manageMetricaTableTemplate,
        saveMetricaTemplate,

        buttonSaveMetricaTemplate,
        buttonRemoveMetricaTemplate,
        metricaDetailTemplate,
        selectSubProcesoTemplate,
        dataTableKpisTemplate,
        dataTableKpisBulkTemplate,
        selectProcesoTemplate,
        selectFiltroSubProcesoTemplate,

        kpisConfig,
        kpisBulkConfig
    ) {

    var ManageMetricaView = Backbone.View.extend({
        // el: '#container',
        initialize: function(options) {
            this.mainTemplate = options && options.mainTemplate ? options.mainTemplate : manageMetricaTableTemplate;
            this.isModal = options && options.isModal ? options.isModal : false;
            this.showBulk = options ? false : true;
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options || {};
            this.initPermisos();
            this.loadMetricas();
        },
        loadMetricas: function() {
            var self = this;
            this.metricas = new MetricaCollection();
            this.unidadesMedida = new UnidadMedidaCollection();
            this.tipoMetricas = new TipoMetricaCollection();
            this.procesos = new ProcesoCollection();

            var complete = _.invoke([
                this.metricas,
                this.unidadesMedida,
                this.tipoMetricas,
                this.procesos
            ], 'fetch');

            $.when.apply($, complete)
            .done(function() {
                self.render();
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        },
        loadMetricasUp: function() {
            this.showBulk = false;
            this.loadMetricas();
        },

        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-metrica'] = 'doSaveMetrica';
        },
        events: {
            'click .remove-metricas': 'doRemoveMetricas',
        },
        findSubProcesos : function(curMetrica) {
            if (!curMetrica) return [];
            var found = false;
            var res = [];
            _.each(this.procesos.toJSON(), function(proceso) {
                if (!found &&
                    _.contains(
                        _.pluck(proceso.subprocesos, "idProceso"),
                        curMetrica.subProceso.idProceso
                    )
                ) {
                    res = proceso.subprocesos;
                    found = true;
                }
            })
            return res;
        },
        findProceso : function(curMetrica) {
            if (!curMetrica) return [];
            var found = false;
            var res = [];
            _.each(this.procesos.toJSON(), function(proceso) {
                if (!found &&
                    _.contains(
                        _.pluck(proceso.subprocesos, "idProceso"),
                        curMetrica.subProceso.idProceso
                    )
                ) {
                    res = proceso;
                    found = true;
                }
            })
            return res;
        },
        initChosenSubproceso : function() {
            modCamposDinamicos.iniciarChosen({
                selector: '#select-subproceso',
                search_contains: true,
                placeholder: 'Elige el subproceso'
            });
        },
        doRemoveMetricas : function() {
            var self = this;
            var kpisToDelete = this.datatableBulk.datatable.rows('.selected').data().toArray();
            modAjax.callAJAXPostJSON({
                url : 'rest/metrica_kpi/remove_bulk',
                jsonVars : kpisToDelete,
                callback : function(response) {
                    // console.log('response: ', response);
                    $.notify('se ha eliminado ' + response + ' metrica(s) de un total de ' +
                        kpisToDelete.length, 'success');
                    self.initialize();
                },
                onerror : function(response) {
                    console.log('ha ocurrido un error al eliminar las métricas');
                    console.log('response: ', response);
                    $.notify('ERROR conectandose con el servidor', 'error');
                }
            })
        },
        doSaveMetrica: function(e, curMetrica) {
            var self = this;
            var compiledTemplate = _.template(saveMetricaTemplate, {
                curMetrica: curMetrica,
                unidadesMedida: this.unidadesMedida.models,
                tipoMetricas: this.tipoMetricas.models,
                procesos : this.procesos.toJSON(),
                html_select_subprocesos : _.template(selectSubProcesoTemplate, {
                    curMetrica : curMetrica,
                    pickFirst : false,
                    subprocesos : this.findSubProcesos(curMetrica),
                })
            });

            $('#modal-area').html(compiledTemplate);

            modCamposDinamicos.iniciarChosen({
                selector: '#select-unidad-medida',
                search_contains: true,
                placeholder: 'Elige una unidad de medida'
            });
            modCamposDinamicos.iniciarChosen({
                selector: '#select-tipo-metrica',
                search_contains: true,
                placeholder: 'Elige un tipo de KPI'
            });
            modCamposDinamicos.iniciarChosen({
                selector: '#select-proceso',
                search_contains: true,
                placeholder: 'Elige el proceso'
            });
            this.initChosenSubproceso();
            $('#select-proceso').change(function() {
                var idProceso = parseInt($(this).val());
                 var proceso = _.find(self.procesos.toJSON(), function(proceso) {
                            return proceso.idProceso == idProceso
                });
                $('#subprocesos-container').html(_.template(selectSubProcesoTemplate, {
                    curMetrica : curMetrica,
                    pickFirst : true,
                    subprocesos : proceso ? proceso.subprocesos : []
                }));
                self.initChosenSubproceso();
            })


            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#select-unidad-medida').trigger('chosen:activate');
                // $('#descripcion-metrica').focus();
            });
            $('#modal-area #registrar-metrica').click(function() {
                $('#kpi-loading').show();
                $(this).addClass('disabled');

                var curMetrica = new MetricaModel({
                    descripcionMetrica: $('#descripcion-metrica').val(),
                    definicionExplicacion: $('#definicion-explicacion').val(),
                    formula: $('#formula-metrica').val(),
                    unidadMedida : {
                        idUnidadMedida: $('#select-unidad-medida').val(),
                    },
                    tipoMetrica : {
                        idTipoMetrica: $('#select-tipo-metrica').val(),
                    },
                    subProceso : {
                        idProceso : $('#select-subproceso').val()
                    }
                });

                var update = false;
                if ($('#id-metrica').val().trim() != "") {
                    curMetrica.set('idMetrica', $('#id-metrica').val().trim());
                    update = true;
                }

                var hasMessages = modValidator.validar(
                    new MetricaValidator(curMetrica),
                    curMetrica.toJSON()
                );

                var valid = (hasMessages == true) ? hasMessages : false;
                // console.log('valid: ', valid);
                // console.log('se detiene acá');
                // return false;
                if (!valid) {
                    $('#kpi-loading').hide();
                    $(this).removeClass('disabled');

                    $('#myModal #registrar-metrica').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    $(this).off('click');
                    var el = this;
                    /* está validado entonces guardamos los cambios */
                    curMetrica.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' el KPI exitosamente', 'success');
                            self.loadMetricas();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $('#kpi-loading').hide();
                            $(el).removeClass('disabled');

                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curMetrica) {
            this.doSaveMetrica(null, curMetrica);
            return false;
        },
        doDelete: function(curMetrica) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTE KPI?</strong></h4>',
            function(ok) {

                if (!ok) return;

                var metricaModel = new MetricaModel({
                    idMetrica : curMetrica.idMetrica,
                });

                metricaModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó el KPI exitosamente', 'success');
                        self.loadMetricas();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        doSelect: function(curMetrica) {
            $('#idMetrica').val(curMetrica.idMetrica);
            $('#selected-metrica').html(curMetrica.descripcionMetrica);
            $('#select-metrica').html('Seleccionar otro KPI');
            $('#show-periodos').show();
            $('#myModal2').modal('hide');
        },
        render: function() {
            // console.log('this.procesos.toJSON(): ', this.procesos.toJSON());
            // console.log('this.metricas.toJSON(): ', this.metricas.toJSON());
            var self = this;
            modUtils.processCustomMessages();
            this.$el.html(_.template(this.mainTemplate, {
                showBulk : this.showBulk
            }));
            this.prepareFields();
            this.showGrid(this.metricas.toJSON());
            if (!this.isModal)
                this.showGridBulk(this.metricas.toJSON());
        },
        showGrid: function(kpis) {
            var self = this;
            this.$('#kpis-container').html(_.template(dataTableKpisTemplate, {
                modSecurity : modSecurity,
                curUrlDescripcion : this.curUrlDescripcion,
                isModal : this.isModal,
            }));
            var config = kpisConfig.getConfig(this);
            this.datatable = new DataTable();
            this.datatable.showGrid(kpis, config);
            if (this.isModal) {
                this.$('#myModal2').modal()
                .on('shown.bs.modal', function() {
                    self.datatable.datatable.columns.adjust().draw();
                    config.$container.css('visibility', 'visible');
                })
                .on('hidden.bs.modal', function() {
                    $('#myModal').focus();
                    $('body').addClass('modal-open');
                });
            }
            if (modSecurity.canCreate(this.curUrlDescripcion) && !this.isModal)
                this.$('#kpis-container .add-item').html(_.template(buttonSaveMetricaTemplate, {}));
        },
        showGridBulk: function(kpis) {
            var self = this;
            this.$('#kpis-bulk-container').html(_.template(dataTableKpisBulkTemplate, {
                modSecurity : modSecurity,
                curUrlDescripcion : this.curUrlDescripcion,
                isModal : this.isModal
            }));
            var config = kpisBulkConfig.getConfig(this);
            this.datatableBulk = new DataTable();
            this.datatableBulk.showGrid(kpis, config);
            this.$('#kpis-bulk-container .add-item').html(_.template(buttonRemoveMetricaTemplate, {}));
        },
        formatDataTableDetails: function(curMetrica) {
            // console.log('curMetrica: ', curMetrica);
            curMetrica.subProceso.proceso = this.findProceso(curMetrica);
            return _.template(metricaDetailTemplate, {
                curMetrica : curMetrica
            });
        },
        clearContainers : function() {
            self.$('#select-procesos-container').empty();
            self.$('#select-subprocesos-container').empty();
        },
        prepareFields: function() {
            var self = this;
            if (!this.isModal && modSecurity.canUpload(this.curUrlDescripcion)) {
                modCamposDinamicos.iniciarUploader({
                    selector: '#fine-uploader-kpis',
                    endpoint: 'uploadkpis',
                    text: {
                        uploadButton:
                        '<i class="fa fa-upload fa-lg" style="margin-right:15px">' +
                        '</i> Subir Excel con los KPIs (*.xlsx) máx 10MB'
                    },
                    callback : _.bind(this.loadMetricasUp, this),
                    validation : {
                        sizeLimit : 1024 * 1024 * 10, // 10MB
                        allowedExtensions: ['xlsx']
                    }
                    // context : self
                });
            }

            modCamposDinamicos.iniciarChosen({
                selector: '.chosen-select',
                search_contains: true
            });
            $('#mostrar-kpis-por').change(function() {
                var mostrarx = $(this).val();
                self.clearContainers();
                switch(mostrarx) {
                    case '':
                        self.showGrid(self.metricas.toJSON());
                        break;
                    case 'xproceso':
                        self.datatable.datatable.clear().draw();
                        var compiledTemplate = _.template(selectProcesoTemplate, {
                            procesos : self.procesos.toJSON(),
                        });
                        self.$('#select-procesos-container').html(compiledTemplate);
                        modCamposDinamicos.iniciarChosen({
                            selector: '#select-proceso',
                            search_contains: true
                        })
                        self.$('#select-proceso').change(function() {
                            // console.log('entra en select-gerencia ' + Math.floor(Math.random() * 100));
                            var idProceso = $(this).val();
                            // console.log('idProceso: ', idProceso);
                            var compiledTemplate = _.template(selectFiltroSubProcesoTemplate, {
                                subprocesos : self.getSubprocesos(idProceso),
                                byProceso : true
                            })
                            self.$('#select-subprocesos-container').html(compiledTemplate);
                            modCamposDinamicos.iniciarChosen({
                                selector: '#select-subproceso',
                                search_contains: true
                            });
                            self.showGrid(self.getMetricasByProceso(idProceso));
                            self.addSubProcesoChangeEventHandler();
                        })
                        break;
                    case 'xsubproceso':
                        self.datatable.datatable.clear().draw();
                        var compiledTemplate = _.template(selectFiltroSubProcesoTemplate, {
                            subprocesos : self.getSubprocesos(),
                            byProceso : false
                        })
                        self.$('#select-procesos-container').html(compiledTemplate);
                        modCamposDinamicos.iniciarChosen({
                            selector: '#select-subproceso',
                            search_contains: true
                        })
                        self.addSubProcesoChangeEventHandler();
                }
            })
        },
        addSubProcesoChangeEventHandler : function() {
            var self = this;
            this.$('#select-subproceso').change(function() {
                var idSubProceso = $(this).val();
                if (modUtils.isNumeric(idSubProceso)) {
                    self.showGrid(self.getMetricasBySubProceso(idSubProceso));
                }
                else {
                    var idProceso = self.$('#select-proceso').val();
                    self.showGrid(self.getMetricasByProceso(idProceso));
                }
            })
        },
        getMetricasBySubProceso : function(idSubProceso) {
            var metricas = _.filter(this.metricas.toJSON(), function(metrica) {
                return metrica.subProceso.idProceso == idSubProceso
            })
            // console.log('metricas: ', metricas);
            return metricas;
        },
        getMetricasByProceso : function(idProceso) {
            var metricas = _.filter(this.metricas.toJSON(), function(metrica) {
                return metrica.subProceso.procesoParent &&
                metrica.subProceso.procesoParent.idProceso == idProceso
            })
            // console.log('metricas: ', metricas);
            return metricas;
        },
        getSubprocesos : function(idProceso) {
            var subprocesos;
            if (idProceso) {
                proceso = _.find(this.procesos.toJSON(), function(proceso) {
                    return proceso.idProceso == idProceso
                })
                return proceso.subprocesos;
            }
            else {
                subprocesos = [];
                _.each(this.procesos.toJSON(), function(proceso) {
                    subprocesos = subprocesos.concat(proceso.subprocesos);
                })
            }
            return subprocesos;
        }
    });

    return ManageMetricaView;
});