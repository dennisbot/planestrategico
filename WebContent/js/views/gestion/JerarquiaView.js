define([

    'underscore',
    'backbone',

    'mods/modDataTables',
    'mods/modDataTablesSearchModal',
    'mods/validaciones/modValidator',
    'mods/modCamposDinamicos',
    'mods/modUtils',
    'mods/modSecurity',

    'bootbox',
    // 'subsequenceSearch',
    'config/searchItemEstrategicoConfig',

    'text!templates/gestion/searchItemEstrategicoTemplate.html',
    'text!templates/gestion/selectResponsableTemplate.html',
    'text!templates/gestion/niveles/iniciativas/selectKeyuserTemplate.html',
    'text!templates/gestion/niveles/iniciativas/selectSupervisorTemplate.html',
    'text!templates/gestion/jerarquiaBreadcrumbTemplate.html',
    'text!templates/gestion/detailedDepthTemplate.html',

    'text!templates/kpi/metricaDetailTemplate.html',
    'text!templates/gestion/searchDetailItemEstrategicoTemplate.html',


    'collections/ItemEstrategicoCollection',
    'collections/DependenciaOrganizacionalCollection',
    'collections/DescripcionNivelCollection',
    'collections/PeriodoCollection',
    'collections/CategoriaCollection',
    'collections/EmpleadoCollection',

    'models/ItemEstrategicoModel',
    'models/ResponsableModel',
    'models/DependenciaOrganizacionalModel',

    'mods/itemEstrategico/modItemEst',

], function(
    _,
    Backbone,

    modDataTables,
    modDataTablesSearchModal,
    modValidator,
    modCamposDinamicos,
    modUtils,
    modSecurity,

    bootbox,
    // subsequenceSearch,
    searchItemEstrategicoConfig,

    searchItemEstrategicoTemplate,
    selectResponsableTemplate,
    selectKeyuserTemplate,
    selectSupervisorTemplate,
    jerarquiaBreadcrumbTemplate,
    detailedDepthTemplate,

    metricaDetailTemplate,
    searchDetailItemEstrategicoTemplate,

    ItemEstrategicoCollection,
    DependenciaOrganizacionalCollection,
    DescripcionNivelCollection,
    PeriodoCollection,
    CategoriaCollection,
    EmpleadoCollection,
    ItemEstrategicoModel,
    ResponsableModel,
    DependenciaOrganizacionalModel,

    modItemEst

) {
    var JerarquiaView = Backbone.View.extend({
        el: '#container',
        curItemStack: [],
        curItem: null,
        initialize : function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.curItemStack = [];
            this.curItem = null;
            this.options = options || {};
            this.categorias = null;
            this.compiledSearchTemplate = null;
            this.loadSeedData();
            document.addEventListener('keydown', _.bind(this.searchHotKey, this), false);
            this.forceNavigation();
        },
        searchHotKey : function(e) {
            var thechar = String.fromCharCode(e.keyCode);
            /* si -> alt + p */
            if (e.altKey && e.keyCode == 80) {
                this.doBuscarItemEstrategico(e);
            }
        },
        forceNavigation : function() {
            $('#menu-gestion .dropdown-menu a').off('click.force').on('click.force', function() {
                var newFragment = Backbone.history.getFragment($(this).attr('href'));
                if (Backbone.history.fragment == newFragment) {
                    // need to null out Backbone.history.fragement because
                    // navigate method will ignore when it is the same as newFragment
                    Backbone.history.fragment = null;
                    Backbone.history.navigate(newFragment, true);
                }
            })
        },
        loadSeedData: function() {
            var self = this;
            this.itemEstrategicos = new ItemEstrategicoCollection();
            this.dependencias = new DependenciaOrganizacionalCollection();
            this.descripcionesNivel = new DescripcionNivelCollection();
            this.periodos = new PeriodoCollection();
            this.empleados = new EmpleadoCollection();
            var complete = _.invoke([
                this.itemEstrategicos,
                this.dependencias,
                this.descripcionesNivel,
                this.periodos,
                this.empleados
            ], 'fetch');
            /* el callback es el done de jquery ajax para cada elemento del arreglo */
            $.when.apply($, complete)
            .done(function() {
                // console.log('self.periodos.toJSON(): ', self.periodos.toJSON());
                self.render();
                // console.log('self.empleados.toJSON(): ', self.empleados.toJSON());
                self.empleadosJSON = self.empleados.toJSON();
                // console.log('self.empleadosJSON: ', self.empleadosJSON);
                /*self.searchList = _.chain(self.itemEstrategicos.toJSON())
                .filter(function(itemEstrategico) {
                    return itemEstrategico.descripcionItem != null
                }).value();
                */
                self.searchList = _.chain(self.itemEstrategicos.toJSON())
                .filter(function(itemEstrategico) {
                    return itemEstrategico.descripcionItem != null
                }).value();
                self.searchListString = _.pluck(self.searchList, 'descripcionItem');

                /*console.log('self.searchList: ', self.searchList);
                console.log('self.searchListString: ', self.searchListString);*/

                // console.log('self.searchList: ', self.searchList);
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        },
        loadSearchData : function() {
            var self = this;
            this.itemEstrategicos = new ItemEstrategicoCollection();
            var complete = _.invoke([
                this.itemEstrategicos,
            ], 'fetch');
            /* el callback es el done de jquery ajax para cada elemento del arreglo */
            $.when.apply($, complete)
            .done(function() {
                self.searchList = _.chain(self.itemEstrategicos.toJSON())
                .filter(function(itemEstrategico) {
                    return itemEstrategico.descripcionItem != null
                }).value();
                self.searchListString = _.pluck(self.searchList, 'descripcionItem');
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            });
        },
        events : {
            'click .add-new-item': 'doSaveItem',
            'click .hierachy': 'doShowHierachy',
            'click .remove-cur-detailed-item': 'doRemoveCurDetailedItem',
            'click #buscar-item-estrategico': 'doBuscarItemEstrategico',
        },
        doShowHierachy: function(e, level) {
            var curLevel, direction = 0, back = false;
            if (e) {
                if (modUtils.isNumeric(e)) {
                    curLevel = e;
                }
                else {
                    e.preventDefault();
                    curLevel = parseInt($(e.target).attr('data-nivel-id'));
                    while (this.curItemStack.length &&
                        curLevel <= this.curItemStack[this.curItemStack.length - 1].descripcionNivel.idNivel)
                        this.curItemStack.pop();
                    this.curItem = this.curItemStack.length
                    ? this.curItemStack[this.curItemStack.length - 1] : null;
                }
            } else {
                back = level ? true : false;
                curLevel = level ? level : this.curItem.descripcionNivel.idNivel;
                direction = 1;
            }
            var compiledTemplate = _.template(jerarquiaBreadcrumbTemplate, {
                levels: this.descripcionesNivel.models,
                curLevel : curLevel + direction,
            })
            this.$el.html(compiledTemplate);
            modCamposDinamicos.iniciarToolTips();
            if (this.curItemStack.length + 1 == curLevel + direction)
                this.options.appRouter.selectItem(this.getLiSelector(curLevel + direction));
            this.loadItemsByLevel(curLevel + direction, back);
        },
        getLiSelector: function(index) {
            var arr = ['', '#objetivos-estrategicos', '#objetivos-clave',
                        '#objetivos-especificos', '#iniciativas'];
            return this.curUrlDescripcion = arr[index];
        },
        doSaveItem: function(e, curItem) {
            var self = this;
            // console.log('curItem: ', curItem);
            var level =  e ? parseInt($(e.target).attr('data-level')) : curItem.descripcionNivel.idNivel;
            // console.log('level: ', level);
            // console.log('this.curItem: ', this.curItem);
            var idParentItem = e
                ? parseInt($(e.target).attr('data-item-estrategico-id'))
                : this.curItem ? this.curItem.idItemEstrategico : null;
            // console.log('idParentItem: ', idParentItem);
            // return false;

            // console.log('this.curItemStack: ', this.curItemStack);
            // console.log('curItem: ', curItem);
            // console.log('==========================""""""""""""""""""""""');
            // console.log('this.curItemStack: ', this.curItemStack);
            // console.log('level: ', level);

            if ((
                (!this.curItemStack.length && level != 1)
                || this.curItemStack.length + 1 != level
                || this.reload) && curItem ) {
                this.curItemStack = this.getItemStack(curItem);
                // console.log('this.curItemStack: ', this.curItemStack);
                this.curItem = this.curItemStack[this.curItemStack.length - 1];
                // console.log('entra en reload cur item stack y curitem');
                this.reload = true;
            }
            else {
                // console.log('se va al else');
            }
            var genericParams = {
                curItem: curItem,
                items : this.curItemStack,
                empleados : this.empleadosJSON,
                level: level,
            }
            // console.log('curItem: ', curItem);
            modItemEst.attachTemplateParamsByLevel(level, genericParams, this);

            modItemEst.customActionsBeforeHtmlTemplate(level, curItem, this);

            var compiledTemplate = _.template(modItemEst.getSaveItemEstTemplate(level), genericParams);

            $('#modal-area').html(compiledTemplate);

            modItemEst.customActionsAfterHtmlTemplate(level, curItem, this);
            /* todos los niveles tienen responsables */
            this.curResponsables = [];
            if (curItem && curItem.responsables && curItem.responsables.length) {
                this.curResponsables = _.pluck(curItem.responsables, 'idResponsable');
                this._redrawResponsables(curItem);
            }


            $('#myModal').modal().on('shown.bs.modal', function() {
                $(modItemEst.getFocusableSelector(level)).focus();
            });
            $('#modal-area ' + modItemEst.getButtonSaveSelector(level)).click(function() {
                var $curButtonSave = $(this);

                $('#item-estrategico-loading').show();
                $curButtonSave.addClass('disabled');

                var curItem = modItemEst.getItemEstrategicoModel({
                    level: level,
                    idParentItem: idParentItem,
                }, self, e ? true : false);
                var update = false;

                if ($('#idItemEstrategico').val().trim() != "") {
                    curItem.set('idItemEstrategico', $('#idItemEstrategico').val().trim());
                    update = true;
                }
                // console.log('curItem.toJSON(): ', curItem.toJSON());
                var hasMessages = modValidator.validar(
                    modItemEst.getInstanceValidator(level),
                    curItem.toJSON()
                );
                /* limpiamos los idItemEstrategico que sean nuevos para evitar que se guarden con update */
                if (curItem.get('itemEstrategicos') != null) {
                    curItem.set('itemEstrategicos',
                        _.map(curItem.get('itemEstrategicos'), function(itemEstrategico) {
                        if (!modUtils.isNumeric(itemEstrategico.get('idItemEstrategico'))) {
                            itemEstrategico.unset('idItemEstrategico');
                        }
                        return itemEstrategico;
                    }));
                }

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $('#item-estrategico-loading').hide();
                    $curButtonSave.removeClass('disabled');

                    $('#myModal ' + modItemEst.getButtonSaveSelector(level)).notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    $curButtonSave.off('click');
                    /* está validado entonces guardamos los cambios */
                    curItem.save({}, {
                        beforeSend : function(jqXHR, settings) {
                            // console.log('settings.data: ', settings.data);
                        },
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';

                            $.notify('se ha ' + message + ' '
                                + modItemEst.getItemDescription(level)
                                + ' exitosamente', 'success');

                            self.loadItemsByLevel(level);
                            $('#myModal').modal('hide');
                            self.loadSearchData();
                        },
                        error: function (model, response, options) {
                            $('#item-estrategico-loading').hide();
                            $curButtonSave.removeClass('disabled');
                            $.notify('ERROR conectandose con el servidor');
                            console.log('response: ', response);
                        }
                    })
                }
            })
        },
        doAddResponsable : function(e) {
            e.preventDefault();
            this._updateSelects(0, this.curEditItem);
        },
        doAddKeyuser : function(e) {
            e.preventDefault();
            var compiledTemplate = _.template(selectKeyuserTemplate);
            var idTemp = modUtils.uniqueID();
            $('#myModal .keyuser').html(compiledTemplate({
                keyusers : this.empleadosJSON,
                idEmpleado: 0,
                idTemp : idTemp
            }));
            modCamposDinamicos.iniciarChosen({
                selector: '.keyuser #select-keyuser',
                search_contains: true,
                placeholder: 'Elige un Keyuser',
            });
            $('.keyuser #select-keyuser').trigger('chosen:activate');
            this._addEventHandleRemoveKeyUser();
        },
        _addEventHandleRemoveKeyUser : function() {
            $('#myModal #remove-keyuser').off('click.removeKeyuser').on('click.removeKeyuser', function(e) {
                e.preventDefault();
                var $this = $(this);
                var idKeyuser = $this.attr('data-keyuser-id');
                if (modUtils.isNumeric(idKeyuser)) {
                    bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR A ESTE KEYUSER?</strong></h4>',
                    function(ok) {
                        if (!ok) return;
                        $('#myModal .keyuser').empty();
                    })
                }
                else {
                    $('#myModal .keyuser').empty();
                }
            });
        },
        doAddSupervisor : function(e) {
            e.preventDefault();
            var compiledTemplate = _.template(selectSupervisorTemplate);
            var idTemp = modUtils.uniqueID();
            $('#myModal .supervisor').html(compiledTemplate({
                empleados : this.empleadosJSON,
                idEmpleado: 0,
                idTemp : idTemp
            }));
            modCamposDinamicos.iniciarChosen({
                selector: '.supervisor #select-supervisor',
                search_contains: true,
                placeholder: 'Elige un Supervisor',
            });
            $('.supervisor #select-supervisor').trigger('chosen:activate');
            this._addEventHandleRemoveSupervisor();
        },
        _addEventHandleRemoveSupervisor : function() {
            $('#myModal #remove-supervisor').off('click.removeSup').on('click.removeSup', function(e) {
                e.preventDefault();
                var $this = $(this);
                var idSupervisor = $this.attr('data-supervisor-id');
                if (modUtils.isNumeric(idSupervisor)) {
                    bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR A ESTE SUPERVISOR?</strong></h4>',
                    function(ok) {
                        if (!ok) return;
                        $('#myModal .supervisor').empty();
                    })
                }
                else {
                    $('#myModal .supervisor').empty();
                }
            });
        },
        _redrawResponsables: function(curItem) {
            var self = this;
            $('#myModal .responsables').empty();

            $(this.curResponsables).each(function (index, idEmpleado) {
                self._updateSelects(idEmpleado, curItem);
            })
        },
        _updateSelects: function(idEmpleado, curItem) {
            var self = this;
            // console.log('this.curResponsables: ', this.curResponsables);
            var diff = _.difference(_.pluck(this.empleadosJSON, 'idEmpleado'), this.curResponsables);
            if (idEmpleado) diff.push(idEmpleado);
            var responsables = _.filter(this.empleadosJSON, function (empleado) {
                return _.find(diff, function(idEmpleado) { return idEmpleado == empleado.idEmpleado }) !== undefined;
            });
            var idTempResponsable = idEmpleado ? idEmpleado : modUtils.uniqueID();
            var compiledTemplate = _.template(selectResponsableTemplate);
            $('#myModal .responsables').append(compiledTemplate({
                responsables : responsables,
                idTempResponsable: idTempResponsable,
                idEmpleado: idEmpleado,
            }));

            modCamposDinamicos.iniciarChosen({
                selector: '.responsables #select-responsable-' + idTempResponsable,
                search_contains: true,
                placeholder: 'Elige un responsable',
            });

            $('#myModal #remove-responsable-' + idTempResponsable).click(function(e) {
                e.preventDefault();
                var $this = $(this);
                var idResponsable = $this.attr('data-responsable-id');
                if (modUtils.isNumeric(idResponsable) && curItem) {
                    if (self.curResponsables.length == 1) {
                        bootbox.alert('<h4 class="text-center"><strong>'+
                            'NO SE PUEDE QUITAR EL RESPONSABLE, '+
                            'SE REQUIERE AL MENOS UNO, PUEDE SELECCIONAR OTRO ' +
                            'DE LA LISTA DESPLEGABLE</strong></h4>');
                    }
                    else
                    bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR A ESTE RESPONSABLE?</strong></h4>',
                    function(ok) {
                        if (!ok) return;
                        self.curResponsables = _.without(self.curResponsables, parseInt(idResponsable));
                        self._redrawResponsables(curItem);
                    })
                }
                else {
                    self.curResponsables = _.without(self.curResponsables, parseInt(idResponsable));
                    self._redrawResponsables(curItem);
                }
            })

            $('#select_responsable_' + idTempResponsable + '_chosen input').focus(function() {
                var curVal = $('#myModal #select-responsable-' + idTempResponsable).val();
                if (modUtils.isNumeric(curVal))
                    self.prevIdResponsable = parseInt(curVal);
            });

            $('#myModal #select-responsable-' + idTempResponsable)
            .change(function() {
                if (self.prevIdResponsable)
                    self.curResponsables = _.without(self.curResponsables, self.prevIdResponsable);

                var idEmpleado = $(this).val();
                if (modUtils.isNumeric(idEmpleado)) {
                    self.curResponsables.push(parseInt(idEmpleado));
                    self._redrawResponsables();
                    $('#add-responsable').focus();
                }
            })
            /* si no existe empleado entonces enfocar (se está añadiendo uno nuevo) */
            if (!idEmpleado)
                $('#myModal #select-responsable-' + idTempResponsable).trigger('chosen:activate');
        },
        render: function() {
            this.doShowHierachy(this.options.level);
        },
        doGo: function(curItem) {
            if (this.curItemStack.length + 1 != curItem.descripcionNivel.idNivel) {
                this.curItemStack = this.getItemStack(curItem);
            }
            /* add the current element to the stack */
            this.curItemStack.push(curItem);
            this.curItem = curItem;
            this.doShowHierachy();
        },
        doEdit: function(curItem) {
            this.curEditItem = curItem;
            this.doSaveItem(null, curItem);
        },
        doDelete: function(curItem) {

            var self = this;
            var level = curItem.descripcionNivel.idNivel;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ' +
                    modItemEst.getItemDescription(level).toUpperCase() + '?</strong></h4>',
            function(ok) {

                if (!ok) return;

                var itemEstrategicoModel = new ItemEstrategicoModel({
                    idItemEstrategico : curItem.idItemEstrategico,
                });
                itemEstrategicoModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó ' +
                            modItemEst.getItemDescription(level) +
                            ' exitosamente', 'success');
                        self.loadItemsByLevel(level);
                        self.loadSearchData();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        loadItemsByLevel: function(level, back) {
            var self = this;
            // console.log('level: ', level);

            var tableTemplateParams = {
                newItemText : modItemEst.getCurButtonMessage(level),
                level : level,
                curItem : this.curItem ? this.curItem : null,
                curUrlDescripcion : this.curUrlDescripcion,
                modSecurity : modSecurity,
            };

            var compiledTemplate = _.template(
                                    modItemEst.getTableTemplate(level),
                                    tableTemplateParams
                                );

            this.$('#item-estrategico-container').html(compiledTemplate);
            this.generateDetailedDepth(level, back);
            // nivel 4
            // getGrandChildren true
            // parentId 2
            var itemEstrategicoModel = new ItemEstrategicoModel({}, {
                nivel: level,
                getGrandChildren: back,
                parentId: (this.curItem && this.curItem.idItemEstrategico)
                ? (back) ? this.curItem.idParentItem : this.curItem.idItemEstrategico : null,
            });
            var models = [itemEstrategicoModel];
            if (level == 4) {
                this.categorias = new CategoriaCollection();
                models.push(this.categorias);
            }

            var complete = _.invoke(models, 'fetch', {
                success: function(model, response, opts) {
                    if (model instanceof ItemEstrategicoModel) {
                        // console.log('response: ', response);
                        // console.log('response: ', response);
                        var config = modItemEst.getDataTableConfig(level, self);
                        self.curCollection = response;
                        modDataTables.showGrid(response, config);
                        self.$('.mostrar-por').html(
                            modItemEst.getMostrarPorCompiledTemplate(level)
                        );
                        if ((!back && self.curItem && level != 5) || level == 1) {
                            if (modSecurity.canCreate(self.curUrlDescripcion)) {
                                /* nos aseguramos que la iniciativa que queremos añadir
                                le corresponda al objetivo específico correcto */
                                if (level <= 3 ||
                                    (
                                        level == 4 &&
                                        (
                                            parametros.curGerencia.idDependenciaOrganizacional ==
                                            self.curItem.dependenciaOrganizacional.idDependenciaOrganizacional
                                        ||
                                        parametros.empleado.rol.abreviatura == "SUPER_ADMIN"
                                        )
                                    )
                                )
                                self.$('.add-item').html(
                                    modItemEst.getButtonSaveTemplate(
                                        level,
                                        tableTemplateParams
                                    )
                                );
                            }
                        }
                        else {
                            // console.log('no se puede crear :(');
                        }
                        modItemEst.attachEventHandlersMostrarPor(level, self, compiledTemplate, tableTemplateParams, back);
                        $('#mostrar-por button').last().click();
                    }
                },
                error: function(model, response, opts) {
                    console.log('ha ocurrido un error');
                    console.log('response: ', response);
                }
            });
        },
        generateDetailedDepth: function(level, back) {
            var curstack = modUtils.deepClone(this.curItemStack);
            var showClose = true;
            if (back) curstack.pop(), level--, showClose = false;
            var compiledTemplate = _.template(detailedDepthTemplate, {
                items: curstack,
                level: level,
                showClose: showClose,
            });
            this.$('#detailed-depth').html(compiledTemplate);
            modCamposDinamicos.iniciarToolTips();
        },
        doRemoveCurDetailedItem: function(e) {
            e.preventDefault();
            var curAnchor = this.$(e.target);
            // var dataIdItemEstrategico = parseInt(curAnchor.attr('data-id-item-estrategico'));
            var dataLevel = parseInt(curAnchor.attr('data-level'));
            curAnchor.closest('span').remove();
            this.curItemStack = [];
            /* quitamos el curitem */
            // this.curItem = null;
            this.doShowHierachy(null, dataLevel);
        },
        getGerencias: function() {
            var gerencias = _.filter(this.dependencias.toJSON(), function(jerarquia) {
                return jerarquia.idNivelJerarquico == 4 ||
                jerarquia.descripcionDependencia.indexOf('GERENCIA') != -1
            })
            return gerencias;
        },
        getSuperintendencias: function() {
            var self = this;
            var superintendencias = _.filter(this.dependencias.toJSON(), function(jerarquia) {
                return jerarquia.parentDependencia.idDependenciaOrganizacional ==
                self.curItem.dependenciaOrganizacional.idDependenciaOrganizacional;
            })
            return superintendencias;
        },
        getItemStack : function(curItem) {
            var itemEstrategicoModel = new ItemEstrategicoModel({}, {
                hierachy : curItem.idParentItem,
            });
            var curItemStack;
            itemEstrategicoModel.fetch({
                async: false,
                success: function(model, response, options) {
                    // console.log('response: ', response);
                    curItemStack = response;
                },
                error: function(model, response, options) {
                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('response: ', response);
                }
            });
            return curItemStack;
        },
        formatDataTableDetails: function(curMetrica) {
            return _.template(metricaDetailTemplate, {curMetrica : curMetrica});
        },
        formatDataTableSearchDetails: function(curItem) {
            return _.template(searchDetailItemEstrategicoTemplate, {curItem : curItem});
        },
        doSelectSearch: function(curItem) {
            if (curItem.descripcionNivel.idNivel != 1)
                this.curItemStack = this.getItemStack(curItem);
            else
                this.curItemStack = [];
            /* add the current element to the stack */
            this.curItemStack.push(curItem);
            this.curItem = curItem;
            this.doShowHierachy();
            $('#myModal3').modal('hide');
        },
        /* fuzzy, mejor implementación */
        doBuscarItemEstrategico: function(e) {
            e.preventDefault();
            var self = this;
            if (!this.compiledSearchTemplate) {
                modDataTablesSearchModal.datatable = null;
                this.compiledSearchTemplate = _.template(searchItemEstrategicoTemplate, {});
                $('#modal-area-3').html(this.compiledSearchTemplate);
            }
            var options = {
                pre: '<span class="highlight">',
                post: '</span>'
            }
            $('#myModal3').modal().on('shown.bs.modal', function() {
                $('#search-term').off('keyup.search').on('keyup.search', function(e) {
                    // console.log('e.keyCode: ', e.keyCode);
                    if (e.altKey || e.ctrlKey || e.shiftKey || e.keyCode == 18 ||
                        e.keyCode == 17 || e.keyCode == 13 || e.keyCode == 16
                        || e.keyCode == 20 || e.keyCode == 27 || e.keyCode == 9) return false;

                    var searchString = $(this).val();
                    $('#div-table-search-container').show();
                    var results = fuzzy.filter(searchString, self.searchListString, options);
                    var newCollection = _.map(results, function(result) {
                        self.searchList[result.index].descripcionItemResult = result.string;
                        return self.searchList[result.index];
                    });
                    if (modDataTablesSearchModal.datatable) {
                        modDataTablesSearchModal.datatable.clear();
                        if (newCollection.length > 0) {
                            modDataTablesSearchModal.datatable.rows.add(newCollection);
                        }
                        modDataTablesSearchModal.datatable.columns.adjust().draw();
                        modDataTablesSearchModal.prepararEdiciones();
                    }
                    else {
                        modDataTablesSearchModal
                            .showGrid(newCollection, searchItemEstrategicoConfig.getConfig(self));
                    }
                }).focus();
            }).on('hidden.bs.modal', function() {
                $('#div-table-search-container').hide();
                $('#search-term').val('');
            })
        }
    });

    return JerarquiaView;
});