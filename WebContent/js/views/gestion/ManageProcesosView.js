define([
        'underscore',
        'backbone',

        'mods/modUtils',
        'mods/DataTable/DataTable',
        'mods/validaciones/modValidator',
        'mods/validaciones/ProcesoValidator',

        'bootbox',
        'mods/modSecurity',

        'models/ProcesoModel',

        'text!templates/proceso/buttonSaveProcesoTemplate.html',
        'text!templates/proceso/manageProcesosTemplate.html',
        'text!templates/proceso/saveProcesoTemplate.html',
        'text!templates/proceso/columnaOpsProcesosTemplate.html',

        'views/gestion/ManageSubProcesosView',
    ], function(
        _,
        Backbone,

        modUtils,
        DataTable,
        modValidator,
        ProcesoValidator,

        bootbox,
        modSecurity,

        ProcesoModel,

        buttonSaveProcesoTemplate,
        manageProcesosTemplate,
        saveProcesoTemplate,
        columnaOpsProcesosTemplate,

        ManageSubProcesosView
    ) {

    var ManageProcesosView = Backbone.View.extend({
        el: '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            this.options = options || {};
            this.fetchProcesos();
            this.initPermisos();
        },
        fetchProcesos: function() {
            var self = this;
            (new ProcesoModel()).fetch({
                success: function (model, response, options) {
                    // console.log('response: ', response);
                    self.options.procesos = response;
                    self.render();
                },
                error: function (model, response, options) {
                    $.notify('ERROR conectandose con el servidor', 'error');
                    console.log('response: ', response);
                }
            })

        },
        initPermisos : function() {
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.events['click .add-new-proceso'] = 'doSaveProceso';
        },
        events: {
        },
        doSaveProceso: function(e, curProceso) {
            var update = e ? false : true;
            var self = this;
            var compiledTemplate = _.template(saveProcesoTemplate, {
                curProceso: curProceso,
            });

            $('#modal-area').html(compiledTemplate);
            $('#myModal').modal().on('shown.bs.modal', function() {
                $('#descripcion-proceso').focus();
            });
            $('#modal-area #registrar-proceso').click(function() {
                $(this).addClass('disabled');
                $('#proceso-loading').show();

                var curProceso = new ProcesoModel({
                    idProceso : $('#idProceso').val().trim(),
                    codProceso : $('#codProceso').val().trim(),
                    descripcion: $('#descripcion-proceso').val(),
                });

                var hasMessages = modValidator.validar(
                    new ProcesoValidator(
                        update,
                        curProceso.urlRoot,
                        curProceso.get('idProceso')
                    ),
                    curProceso.toJSON()
                );

                var valid = (hasMessages == true) ? hasMessages : false;

                if (!valid) {
                    $(this).removeClass('disabled');
                    $('#proceso-loading').hide();

                    $('#myModal #registrar-proceso').notify(
                        modValidator.showErrorMessages(hasMessages), {
                            position: 'left bottom',
                            className: 'error',
                        }
                    )
                }
                else {
                    if (!update)
                        curProceso.unset('idProceso');

                    $(this).off('click');
                    var el = this;
                    /* está validado entonces guardamos los cambios */
                    curProceso.save({}, {
                        success: function (model, response, options) {
                            var message = (update) ? 'actualizado' : 'creado';
                            $.notify('se ha ' + message + ' el proceso exitosamente', 'success');
                            self.fetchProcesos();
                            $('#myModal').modal('hide');
                        },
                        error: function (model, response, options) {
                            $('#proceso-loading').hide();
                            $(el).removeClass('disabled');

                            $.notify('ERROR conectandose con el servidor')
                        }
                    })
                }
            })
            return false;
        },
        doEdit: function(curProceso) {
            this.doSaveProceso(null, curProceso);
            return false;
        },
        doDelete: function(curProceso) {
            var self = this;
            bootbox.confirm('<h4 class="text-center">' +
                    '<strong>¿ESTÁS SEGURO DE QUITAR ESTA DESCRIPCIÓN DEL PROCESO?</strong></h4>',
            function(ok) {

                if (!ok) return;
                var procesoModel = new ProcesoModel({
                    idProceso : curProceso.idProceso,
                });
                procesoModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó la descripción del proceso exitosamente', 'success');
                        self.fetchProcesos();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        formatDataTableDetails : function(curItem) {
            // console.log('curItem.idProceso: ', curItem.idProceso);
            return '<div id="subprocesos-' +  curItem.idProceso +'"></div>';


        },
        onChildRowRendered : function(curItem) {
            return new ManageSubProcesosView({
                el : '#subprocesos-' + curItem.idProceso,
                idProcesoParent : curItem.idProceso
            });
        },
        render: function() {
            var self = this;
            modUtils.processCustomMessages();
            var compiledTemplate = _.template(manageProcesosTemplate, {
                curUrlDescripcion : this.curUrlDescripcion,
                modSecurity : modSecurity
            });
            this.$el.html(compiledTemplate);

            var config = {
                rowId : 'idProceso',
                hasDetails : true,
                formatDataTableDetails : _.bind(this.formatDataTableDetails, this),
                onChildRowRendered : _.bind(this.onChildRowRendered, this),
                container: '.data-table-container',
                $container: this.$('.data-table-container'),
                order : [[1, 'asc']],
                doEdit: _.bind(this.doEdit, this),
                doDelete: _.bind(this.doDelete, this),
                columns: [
                    {
                        sClass: 'details-control text-center',
                        orderable : false,
                        data : null,
                        defaultContent : '',
                        width: '8%',
                    },
                    {
                        data : 'descripcion',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                ],
            }
            if (modSecurity.canEdit(this.curUrlDescripcion) ||
                modSecurity.canDelete(this.curUrlDescripcion)) {
                config.columns.push({
                        data : 'idProceso',
                        width : '20%',
                        orderable : false,
                        sClass : (modSecurity.canEdit(this.curUrlDescripcion) &&
                        !modSecurity.canDelete(this.curUrlDescripcion)) ||
                        (!modSecurity.canEdit(this.curUrlDescripcion) &&
                        modSecurity.canDelete(this.curUrlDescripcion)) ? 'text-center' : ''
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsProcesosTemplate, {
                                modSecurity : modSecurity,
                                curUrlDescripcion : self.curUrlDescripcion,
                                idProceso : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                });
            }
            this.datatable = new DataTable();
            // console.log('this.options.procesos: ', this.options.procesos);
            this.datatable.showGrid(this.options.procesos, config);
            if (modSecurity.canCreate(this.curUrlDescripcion))
                this.$('.add-item').html(_.template(buttonSaveProcesoTemplate, {}));
        }
    });

    return ManageProcesosView;
});