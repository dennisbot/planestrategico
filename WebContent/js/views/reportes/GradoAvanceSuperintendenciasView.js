define([
    'underscore',
    'backbone',
    'collections/ParametrosCollection',
    'collections/BPINCollection',
    'mods/modCamposDinamicos',
    'highcharts',
    'drilldown',

    'mods/modSecurity',

    'text!templates/reportes/reporteIniciativasTemplate.html',
    'text!templates/reportes/optionSuperintendenciasTemplate.html',
    'text!templates/reportes/selectMesTemplate.html',
], function(
    _,
    Backbone,
    ParametrosCollection,
    BPINCollection,
    modCamposDinamicos,
    highcharts,
    drilldown,

    modSecurity,

    reporteIniciativasTemplate,
    optionSuperintendenciasTemplate,
    selectMesTemplate
    ) {

    var GradoAvanceSuperintendenciasView = Backbone.View.extend({
        el : '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            var self = this;
            this.parametros = new ParametrosCollection();
            this.parametros.on('sync', this.parse2render, this);
            this.parametros.fetchIniciativas({
                idDependencia : 99,
                queryType : 'month',
                param : 3
            });
            this.initPermisos();
        },
        initPermisos : function() {
            // if (modSecurity.canCreate(this.curUrlDescripcion))
            //     this.events['click .add-new-level'] = 'doSaveTipoMetrica';
        },
        /* el nombre siempre será onClose cuando se quiera liberar espacio de memoria */
        onClose : function() {
            /* opcional, si se usa el mismo contenedor se sobreescribirá con su nuevo anfitrión */
            this.$el.empty();
        },
        parse2render : function() {
            var params = this.parametros.first();
            this.params = {
                dependencias : params.get('dependencias'),
                iniciativas : params.get('iniciativas'),
                periodos : params.get('periodos'),
            }
            this.render();
            // console.log('this.params: ', this.params);
        },
        getPeriodos : function() {

        },
        clearContainers : function() {
            self.$('#chart-container').empty();
        },
        prepareFields : function() {
            var self = this;

            modCamposDinamicos.iniciarChosen({
                selector: '.chosen-select',
                search_contains: true
            });

            modCamposDinamicos.iniciarChosen({
                selector: '#select-periodo',
                search_contains: true
            });

            self.$('#select-periodo').change(function() {
                self.mostrarChart();
            })

            this.$('#mostrar-ini-por').change(function() {
                var opcion = $(this).val();
                // console.log('opcion: ', opcion);
                if (opcion == 'xgerencias') {
                    self.$('#row-gerencias').show();
                    var idGerencia = self.$('#select-gerencias').val();
                    self.$('#select-superintendencias').html(_.template(optionSuperintendenciasTemplate, {
                        superintendencias : self.getSuperintendencias({idDependenciaOrganizacional : idGerencia})
                    }));
                    self.$('#select-superintendencias').trigger('chosen:updated').trigger('chosen:activate');
                }
                else {
                    // es por superintendencias
                    self.$('#row-gerencias').hide();
                    self.$('#select-superintendencias').html(_.template(optionSuperintendenciasTemplate, {
                        superintendencias : self.getSuperintendencias()
                    }));
                    self.$('#select-superintendencias').trigger('chosen:updated').trigger('chosen:activate');
                }
            });

            this.$('#select-gerencias').change(function() {
                var idGerencia = $(this).val();
                // console.log('idGerencia: ', idGerencia);
                self.$('#select-superintendencias').html(_.template(optionSuperintendenciasTemplate, {
                    superintendencias : self.getSuperintendencias({idDependenciaOrganizacional : idGerencia})
                }));
                self.$('#select-superintendencias').trigger('chosen:updated').trigger('chosen:activate');
            });

            this.$('#select-superintendencias').change(function() {
                self.mostrarChart();
            })

            this.$('#show-chart-by').change(function() {
                self.$('#select-mes-container').show();
                self.mostrarChart();
            })
        },
        mostrarChart : function() {
            var self = this;
            var mostrarx = $('#show-chart-by').val();
            self.clearContainers();
            var idDependenciaOrganizacional = self.$('#select-superintendencias').val();
            var descripcionDependencia = self.$('#select-superintendencias')
                                                        .find('option:selected').text();

            if (!idDependenciaOrganizacional) return true;
            switch(mostrarx) {
                case 'ytd':
                    self.$('#select-mes-container').hide();
                    self.showChartBy({
                        idDependenciaOrganizacional : idDependenciaOrganizacional,
                        descripcionDependencia : descripcionDependencia,
                        mes : null,
                        mostrarx : mostrarx,
                        title : '% DE AVANCE DE INICIATIVAS YTD'
                    })
                    break;
                case 'xmes':
                case 'xmes_acumulado':
                    var mes = self.$('#select-periodo').val();
                    var textMonth = self.$('#select-periodo').find('option:selected').text();
                    if (!mes) break;
                    self.showChartBy({
                        idDependenciaOrganizacional : idDependenciaOrganizacional,
                        descripcionDependencia : descripcionDependencia,
                        mes : mes,
                        mostrarx : mostrarx,
                        title : '% DE AVANCE DE INICIATIVAS ' +
                        (mostrarx != 'xmes' ? 'ACUMULADAS ' : '') +
                        'AL MES DE ' + textMonth
                    })
                    break;
                case 'anual':
                    self.$('#select-mes-container').hide();
                    self.showChartBy({
                            idDependenciaOrganizacional : idDependenciaOrganizacional,
                            descripcionDependencia : descripcionDependencia,
                            mes : null,
                            mostrarx : mostrarx,
                            title : '% DE AVANCE DE INICIATIVAS ANUAL'
                        })
                    break;
            }
        },
        showChartBy : function(options) {
            // console.log('showChartByMonth');
            this.showChart(
                this.map(
                    this.getIniciativas({
                        idDependenciaOrganizacional : options.idDependenciaOrganizacional,
                        queryType : options.mostrarx,
                        param : options.mes
                    })
                ),
                {
                    type : 'bar',
                    title : options.title,
                    subtitle : {
                        text :  'PARA LA <strong>' + options.descripcionDependencia + '</strong>'
                    }
                }
            );
        },
        map : function(iniciativasJSON) {
            return _.map(iniciativasJSON, function(iniJSON) {
                var color = iniJSON.gradoAvanceMensual >= 0 && iniJSON.gradoAvanceMensual <= .15 ? '#FF0000' :
                iniJSON.gradoAvanceMensual > .15 && iniJSON.gradoAvanceMensual < .5 ? '#FF8C00' :
                iniJSON.gradoAvanceMensual >= .5 && iniJSON.gradoAvanceMensual < 1 ? 'yellow' : '#7FFF00';
                return {
                    name : iniJSON.descripcionItem,
                    y : iniJSON.gradoAvanceMensual * 100,
                    color :  color,
                    drilldown : null
                }
            });
        },
        showChart : function(data, options) {
            // console.log('data: ', data);
            // console.log('options: ', options);
            // console.log('data: ', data);
            this.$('#chart-container').highcharts({
            // this.$el.highcharts({
                chart: {
                    type: options.type
                },
                title: {
                    text: options.title,
                },
                subtitle: {
                    text: options.subtitle.text
                },
                xAxis: {
                    type: 'category',
                    labels : {
                        // rotation : -45,
                        style : {
                            fontSize: '1em',
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: '% DE AVANCE DE INICIATIVAS'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:black">{point.name}</span>: avance de <b>{point.y:.2f}%</b><br/>'
                },

                series: [{
                    name: "INICIATIVA",
                    colorByPoint: true,
                    data: data
                }]
            });
        },
        getGerencias : function() {
            var gerencias = _.filter(this.params.dependencias, function(dependencia) {
                return dependencia.nivelJerarquico.nivel == 4
            })
            return gerencias;
        },
        getSuperintendencias : function(gerencia) {
            var superintendencias = _.filter(this.params.dependencias, function(dependencia) {
                return (gerencia)
                ? dependencia.parentDependencia.idDependenciaOrganizacional == gerencia.idDependenciaOrganizacional
                : dependencia.nivelJerarquico.nivel == 5;
            })
            return superintendencias;
        },
        getIniciativas : function(options) {
            var iniciativas = new BPINCollection();
            options.async = false;
            options.beforeSend = function(jqXHR, settings) {
                // console.log('settings: ', settings);
            }
            iniciativas.fetchIniciativas(options)
            var inis = _.sortBy(iniciativas.toJSON(), 'gradoAvanceMensual');
            // console.log('inis: ', inis);
            return inis;
        },
        render : function() {
            var self = this;
            var gerencias = this.getGerencias();
            var superintendencias = this.getSuperintendencias(gerencias[0]);
            var selectMonth = _.template(selectMesTemplate, { periodos : self.params.periodos });

            this.$el.html(_.template(reporteIniciativasTemplate, {
                gerencias : gerencias,
                superintendencias : superintendencias,
                selectMonth : selectMonth
            }));
            // console.log('this.getBPin(): ', this.getBPin());
            // console.log('this.map(this.getBPin()): ', this.map(this.getBPin()));
            this.prepareFields();
            // this.showChart(this.map(this.parametros.toJSON()), {
            //     type : 'bar',
            //     title : 'Top 10 de Superintendencias YTD'
            // });
        }
    })

    return GradoAvanceSuperintendenciasView;
});