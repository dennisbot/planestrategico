define([
    'underscore',
    'backbone',
    'collections/ParametrosCollection',
    'collections/BPINCollection',
    'mods/modCamposDinamicos',
    'highcharts',
    'drilldown',

    'mods/modSecurity',

    'text!templates/reportes/reporteGerenciasTemplate.html',
    'text!templates/reportes/selectMesTemplate.html',
], function(
    _,
    Backbone,
    ParametrosCollection,
    BPINCollection,
    modCamposDinamicos,
    highcharts,
    drilldown,

    modSecurity,

    reporteGerenciasTemplate,
    selectMesTemplate
    ) {

    var GradoAvanceGerenciasView = Backbone.View.extend({
        el : '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            var self = this;
            this.parametros = new ParametrosCollection();
            this.parametros.on('sync', this.parse2render, this);
            this.parametros.fetchIniciativas({
                idDependencia : 99,
                queryType : 'month',
                param : 3
            });
            this.initPermisos();
        },
        initPermisos : function() {
            // if (modSecurity.canCreate(this.curUrlDescripcion))
            //     this.events['click .add-new-level'] = 'doSaveTipoMetrica';
        },
        /* el nombre siempre será onClose cuando se quiera liberar espacio de memoria */
        onClose : function() {
            /* opcional, si se usa el mismo contenedor se sobreescribirá con su nuevo anfitrión */
            this.$el.empty();
        },
        parse2render : function() {
            var params = this.parametros.first();
            this.params = {
                dependencias : params.get('dependencias'),
                iniciativas : params.get('iniciativas'),
                periodos : params.get('periodos'),
            }
            this.render();
            // console.log('this.params: ', this.params);
        },
        getPeriodos : function() {

        },
        clearContainers : function() {
            self.$('#chart-container').empty();
        },
        prepareFields : function() {
            var self = this;

            modCamposDinamicos.iniciarChosen({
                selector: '.chosen-select',
                search_contains: true
            });

            modCamposDinamicos.iniciarChosen({
                selector: '#select-periodo',
                search_contains: true
            })
            self.$('#select-periodo').change(function() {
                self.mostrarChart();
            })
            this.$('#select-gerencias').change(function() {
                self.mostrarChart();
            });
            this.$('#show-chart-by').change(function() {
                self.mostrarChart();
            })
        },
        mostrarChart : function() {
            var self = this;
            var mostrarx = self.$('#show-chart-by').val();
            self.clearContainers();
            var idDependenciaOrganizacional = self.$('#select-gerencias').val();
            var descripcionDependencia = self.$('#select-gerencias')
                                                        .find('option:selected').text();

                if (!idDependenciaOrganizacional) return true;

                switch(mostrarx) {
                    case 'ytd':
                        self.$('#row-select-mes-container').hide();
                        self.showChartBy({
                            idDependenciaOrganizacional : idDependenciaOrganizacional,
                            descripcionDependencia : descripcionDependencia,
                            mes : null,
                            mostrarx : mostrarx,
                            title : 'TOP 10 Avance por Objetivos Específicos YTD'
                        })
                        break;
                    case 'xmes':
                    case 'xmes_acumulado':
                        self.$('#row-select-mes-container').show();

                        var $this = self.$('#select-periodo');
                        var mes = $this.val();

                        if (!mes) break;

                        var textMonth = $this.find('option:selected').text();
                        textMonth = textMonth.charAt(0).toUpperCase() + textMonth.slice(1).toLowerCase();
                        self.showChartBy({
                            idDependenciaOrganizacional : idDependenciaOrganizacional,
                            descripcionDependencia : descripcionDependencia,
                            mes : mes,
                            mostrarx : mostrarx,
                            title : 'TOP 10 Avance por Objetivos Específicos ' +
                            (mostrarx == 'xmes' ? 'para el mes de ' +
                                textMonth : 'acumulado hasta el mes de ' + textMonth)
                        })
                        break;
                    case 'anual':
                        self.$('#row-select-mes-container').hide();
                        self.showChartBy({
                                idDependenciaOrganizacional : idDependenciaOrganizacional,
                                descripcionDependencia : descripcionDependencia,
                                mes : null,
                                mostrarx : mostrarx
                            })
                        break;
                }
        },
        showChartBy : function(options) {
            // console.log('showChartByMonth');
            this.showChart(
                this.map(
                    this.getObjetivosEspecificos({
                        idDependenciaOrganizacional : options.idDependenciaOrganizacional,
                        queryType : options.mostrarx,
                        param : options.mes
                    })
                ),
                {
                    type : 'bar',
                    title : options.title,
                    subtitle : {
                        text :  'PARA LA <strong>' + options.descripcionDependencia + '</strong>'
                    }
                }
            );
        },
        map : function(OEspecificoJSON) {
            return _.map(OEspecificoJSON, function(oespecifico) {
                var color = oespecifico.bpin >= 0 && oespecifico.bpin <= .15 ? '#FF0000' :
                oespecifico.bpin > .15 && oespecifico.bpin < .5 ? '#FF8C00' :
                oespecifico.bpin >= .5 && oespecifico.bpin < 1 ? 'yellow' : '#7FFF00';
                return {
                    name : oespecifico.itemEstrategico.descripcionItem,
                    y : oespecifico.bpin * 100,
                    color :  color,
                    drilldown : null
                }
            });
        },
        showChart : function(data, options) {
            // console.log('data: ', data);
            // console.log('options: ', options);
            // console.log('data: ', data);
            this.$('#chart-container').highcharts({
            // this.$el.highcharts({
                chart: {
                    type: options.type
                },
                title: {
                    text: options.title,
                },
                subtitle: {
                    text: options.subtitle.text
                },
                xAxis: {
                    type: 'category',
                    labels : {
                        // rotation : -45,
                        style : {
                            fontSize: '1em',
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: '% DE AVANCE DE INICIATIVAS POR OBJETIVOS ESPECÍFICOS'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:black">{point.name}</span>: avance de <b>{point.y:.2f}%</b><br/>'
                },

                series: [{
                    name: "OBJETIVO ESPECÍFICO",
                    colorByPoint: true,
                    data: data
                }]
            });
        },
        getGerencias : function() {
            var gerencias = _.filter(this.params.dependencias, function(dependencia) {
                return dependencia.nivelJerarquico.nivel == 4 ||
                dependencia.descripcionDependencia.indexOf('GERENCIA') != -1
            })
            return gerencias;
        },
        getObjetivosEspecificos : function(options) {
            var objetivosEspecificos = new BPINCollection();
            options.async = false;
            options.beforeSend = function(jqXHR, settings) {
                // console.log('settings: ', settings);
            }
            objetivosEspecificos.fetchObjetivosEspecificos(options);
            var objEspecificos = _.sortBy(objetivosEspecificos.toJSON(), 'gradoAvanceMensual');
            // console.log('objEspecificos: ', objEspecificos);
            return objEspecificos;
        },
        render : function() {
            var self = this;
            var gerencias = this.getGerencias();
            var selectMonth = _.template(selectMesTemplate, { periodos : self.params.periodos });
            this.$el.html(_.template(reporteGerenciasTemplate, {
                gerencias : gerencias,
                selectMonth : selectMonth
            }));
            // console.log('this.getBPin(): ', this.getBPin());
            // console.log('this.map(this.getBPin()): ', this.map(this.getBPin()));
            this.prepareFields();
            this.mostrarChart();
            // this.showChart(this.map(this.parametros.toJSON()), {
            //     type : 'bar',
            //     title : 'Top 10 de Superintendencias YTD'
            // });
        }
    })

    return GradoAvanceGerenciasView;
});