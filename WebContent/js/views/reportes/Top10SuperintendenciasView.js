define([
    'underscore',
    'backbone',
    'collections/PeriodoCollection',
    'collections/BPINCollection',
    'mods/modCamposDinamicos',
    'highcharts',
    'drilldown',

    'mods/modSecurity',


    'text!templates/reportes/supChartTemplate.html',
    'text!templates/reportes/selectMesTemplate.html',
], function(
    _,
    Backbone,
    PeriodoCollection,
    BPINCollection,
    modCamposDinamicos,
    highcharts,
    drilldown,

    modSecurity,

    supChartTemplate,
    selectMesTemplate
    ) {

    var Top10SuperintendenciasView = Backbone.View.extend({
        el : '#container',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            $('#custom-css').empty();
            var self = this;
            this.periodos = new PeriodoCollection();
            this.bpins = new BPINCollection([], {
                filter : {
                    name : 'superintendencias',
                    type : 'ytd'
                }
            });
            var models = [this.periodos, this.bpins];
            var complete = _.invoke(models, 'fetch');
            $.when.apply($, complete)
            .done(function() {
                self.render();
                // console.log('self.periodos.toJSON(): ', self.periodos.toJSON());
                // console.log('self.bpins.toJSON(): ', self.bpins.toJSON());
            })
            .fail(function(response) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('response: ', response);
            })
            this.initPermisos();
        },
        initPermisos : function() {
            // if (modSecurity.canCreate(this.curUrlDescripcion))
            //     this.events['click .add-new-level'] = 'doSaveTipoMetrica';
        },
        /* el nombre siempre será onClose cuando se quiera liberar espacio de memoria */
        onClose : function() {
            /* opcional, si se usa el mismo contenedor se sobreescribirá con su nuevo anfitrión */
            this.$el.empty();
        },
        getPeriodos : function() {

        },
        clearContainers : function() {
            self.$('#select-mes-container').empty();
            self.$('#chart-container').empty();
        },
        prepareFields : function() {
            var self = this;
            modCamposDinamicos.iniciarChosen({
                selector: '.chosen-select',
                search_contains: true
            });
            $('#show-chart-by').change(function() {
                var mostrarx = $(this).val();
                self.clearContainers();
                switch(mostrarx) {
                    case 'ytd':
                        self.showChart(self.getBPin({
                            name : 'superintendencias',
                            type : 'ytd'
                        }), {
                            type : 'bar',
                            title : 'Top 10 de Superintendencias YTD'
                        });
                        break;
                    case 'xmes':
                        var compiledTemplate = _.template(selectMesTemplate, {
                            periodos : self.periodos.toJSON()
                        });
                        self.$('#select-mes-container').html(compiledTemplate);
                        modCamposDinamicos.iniciarChosen({
                            selector: '#select-periodo',
                            search_contains: true
                        })
                        self.$('#select-periodo').change(function() {
                            var $this = $(this);
                            var mes = $this.val();
                            var textMonth = $this.find('option:selected').text();
                            textMonth = textMonth.charAt(0).toUpperCase() + textMonth.slice(1).toLowerCase();
                            self.showChart(self.getBPin({
                                name : 'superintendencias',
                                type : 'month',
                                param: mes
                            }), {
                                type : 'bar',
                                title : 'Top 10 de Superintendencias para el Mes de ' + textMonth

                            });
                        })
                        break;
                    case 'anual':
                        self.showChart(self.getBPin({
                            name : 'superintendencias',
                            type : 'anual'
                        }), {
                            type : 'bar',
                            title : 'Top 10 de Superintendencias Anual'
                        });
                        break;
                }
            })
        },
        getBPin : function(options) {
            this.bpins = new BPINCollection([], {
                filter : {
                    name : options.name,
                    type : options.type,
                    param : options.param
                }
            });
            this.bpins.fetch({
                async: false,
                beforeSend : function() {
                    $('.miloading').show();
                },
                success: function() {
                    $('.miloading').hide();
                }
            })
            return this.map(this.bpins.toJSON());
        },
        map : function(bpinsJSON) {
            return _.map(bpinsJSON, function(bpins) {
                var color = bpins.bpin >= 0 && bpins.bpin <= .15 ? '#FF0000' :
                bpins.bpin > .15 && bpins.bpin < .5 ? '#FF8C00' :
                bpins.bpin >= .5 && bpins.bpin < 1 ? 'yellow' : '#7FFF00';
                return {
                    name : bpins.itemEstrategico.descripcionItem,
                    y : bpins.bpin * 100,
                    color : color,
                    drilldown : null
                }
            });
        },
        showChart : function(data, options) {
            // console.log('data: ', data);
            this.$('#chart-container').highcharts({
            // this.$el.highcharts({
                chart: {
                    type: options.type
                },
                title: {
                    text: options.title,
                },
                subtitle: {
                    text: 'Indice de Desempeño de Negocio (BPIn)'
                },
                xAxis: {
                    type: 'category',
                    labels : {
                        // rotation : -45,
                        style : {
                            fontSize: '1em',
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: 'Total percent BPIn'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:black">{point.name}</span>: BPIn de <b>{point.y:.2f}%</b><br/>'
                },

                series: [{
                    name: "SUPERINTENDENCIA",
                    colorByPoint: true,
                    data: data
                }]
            });
        },
        render : function() {
            this.$el.html(_.template(supChartTemplate, {
                header : 'SUPERINTENDENCIAS'
            }));
            // console.log('this.getBPin(): ', this.getBPin());
            // console.log('this.map(this.getBPin()): ', this.map(this.getBPin()));
            this.prepareFields();
            this.showChart(this.map(this.bpins.toJSON()), {
                type : 'bar',
                title : 'Top 10 de Superintendencias YTD'
            });
        }
    })

    return Top10SuperintendenciasView;
});