define([
    'underscore',
    'backbone',
    'models/ArchivoModel',
    'models/ItemEstrategicoModel',
    'collections/ArchivoCollection',

    'mods/modDataTablesArchivo',
    'mods/modCamposDinamicos',
    'mods/modUtils',
    'bootbox',

    'config/archivoConfig',

    'text!templates/gestion/niveles/iniciativas/control/archivo/verAdjuntosTemplate.html',
    'text!templates/gestion/niveles/iniciativas/control/archivo/dataTableArchivosTemplate.html',
], function(
    _,
    Backbone,
    ArchivoModel,
    ItemEstrategicoModel,
    ArchivoCollection,

    modDataTablesArchivo,
    modCamposDinamicos,
    modUtils,
    bootbox,

    archivoConfig,

    verAdjuntosTemplate,
    dataTableArchivosTemplate
    ) {

    var ArchivoView = Backbone.View.extend({
        el : '#modal-area',
        initialize: function(options) {
            this.curUrlDescripcion = '#' + Backbone.history.getFragment();
            // $('#custom-css').empty();
            modUtils.extractToContext(options, this);
            var self = this;
            // this.$el.show();
            this.archivos = new ArchivoCollection([], {
                idItemEstrategico : self.meta.idItemEstrategico,
            });
            var complete = _.invoke([this.archivos], 'fetch');
            $.when.apply($, complete)
            .done(function(model, response, options) {
                // console.log('model: ', model);
                // console.log('response: ', response);
                // console.log('options: ', options);
                self.render();
            })
            .fail(function(model, response, options) {
                $.notify('ERROR conectandose con el servidor', 'error');
                console.log('error');
            })
            this.initPermisos();
        },
        /* el nombre siempre será onClose cuando se quiera liberar espacio de memoria */
        onClose : function() {
            this.$el.empty();
        },
        initPermisos : function() {
        },
        showGrid: function(archivosJSON) {
            var self = this;
            /* creamos la tabla que contendrá los archivos listados */
            this.$('#archivo-container').html(dataTableArchivosTemplate);

            if (archivosJSON)
                modDataTablesArchivo.showGrid(archivosJSON, archivoConfig.getConfig(this));
            else {
                this.archivos = new ArchivoCollection([], {
                    idItemEstrategico : this.meta.idItemEstrategico,
                });
                var complete = _.invoke([this.archivos], 'fetch');
                $.when.apply($, complete)
                .done(function(model, response, options) {
                    // console.log('model: ', model);
                    // console.log('response: ', response);
                    // console.log('options: ', options);
                    var config = archivoConfig.getConfig(self);
                    modDataTablesArchivo.showGrid(self.archivos.toJSON(), config);
                    config.$container.css('visibility', 'visible');
                })
                .fail(function() {
                    $.notify('ERROR conectandose al servidor', 'error');
                })
            }
        },
        doDeleteFile: function(curFile) {
            // console.log('doDeleteFile');
            // console.log('curFile: ', curFile);
            var self = this;
            bootbox.confirm('<h5 class="text-center">' +
            '¿ESTÁS SEGURO DE ELIMINAR EL ARCHIVO <strong>' + curFile.theFileName +
            '</strong>?</h5>', function(ok) {
                if (!ok) return;
                var archivoModel = new ArchivoModel({
                    idArchivo : curFile.idArchivo,
                })
                archivoModel.destroy({
                    success: function(model, response, options) {
                        $.notify('Se eliminó el archivo exitosamente', 'success');
                        self.showGrid();
                    },
                    error: function(model, response, options) {
                        $.notify('ERROR conectandose con el servidor', 'error');
                        console.log('ocurrió un error');
                        console.log('response: ', response);
                    }
                })
            })
        },
        getPeriodoServer : function() {
            return this.serverDate.getMonth() + 1;
        },
        getPeriodoMeta : function() {
            return this.meta.periodo.mes;
        },
        canShowUploadLink : function() {
            /* añadir esta linea más cuando se quiera restringir la carga de metas por periodos (flexible por ahora) */
            // return this.getPeriodoMeta() == this.getPeriodoServer() &&
            return this.parentContext.canEditIniciativa(this.iniciativa.dependenciaOrganizacional.idDependenciaOrganizacional)
            // || true;
        },
        iniciarEditInplaceComentario : function() {
            var meta = this.meta;
            modCamposDinamicos.iniciarEditInPlace({
                args : {
                    error_sink: function (idOfEditor, errorString) {
                        console.log('hubo un error: ', idOfEditor);
                        console.log(errorString);
                    },
                    callback: function (element_id, update_value, original_value, settings) {
                        // console.log('settings.default_text: ', settings.default_text);
                        // console.log('element_id: ', element_id);
                        // console.log('update_value: ', update_value);
                        // console.log('original_value: ', original_value);
                        // console.log('meta.idItemEstrategico: ', meta.idItemEstrategico);

                        if (update_value == settings.default_text) return original_value;

                        var metaModel = new ItemEstrategicoModel({
                            idItemEstrategico : meta.idItemEstrategico,
                        });
                        metaModel.fetch({
                            async: false,
                            success : function (model, response, options) {
                                metaModel.set('comentarios', update_value)
                                metaModel.save({}, {
                                    async: false,
                                    success: function (model, response, options) {
                                        self.$('#meta-comentario-' + meta.idItemEstrategico).notify('guardado!', {
                                            className : 'success',
                                            position: 'top right',
                                        });
                                    },
                                    error : function (model, response, options) {
                                        update_value = original_value;
                                        self.$('#meta-comentario-' + meta.idItemEstrategico).notify(
                                            'ERROR conectándose con el servidor', {
                                                className : 'error',
                                                position: 'top right',
                                        });
                                        console.log('response: ', response);
                                    }
                                })
                            }
                        })
                        meta.comentarios = update_value;
                        return update_value;
                    },
                    default_text: 'Haga click aquí para añadir comentarios (presione tab para confirmar)',
                    value_required: true,
                    field_type: 'textarea',
                    textarea_rows : 2,
                    inputNameAndClass : 'name="inplace_value" ' +
                    'style="text-transform:lowercase" class="form-control inplace_field" '
                },
                // selector : '#meta-comentario-' + meta.idItemEstrategico,
                selector : '.editinplace',
                inputInplaceFieldSelector : 'textarea.inplace_field',
                onfocus: function() {
                    $(this).closest('p').css({padding: 0});
                    $(this).css({
                        'border-style': 'none',
                        'border-width': '0',
                        // 'background-color': '#f9f9f9',
                        'background-color': '#ffffdd',
                        'box-shadow': 'inset 0 1px 2px rgba(0,0,0,.3)',
                        '-webkit-box-shadow': 'inset 0 1px 2px rgba(0,0,0,.3)',
                        'padding': '7px 10px',
                        'margin': '0',
                    });
                },
                onblur: function() {
                    $(this).closest('p').css({padding: '7px 0px 10px 10px'});
                }
            })
        },
        iniciarUploaderArchivo : function() {
            var meta = this.meta;
            var iniciativa = this.iniciativa;
            if (this.canShowUploadLink()) {
                modCamposDinamicos.iniciarUploader({
                    selector: '#fine-uploader-meta-' + meta.idItemEstrategico,
                    endpoint: 'upload/' + meta.idItemEstrategico
                    + '/' + iniciativa.idItemEstrategico,
                    text: {
                        uploadButton:
                        '<i class="fa fa-upload fa-lg" style="margin-right:15px">' +
                        '</i> Subir Archivo y/o Evidencia (<strong>Máximo 10MB de tamaño</strong>)'
                    },
                    callback : _.bind(this.showGrid, this),
                    validation : {
                        sizeLimit : 1024 * 1024 * 10, // 5MB
                        allowedExtensions: []
                    }
                    // context : self
                });
            }
        },
        render : function() {
            var self = this;
            this.$el.html(_.template(verAdjuntosTemplate, {
                showUploadLink : this.canShowUploadLink(),
                meta : this.meta,
                iniciativa : this.iniciativa,
            }))
            this.iniciarEditInplaceComentario();
            this.iniciarUploaderArchivo();
            this.showGrid(this.archivos.toJSON());
            var config = archivoConfig.getConfig(this);
            this.$('#myModal').modal().on('shown.bs.modal', function() {
                modDataTablesArchivo.datatable.columns.adjust().draw();
                config.$container.css('visibility', 'visible');
            });
        }
    })

    return ArchivoView;
});