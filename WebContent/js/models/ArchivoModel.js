define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var ArchivoModel = Backbone.Model.extend({
        urlRoot: 'rest/archivo',
        idAttribute: "idArchivo",
        defaults: {
            'theFileName': null,
            'theFileSize': 0,
            'theContentType': null,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return ArchivoModel;
});