define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var DependenciaOrganizacionalModel = Backbone.Model.extend({
        urlRoot: 'rest/dependenciaOrganizacional',
        idAttribute: "idDependenciaOrganizacional",
        defaults: {
            'descripcion': null,
            'abreviatura': null,
            'nivel': 0,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return DependenciaOrganizacionalModel;
});