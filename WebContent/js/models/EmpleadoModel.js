define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var EmpleadoModel = Backbone.Model.extend({
        urlRoot: 'rest/empleado',
        idAttribute: "idEmpleado",
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return EmpleadoModel;
});