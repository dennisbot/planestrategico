define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var CategoriaModel = Backbone.Model.extend({
        urlRoot: 'rest/categoria',
        idAttribute: "idCategoria",
        defaults: {
            'descripcionCategoria': null,
            'abreviaturaCategoria': null,
            'peso': 0,
            'nivel': null,
            'significancia': '',
            'subcategorias': null,
            'idCategoriaParent': null,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return CategoriaModel;
});