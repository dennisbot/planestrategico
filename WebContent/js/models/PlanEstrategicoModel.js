define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var PlanEstrategicoModel = Backbone.Model.extend({
        urlRoot: 'rest/planEstrategico',
        idAttribute: "idPlanEstrategico",
        defaults: {
            'estado': 1,
            'nombrePlan': null,
            'anio': null,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return PlanEstrategicoModel;
});