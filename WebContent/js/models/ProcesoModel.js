define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var ProcesoModel = Backbone.Model.extend({
        urlRoot: 'rest/proceso',
        idAttribute: "idProceso",
        defaults: {
            'descripcion': null
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
        fetchSubProcesos : function(options) {
            if (options && options.idProcesoParent)
                options.url = this.urlRoot + '/subprocesos/' + options.idProcesoParent;
            // console.log('options.url: ', options.url);
            return Backbone.Model.prototype.fetch.call(this, options);
        },
        saveSubProceso : function(attributes, options) {
            if (options && options.idProcesoParent)
                options.url = this.urlRoot + '/subprocesos/' + options.idProcesoParent;
            // console.log('saveSubProceso -> options.url: ', options.url);
            return Backbone.Model.prototype.save.call(this, attributes, options);
        }

    });
    // Return the model for the module
    return ProcesoModel;
});