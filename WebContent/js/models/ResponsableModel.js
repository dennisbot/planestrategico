define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var ResponsableModel = Backbone.Model.extend({
        urlRoot: 'rest/responsable',
        idAttribute: "idResponsable_idItemEstrategico",
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return ResponsableModel;
});