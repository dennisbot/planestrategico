define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var TipoMetricaModel = Backbone.Model.extend({
        urlRoot: 'rest/tipoMetrica',
        idAttribute: "idTipoMetrica",
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return TipoMetricaModel;
});