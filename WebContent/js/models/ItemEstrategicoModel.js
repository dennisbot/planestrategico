define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var ItemEstrategicoModel = Backbone.Model.extend({
        urlRoot: 'rest/itemEstrategico',
        idAttribute: "idItemEstrategico",
        defaults: {
            'discriminator': null,
            'descripcionNivel': null,
            'planEstrategico': null,
            'metrica': null,
            'categoria': null,
            'dependenciaOrganizacional': null,
            'idParentItem': null,
            'periodo': null,
            'estado': 1,
            'valorReal': 0.0,
            'valorPlaneado': 0.0,
            'tolerancia': 0.0,
            'descripcionItem': null,
            'comentarios': null,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
            if (options && options.parentId) {
                // console.log('entra a set urlroot');
                if (options.getGrandChildren)
                    this.urlRoot += '/getChildrenAndNephews/' + options.parentId;
                else
                    this.urlRoot += '/byparent/' + options.parentId;
            }
            else if (options && options.nivel) {
                // console.log('entra a set urlroot');
                if (options.includeHierachy)
                    this.urlRoot += '/bylevelHierachy/' + options.nivel;
                else
                    this.urlRoot += '/bylevel/' + options.nivel;
            }
            else if (options && options.hierachy) {
                this.urlRoot += '/hierachy/' + options.hierachy;
            }
        },
    });
    // Return the model for the module
    return ItemEstrategicoModel;
});