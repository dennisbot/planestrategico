define(['underscore', 'backbone'], function(_, Backbone) {

    var UtilsModel = Backbone.Model.extend({
        urlRoot : 'rest/utils',
        initialize : function(attributes, options) {
            if (options && options.util)
                this.urlRoot += options.util;
            // console.log('this.urlRoot: ', this.urlRoot);
        }
    })

    return UtilsModel;
});