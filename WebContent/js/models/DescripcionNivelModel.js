define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var DescripcionNivelModel = Backbone.Model.extend({
        urlRoot: 'rest/descripcionNivel',
        idAttribute: "idNivel",
        defaults: {
            'descripcionNivel': null,
            'abreviatura': null
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return DescripcionNivelModel;
});