define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var UnidadMedidaModel = Backbone.Model.extend({
        urlRoot: 'rest/unidadMedida',
        idAttribute: "idUnidadMedida",
        defaults: {
            'unidad': null,
            'abreviatura': null,
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return UnidadMedidaModel;
});