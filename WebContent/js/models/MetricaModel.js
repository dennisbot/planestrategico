define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var UnidadMedidaModel = Backbone.Model.extend({
        urlRoot: 'rest/metrica_kpi',
        idAttribute: "idMetrica",
        defaults: {
            'descripcionMetrica': null,
            'definicionExplicacion': null,
            'unidadMedida' : {
                'idUnidadMedida': 0
            },
            'tipoMetrica' : {
                'idTipoMetrica': 0
            }
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return UnidadMedidaModel;
});