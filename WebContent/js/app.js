var el;

var PATH_REST = '';

var REST_BASE_PATH_SERVICE = '';
var PATH_SERVICE_GERENCIA = '';
var PATH_SERVICE_SUPERINTENDENCIA = '';

var parametros;

var debug = false;

/* global temp variable to custom debug */
var gg;

define(
        [
            'jquery',
            'jqueryAnimateColor',
            'jquerymigrate',
            'bootstrap',
            'underscore',
            'backbone',
            'bbnotfound',

            'cookiejs',
            'notify',
            'colorbox',
            'moment',
            'fullcalendar',
            'langfullcalendar',
            'bootbox',
            'editinplace',
            // 'subsequenceSearch',

            'mods/modSecurity',
            'mods/modMenu',
            'AppRouterMappings',
            'collections/ParametrosCollection',

            'text!templates/menuTemplate.html',
            'text!templates/loadingTemplate.html',
         ],
		function(
            $,
            jqueryAnimateColor,
            jquerymigrate,
            _bootstrap,
            _,
            Backbone,
            bbnotfound,

            cookiejs,
            notify,
            colorbox,
            moment,
            fullcalendar,
            langfullcalendar,
            bootbox,
            editinplace,
            // subsequenceSearch,

            modSecurity,
            modMenu,
            AppRouterMappings,
            ParametrosCollection,

            menuTemplate,
            loadingTemplate
        ) {

    Backbone.View.prototype.close = function() {
        if (this.onClose) this.onClose();
        /* this was before bb 1.1.0 */
        /*
        this.remove();
        this.unbind();
        */
        /* from bb 1.1.0 and above */
        // When the view is displayed again, a new instance of it is used.
        // New event listeners are ths added. Hence, stop delegating events for the current view.
        // This ensures the listeners in this view are no longer triggered.
        // We don't invoke 'this.remove()' since the View must not remove elements from DOM.
        this.undelegateEvents();
    }

    window.ViewManager = {
        // A property to store the current view being displayed.
        currentView: null,

        // Display a Backbone View. Closes the previously displayed view gracefully.
        handle: function(view) {
            // Close the previous view
            if (this.currentView != null) {
                // Invoke the close method on the view.
                // All views have this method, defined via the 'Backbone.View.prototype.close' method.
                this.currentView.close();
                this.currentView.$el.html(_.template(loadingTemplate, {}));
                // console.log('this.currentView.$el: ', this.currentView.$el);
            }
            // Display the current view
            this.currentView = view;
            // return this.currentView.render();
        }
    }

    var initialize = function() {
        // console.log('moment: ', moment);
        // console.log('fullcalendar: ', fullcalendar);
        /* establecemos los paths para acceder a los servicios rest  */
        setPaths();
        /* cargamos los parametros e inicializamos las rutas */
        loadParamsAndRoutes();
    }

    var setPaths = function() {
        REST_BASE_PATH_SERVICE = BASE_URL + 'rest/';
        PATH_SERVICE_GERENCIA = REST_BASE_PATH_SERVICE + 'gerencia'
        PATH_SERVICE_SUPERINTENDENCIA = REST_BASE_PATH_SERVICE + 'superintendencia'
    }

    var loadParamsAndRoutes = function() {
        /*console.log('BASE_URL: ', BASE_URL);
        console.log('REST_BASE_PATH_SERVICE: ', REST_BASE_PATH_SERVICE);*/
        (new ParametrosCollection()).fetch({
            success: function(model, response, options) {
                // console.log('se cargo los parametros ...');
                parametros = response;

                var allowedMenu = modMenu.getPreparedHTMLMenu(parametros.permisos);
                // console.log('allowedMenu: ', allowedMenu);
                // var menuText = modMenu.generateHTML(allowedMenu);

                /* cargamos el menu en base al usuario logueado */
                // var compiledTemplate = _.template(menuTemplate, {
                //     modSecurity : modSecurity
                // });
                // $('#menu-container').html(compiledTemplate);
                $('#menu-container').html(allowedMenu);
                $('#username').html(parametros.empleado.nombreParaMostrar);

                /* inicializamos las rutas para mostrar las interfaces */
                AppRouterMappings.initialize();
            },
            error: function() {
                bootbox.alert('<h4 class="text-center"><strong>hubo un error inicializando</strong></h4>');
                console.log('hubo un error inicializando');
            }
        });
    }

	return {
		initialize: initialize
	};
});