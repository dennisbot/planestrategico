define([
        'underscore',
        'mods/modSecurity',
        'text!templates/kpi/columnaOpsMetricaTemplate.html',
        'text!templates/kpi/columnaSelectMetricaTemplate.html',
    ],
    function (
        _,
        modSecurity,
        columnaOpsMetricaTemplate,
        columnaSelectMetricaTemplate
    ) {

    var kpisConfig = {
        getConfig: function(context) {
            var config = {
                rowId: 'idMetrica',
                order: [[0, 'asc']],
                lengthMenu : [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                iDisplayLength : 5,
                container: '#datatable-kpis-bulk',
                $container: context.$('#datatable-kpis-bulk'),
                columns: [
                    {
                        orderable : false,
                        data : null,
                        defaultContent : '',
                    },
                    {
                        data : 'descripcionMetrica',
                        sClass: 'alinear-vertical',
                        width : '20em',
                    },
                    {
                        data : 'unidadMedida',
                        sClass: 'alinear-vertical',
                        // width : '40%',
                    },
                    {
                        data : 'formula',
                        sClass: 'alinear-vertical',
                        width : '20em',
                    },
                    {
                        data : 'nivelKpi',
                        sClass: 'alinear-vertical text-center',
                        width : '7em',
                    },
                ],
                select : {
                    style : 'multi',
                    selector : 'td:first-child'
                },
                columnDefs: [
                     {
                        "targets": 0,
                        orderable : false,
                        className : 'select-checkbox'
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                     {
                        "targets": -5,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return data.length > 40 ? data.substr(0, 40) + '...' : data;
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                     {
                        "targets": -3,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return data.length > 40 ? data.substr(0, 40) + '...' : data;
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                ],
            }
            return config;
        }
    }

    return kpisConfig;
});