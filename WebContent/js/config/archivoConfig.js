define([
        'underscore',
        'mods/modUtils',
        'text!templates/gestion/niveles/iniciativas/control/archivo/columnaOpsArchivosAdjuntosTemplate.html',
    ],
    function (
        _,
        modUtils,
        columnaOpsArchivosAdjuntosTemplate
    ) {

    var archivoConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de archivoConfig');
            return {
                rowId : 'idItemEstrategico',
                hasDetails : true,
                // formatDataTableDetails: _.bind(context.formatDataTableDetails, context),
                // onChildRowRendered : _.bind(context.onChildRowRendered, context),
                order: [[1, 'desc']],
                container: '#ver-adjuntos-table',
                $container: $('#ver-adjuntos-table'),
                doDelete : _.bind(context.doDeleteFile, context),
                columns: [
                    {
                        data : 'theFileName',
                        sClass: 'alinear-vertical',
                        // width : '40%',
                    },
                    {
                        data : 'theFileSize',
                        sClass: 'alinear-vertical text-center',
                        width : '10%',
                    },
                    {
                        data : 'theContentType',
                        sClass: 'alinear-vertical text-center',
                        // width : '20%',
                    },
                    {
                        data : 'idArchivo',
                        sClass : 'text-center',
                        width : context.canShowUploadLink() ? '22%' : '17%',
                        orderable: false,
                    },
                ],
                columnDefs: [
                     {
                        "targets": 1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return modUtils.formatBytes(data);
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                     {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(columnaOpsArchivosAdjuntosTemplate, {
                                idArchivo : data,
                                canShowUploadLink : context.canShowUploadLink()
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                 ],
            }
        }
    }

    return archivoConfig;
});