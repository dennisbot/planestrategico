define({
        rutas :  [
                {
                    urlDescription : 'manage-personal',
                    showMethod : 'showManagePersonal'
                },
                {
                    urlDescription : 'manage-procesos',
                    showMethod : 'showManageProcesos'
                },
                {
                    urlDescription : 'manage-levels',
                    showMethod : 'showManageLevels'
                },
                {
                    urlDescription : 'manage-unidad-medida',
                    showMethod : 'showManageUnidadMedida'
                },
                {
                    urlDescription : 'manage-tipo-kpi',
                    showMethod : 'showManageTipoMetrica'
                },
                {
                    urlDescription : 'manage-kpi',
                    showMethod : 'showManageMetrica'
                },
                {
                    urlDescription : 'manage-categoria',
                    showMethod : 'showManageCategoria'
                },
                {
                    urlDescription : 'objetivos-estrategicos',
                    showMethod : 'showObjetivosEstrategicos'
                },
                {
                    urlDescription : 'objetivos-clave',
                    showMethod : 'showObjetivosClave'
                },
                {
                    urlDescription : 'objetivos-especificos',
                    showMethod : 'showObjetivosEspecificos'
                },
                {
                    urlDescription : 'iniciativas',
                    showMethod : 'showIniciativas'
                },
                {
                    urlDescription : 'control-iniciativas',
                    showMethod : 'showControlIniciativas'
                },
                {
                    urlDescription : 'grado-avance-superintendencias',
                    showMethod : 'showGradoAvanceSuperintendencias'
                },
                {
                    urlDescription : 'grado-avance-gerencias',
                    showMethod : 'showGradoAvanceGerencias'
                },
                {
                    urlDescription : 'grado-avance-vicepresidencias',
                    showMethod : 'showGradoAvanceVicepresidencias'
                },
                {
                    urlDescription : 'grado-ejecucion-superintendencias',
                    showMethod : 'showGradoEjecucionSuperintendencias'
                },
                {
                    urlDescription : 'grado-ejecucion-gerencias',
                    showMethod : 'showGradoEjecucionGerencias'
                },
                {
                    urlDescription : 'grado-ejecucion-vicepresidencias',
                    showMethod : 'showGradoEjecucionVicepresidencias'
                },
                {
                    urlDescription : 'grado-eficacia-superintendencias',
                    showMethod : 'showGradoEficaciaSuperintendencias'
                },
                {
                    urlDescription : 'grado-eficacia-gerencias',
                    showMethod : 'showGradoEficaciaGerencias'
                },
                {
                    urlDescription : 'grado-eficacia-vicepresidencias',
                    showMethod : 'showGradoEficaciaVicepresidencias'
                },
                {
                    urlDescription : 'bpin-superintendencias',
                    showMethod : 'showBPINSuperintendencias'
                },
                {
                    urlDescription : 'bpin-gerencias',
                    showMethod : 'showBPINGerencias'
                },
                {
                    urlDescription : 'bpin-vicepresidencias',
                    showMethod : 'showBPINVicepresidencias'
                },
                {
                    urlDescription : 'mi-perfil',
                    optional : true,
                    showMethod : 'showMiPerfil'
                },
                {
                    urlDescription : 'manage-permisos',
                    showMethod : 'showManagePermisos'
                },
                {
                    urlDescription : 'asignacion-roles',
                    showMethod : 'showAsignacionRoles'
                },
                {
                    urlDescription : '',
                    showMethod : 'homeAction'
                }
            ]
});