define([
        'underscore',
        'text!templates/gestion/searchSelectItemEstrategicoTemplate.html',
    ],
    function (
        _,
        searchSelectItemEstrategicoTemplate
    ) {

    var searchItemEstrategicoConfig = {
        getConfig: function(context) {
            // console.log('response: ', response);
            // console.log('entra a getConfig de searchItemEstrategicoConfig');
            return {
                rowId: 'idItemEstrategico',
                hasDetails: true,
                paging: true,
                bFilter: false,
                formatDataTableDetails: _.bind(context.formatDataTableSearchDetails, context),
                order: [[1, 'asc']],
                container: '#table-result-search-item-estrategico',
                $container: $('#table-result-search-item-estrategico'),
                doSelect : _.bind(context.doSelectSearch, context),
                columns: [
                    {
                        sClass: 'details-control',
                        orderable : false,
                        data : null,
                        defaultContent : '',
                    },
                    {
                        data : 'descripcionNivel.idNivel',
                        sClass: 'alinear-vertical text-center',
                        width: '5%',
                        orderable: true,
                    },
                    {
                        data : 'descripcionItemResult',
                        // width : '40%',
                    },
                    {
                        data : 'dependenciaOrganizacional.descripcionDependencia',
                        // width : '40%',
                    },
                    {
                        data : 'idItemEstrategico',
                        width : '12%',
                        orderable: false,
                    },
                ],
                columnDefs: [
                     {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return _.template(searchSelectItemEstrategicoTemplate, { idItemEstrategico : data });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                    },
                ],
            }
        }
    }

    return searchItemEstrategicoConfig;
});