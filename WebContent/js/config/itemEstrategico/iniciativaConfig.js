define([
        'underscore',
        'text!templates/gestion/niveles/iniciativas/columnaOpsIniciativaTemplate.html',
        'mods/modSecurity',
    ],
    function (
        _,
        columnaOpsIniciativaTemplate,
        modSecurity
    ) {

    var iniciativaConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de iniciativaConfig');
            var config = {
                container: '.data-table-container',
                $container: context.$('.data-table-container'),
                order: [[1, 'asc']],
                doGo: _.bind(context.doGo, context),
                doEdit: _.bind(context.doEdit, context),
                doDelete: _.bind(context.doDelete, context),
                columns: [
                    {
                        data : 'descripcionNivel.idNivel',
                        sClass: 'alinear-vertical text-center',
                        width: '5%',
                        orderable: false,
                    },
                    {
                        data : 'descripcionItem',
                        // width : '40%',
                    },
                    {
                        data : 'dependenciaOrganizacional.descripcionDependencia',
                        // width : '40%',
                    },
                ],
                columnDefs: [
                 ],
            }
            if (modSecurity.canEdit(context.curUrlDescripcion) ||
                modSecurity.canDelete(context.curUrlDescripcion) ||
                modSecurity.canView(context.curUrlDescripcion) ) {
                config.columns.push({
                    data : 'idItemEstrategico',
                    width : '28%',
                    orderable: false,
                    sClass : 'alinear-vertical text-center'
                });
                config.columnDefs.push({
                    "targets": -1,
                    "data": null,
                    "mRender": function (data, type, row) {
                        // console.log('row: ', row);
                        return _.template(columnaOpsIniciativaTemplate, {
                            idItemEstrategico : data,
                            nivel : row.descripcionNivel.idNivel,
                            modSecurity : modSecurity,
                            curUrlDescripcion : context.curUrlDescripcion,
                            mine : row.idInsertedBy == parametros.empleado.idEmpleado,
                        });
                    }
                    // "defaultContent": '<button class="btn btn-default">Click!</button>'
                });
            }
            return config;
        }
    }

    return iniciativaConfig;
});