define([
        'underscore',
        'mods/modProgressBar',
        'text!templates/gestion/niveles/metas/columnaOpsMetaTemplate.html',
    ],
    function (
        _,
        modProgressBar,
        columnaOpsMetaTemplate
    ) {

    var metaConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de metaConfig');
            return {
                container: '.data-table-container',
                $container: context.$('.data-table-container'),
                order: [[1, 'asc']],
                doGo: _.bind(context.doGo, context),
                doEdit: _.bind(context.doEdit, context),
                doDelete: _.bind(context.doDelete, context),
                columns: [
                    {
                        data : 'descripcionNivel.idNivel',
                        sClass: 'alinear-vertical text-center',
                        width: '5%',
                        orderable: false,
                    },
                    {
                        data : '',
                        sClass: 'alinear-vertical text-center',
                        // iDataSort : 1,
                        // width : '40%',
                    },
                    {
                        data : 'valorPlaneado',
                        sClass: 'alinear-vertical text-center',
                        width : '13%',
                    },
                    {
                        data : 'valorReal',
                        sClass: 'alinear-vertical text-center',
                        width : '13%',
                    },

                    {
                        data : '',
                        sClass: 'alinear-vertical',
                        width : '40%',
                    }
                ],
                columnDefs: [
                     {
                        "targets": 1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            /* esta linea sirve para hacer un sorting dependiendo de otro valor */
                            if (type == 'sort') return row.idItemEstrategico;
                            return parametros.curPlanEstrategico.anio + ' - ' + row.periodo.abreviaturaMes
                        }
                     },
                     {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            modProgressBar.iniciativa = context.curItem;
                            return modProgressBar.getInstance(5, row).draw();
                        }
                     }
                 ],
            }
        }
    }

    return metaConfig;
});