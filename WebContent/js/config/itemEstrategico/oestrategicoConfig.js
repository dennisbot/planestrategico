define([
        'underscore',
        'text!templates/gestion/niveles/oestrategicos/columnaOpsOEstrategicoTemplate.html',
        'mods/modSecurity',
    ],
    function (
        _,
        columnaOpsOEstrategicoTemplate,
        modSecurity
    ) {

    var oestrategicoConfig = {
        getConfig: function(context) {
            // console.log('entra a getConfig de oestrategicoConfig');
            var config = {
                container: '.data-table-container',
                $container: context.$('.data-table-container'),
                order: [[1, 'asc']],
                doGo: _.bind(context.doGo, context),
                doEdit: _.bind(context.doEdit, context),
                doDelete: _.bind(context.doDelete, context),
                columns: [
                    {
                        data : 'descripcionNivel.idNivel',
                        sClass: 'alinear-vertical text-center',
                        width: '5%',
                        orderable: false,
                    },
                    {
                        data : 'descripcionItem',
                        // width : '40%',
                    },
                    {
                        data : 'dependenciaOrganizacional.descripcionDependencia',
                        sClass : 'text-center',
                        width : '20%',
                    },
                ],
                columnDefs: [{
                        "targets": 2,
                        "data": null,
                        "mRender": function (data, type, row) {
                            if (type == 'sort')
                                return row.idItemEstrategico;
                            return data;
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                    },
                 ],
            }

            if (modSecurity.canEdit(context.curUrlDescripcion) ||
                modSecurity.canDelete(context.curUrlDescripcion) ||
                modSecurity.canView(context.curUrlDescripcion) ) {
                config.columns.push({
                    data : 'idItemEstrategico',
                    width : '28%',
                    orderable: false,
                    sClass : 'alinear-vertical text-center'
                });
                config.columnDefs.push({
                    "targets": -1,
                    "data": null,
                    "mRender": function (data, type, row) {
                        return _.template(columnaOpsOEstrategicoTemplate, {
                            idItemEstrategico : data,
                            nivel : row.descripcionNivel.idNivel,
                            modSecurity : modSecurity,
                            curUrlDescripcion : context.curUrlDescripcion
                        });
                    }
                    // "defaultContent": '<button class="btn btn-default">Click!</button>'
                 });
            }
            return config;
        }
    }

    return oestrategicoConfig;
});