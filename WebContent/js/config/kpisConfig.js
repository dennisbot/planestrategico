define([
        'underscore',
        'mods/modSecurity',
        'text!templates/kpi/columnaOpsMetricaTemplate.html',
        'text!templates/kpi/columnaSelectMetricaTemplate.html',
    ],
    function (
        _,
        modSecurity,
        columnaOpsMetricaTemplate,
        columnaSelectMetricaTemplate
    ) {

    var kpisConfig = {
        getConfig: function(context) {
            var config = {
                rowId: 'idMetrica',
                hasDetails: true,
                formatDataTableDetails: _.bind(context.formatDataTableDetails, context),
                order: [[0, 'asc']],
                lengthMenu : (context.isModal) ? [5, 10, 25, 50, 100] : [10, 25, 50, 100],
                iDisplayLength : (context.isModal) ? 5 : 10,
                container: '#datatable-kpis',
                $container: context.$('#datatable-kpis'),
                doEdit: (!context.isModal) ? _.bind(context.doEdit, context) : null,
                doDelete: (!context.isModal) ? _.bind(context.doDelete, context) : null,
                doSelect : (context.isModal) ? _.bind(context.doSelect, context) : null,
                columns: [
                    {
                        sClass: 'details-control',
                        orderable : false,
                        data : null,
                        defaultContent : '',
                    },
                    {
                        data : 'descripcionMetrica',
                        sClass: 'alinear-vertical',
                        width : '20em',
                    },
                    {
                        data : 'unidadMedida',
                        sClass: 'alinear-vertical',
                        // width : '40%',
                    },
                    {
                        data : 'formula',
                        sClass: 'alinear-vertical',
                        width : '20em',
                    },
                    {
                        data : 'nivelKpi',
                        sClass: 'alinear-vertical text-center',
                        width : '7em',
                    },
                ],
                columnDefs: [
                     {
                        "targets": -5,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return data.length > 40 ? data.substr(0, 40) + '...' : data;
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                     {
                        "targets": -3,
                        "data": null,
                        "mRender": function (data, type, row) {
                            return data.length > 40 ? data.substr(0, 40) + '...' : data;
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     },
                ],
            }
            var canedit = modSecurity.canEdit(context.curUrlDescripcion);
            canedit = false;
            var candelete = modSecurity.canDelete(context.curUrlDescripcion);
            if (context.isModal || canedit || candelete) {
                config.columns.push({
                        data : 'idMetrica',
                        width :  context.isModal ? '12%' : '17%',
                        orderable: false,
                        sClass : 'alinear-vertical' + (context.isModal ||
                            (canedit && !candelete) ||
                            (!canedit && candelete) ? ' text-center' : '')
                });
                config.columnDefs.push({
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            if (context.isModal)
                                return _.template(columnaSelectMetricaTemplate, { idMetrica : data });
                            return _.template(columnaOpsMetricaTemplate, {
                                modSecurity : modSecurity,
                                canedit : canedit,
                                candelete : candelete,
                                idMetrica : data
                            });
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                     });
            }
            return config;
        }
    }

    return kpisConfig;
});