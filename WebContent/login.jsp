<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lan="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="shortcut icon" type="image/x-icon" href="favicon/favicon.ico"/>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/datepicker3.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/planestrategico.css">

<link rel="stylesheet" media="screen" href="js/plugins/handsontable/dist/handsontable.full.css">
<link href="css/font-awesome.min.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
   <script src="js/plugins/misc/html5shiv.js"></script>
   <script src="js/plugins/misc/respond.min.js"></script>
 <![endif]-->

<title>${initParam.APP_NAME} | LAS BAMBAS</title>
</head>
<body role="document" class="login">
    <div class="row vertical-center" id="contenedorLogin">
      <div class="col-md-4 col-md-offset-4 border-left" id="formLogin">
        <form action="login.do" method="post" id="form-action">
          <input type="hidden" name="id" value="login">
          <input type="hidden" name="hash" id="hash" value="">
          <div class="logo">
			<div><img src="css/img/logolbv2.png"></div>
			<strong style="text-transform: uppercase;font-size: 16px;color: #C51230;">${initParam.APP_NAME} <small>V${initParam.APP_VERSION}</small></strong>
		  </div>
          <div class="form-group">
              <div class="input-group">
                  <input type="text" class="form-control" name="txtUsuario" id="usuario" placeholder="Usuario"
                  value="<c:choose><c:when test="${not empty sessionScope.usuario}"><c:out value="${sessionScope.usuario}" /></c:when><c:otherwise><c:out value="" /></c:otherwise></c:choose>"
                  <c:if test="${empty sessionScope.usuario}"><c:out value="autofocus" /></c:if>
                  ><span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
          </div>
          <div class="form-group">
              <div class="input-group">
                  <input type="password" class="form-control" name="txtPassword" id="password" placeholder="Contraseña" value="" autofocus="autofocus"><span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
          </div>
          <div class="form-group">
              <div class="input-group">
                  <button class="btn btn-primary" id="iniciar-sesion"><span class="glyphicon glyphicon-ok"></span> INICIAR SESIÓN </button>
              </div>
          </div>
          <c:if test="${not empty sessionScope.mensajeUsuario}">
          <div class="form-group alert alert-danger">
            <c:out value="${sessionScope.mensajeUsuario}" />
          </div>
          </c:if>
        </form>
      </div>
    </div>
    <script src="js/lib/jquery.min.js"></script>
    <script src="js/login/login.js"></script>
</body>
</html>