<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.empleado}">
  <c:redirect url="/home"/>
</c:if>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="shortcut icon" type="image/x-icon" href="favicon/favicon.ico"/>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/datepicker3.css" rel="stylesheet" type="text/css">

<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/font-awesome-animation.min.css" rel="stylesheet">
<link href="css/chosen/bootstrap-chosen.css" rel="stylesheet">
<link rel="stylesheet" href="css/planestrategico.css">
<link rel="stylesheet" href="css/views/addProgramacion.css">
<link rel="stylesheet" href="css/views/inspeccion/inspeccion.css">
<link rel="stylesheet" href="css/views/revision/listado-revision.css">
<link rel="stylesheet" href="css/views/gestion/breadcrumb.css">
<link rel="stylesheet" href="css/datatable/customtable.css">

<link rel="stylesheet" href="file-uploader/fineuploader.css">
<link rel="stylesheet" href="css/colorbox.css">
<link rel="stylesheet" href="js/lib/fullcalendar-2.4.0/fullcalendar.min.css">
<link rel="stylesheet" href="js/lib/fullcalendar-2.4.0/fullcalendar.print.css" media="print">

<!-- para el dataTables -->
<!-- <link rel="stylesheet" href="DataTables-1.10.9/media/css/jquery.dataTables.css"> -->
<link rel="stylesheet" href="DataTables-1.10.9/media/css/dataTables.bootstrap.css">
<link rel="stylesheet" href="DataTables-1.10.9/media/css/select.dataTables.min.css">
<!-- custom css -->
<style id="custom-css"></style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
   <script src="js/plugins/misc/html5shiv.js"></script>
   <script src="js/plugins/misc/respond.min.js"></script>
 <![endif]-->

<title>${initParam.APP_NAME} | LAS BAMBAS</title>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top bg-blue">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home" tabindex="-1"><img src="images/favicon.png" class="brand">${initParam.APP_NAME} <small>V${initParam.APP_VERSION}</small></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <div id="menu-container"></div>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container top-container theme-showcase" id="bodyContainer">
      <div class="row">
        <div id="container" class="container theme-showcase" role="main">
          <div>
            <h4 class="text-center"><i class="fa fa-refresh fa-spin"></i> Cargando...</h4>
          </div>
        </div>
      </div>
    </div>
    <div id="modal-area"></div>
    <div id="modal-area-2"></div>
    <div id="modal-area-3"></div>
    <script>
      var BASE_URL = '<c:out value="${initParam['BASE_URL']}" />';
      var DOMAIN = '<c:out value="${initParam['DOMAIN']}" />';
      var REPORT_SERVER = '<c:out value="${initParam['REPORT_SERVER']}" />';
    </script>

<script data-main="js/main" src="js/lib/require.js"></script>
<script src="js/plugins/fuzzy/fuzzy.js"></script>
<script src="file-uploader/fineuploader.js"></script>
</body>
</html>