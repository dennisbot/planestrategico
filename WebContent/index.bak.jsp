<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.empleado}">
  <c:redirect url="/home"/>
</c:if>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="shortcut icon" type="image/x-icon" href="favicon/favicon.ico"/>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/datepicker3.css" rel="stylesheet" type="text/css">

<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/font-awesome-animation.min.css" rel="stylesheet">
<link href="css/chosen/bootstrap-chosen.css" rel="stylesheet">
<link rel="stylesheet" href="css/planestrategico.css">
<link rel="stylesheet" href="css/views/addProgramacion.css">
<link rel="stylesheet" href="css/views/inspeccion/inspeccion.css">
<link rel="stylesheet" href="css/views/revision/listado-revision.css">
<link rel="stylesheet" href="css/views/gestion/breadcrumb.css">
<link rel="stylesheet" href="css/datatable/customtable.css">

<link rel="stylesheet" href="file-uploader/fineuploader.css">
<link rel="stylesheet" href="css/colorbox.css">
<link rel="stylesheet" href="js/lib/fullcalendar-2.4.0/fullcalendar.min.css">
<link rel="stylesheet" href="js/lib/fullcalendar-2.4.0/fullcalendar.print.css" media="print">

<!-- para el dataTables -->
<!-- <link rel="stylesheet" href="DataTables-1.10.9/media/css/jquery.dataTables.css"> -->
<link rel="stylesheet" href="DataTables-1.10.9/media/css/dataTables.bootstrap.css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
   <script src="js/plugins/misc/html5shiv.js"></script>
   <script src="js/plugins/misc/respond.min.js"></script>
 <![endif]-->

<title>${initParam.APP_NAME} | LAS BAMBAS</title>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top bg-blue">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home" tabindex="-1"><img src="images/favicon.png" class="brand">${initParam.APP_NAME} <small>V${initParam.APP_VERSION}</small></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <div id="menu-container"></div>
          <ul class="nav navbar-nav navbar-right">
            <li class="page">
              <a href="#" tabindex="-1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" id="username"><i class="fa fa-user fa-lg"></i> <c:out value="${sessionScope.empleado.usuario}"/> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#mi-perfil"><i class="fa fa-lock"></i> MI PERFIL</a></li>
                <li><a href="#manage-permisos"><i class="fa fa-check-square-o"></i> GESTIÓN DE PERMISOS</a></li>
                <li><a href="#asignacion-roles"><i class="fa fa-group"></i> ASIGNACIÓN DE ROLES</a></li>
                <li><a href="<c:url value="/logout"/>" tabindex="-1"><i class="fa fa-power-off"></i> CERRAR SESIÓN</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container top-container theme-showcase" id="bodyContainer">
      <div id="container" class="container theme-showcase" role="main">
        <div>
          <h4 class="text-center"><i class="fa fa-refresh fa-spin"></i> Cargando...</h4>
        </div>
      </div>
    </div>
    <div id="modal-area"></div>
    <div id="modal-area-2"></div>
    <div id="modal-area-3"></div>
    <script>
      var BASE_URL = '<c:out value="${initParam['BASE_URL']}" />'
    </script>

<script data-main="js/main" src="js/lib/require.js"></script>
<script src="js/plugins/fuzzy/fuzzy.js"></script>
<script src="file-uploader/fineuploader.js"></script>
</body>
</html>