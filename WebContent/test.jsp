<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.empleado}">
  <c:redirect url="/home"/>
</c:if>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="shortcut icon" type="image/x-icon" href="favicon/favicon.ico"/>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/datepicker3.css" rel="stylesheet" type="text/css">

<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/font-awesome-animation.min.css" rel="stylesheet">
<link href="css/chosen/bootstrap-chosen.css" rel="stylesheet">
<link rel="stylesheet" href="css/planestrategico.css">
<link rel="stylesheet" href="css/views/addProgramacion.css">
<link rel="stylesheet" href="css/views/inspeccion/inspeccion.css">
<link rel="stylesheet" href="css/views/revision/listado-revision.css">
<link rel="stylesheet" href="css/views/gestion/breadcrumb.css">
<link rel="stylesheet" href="css/datatable/customtable.css">

<link rel="stylesheet" href="file-uploader/fineuploader.css">
<link rel="stylesheet" href="css/colorbox.css">
<link rel="stylesheet" href="js/lib/fullcalendar-2.4.0/fullcalendar.min.css">
<link rel="stylesheet" href="js/lib/fullcalendar-2.4.0/fullcalendar.print.css" media="print">

<!-- para el dataTables -->
<!-- <link rel="stylesheet" href="DataTables-1.10.9/media/css/jquery.dataTables.css"> -->
<link rel="stylesheet" href="DataTables-1.10.9/media/css/dataTables.bootstrap.css">
<!-- custom css -->
<style id="custom-css"></style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
   <script src="js/plugins/misc/html5shiv.js"></script>
   <script src="js/plugins/misc/respond.min.js"></script>
 <![endif]-->

<title>${initParam.APP_NAME} | LAS BAMBAS</title>
</head>
<body>
	<div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
	    <ul id="myTabs" class="nav nav-tabs" role="tablist">
	      <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Home</a></li>
	      <li role="presentation" class=""><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Profile</a></li>
	      <li role="presentation" class="dropdown">
	        <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents">Dropdown <span class="caret"></span></a>
	        <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
	          <li><a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">@fat</a></li>
	          <li><a href="#dropdown2" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">@mdo</a></li>
	        </ul>
	      </li>
	    </ul>
	    <div id="myTabContent" class="tab-content">
	      <div aria-labelledby="home-tab" id="home" class="tab-pane fade active in" role="tabpanel">
	        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
	      </div>
	      <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
	        <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
	      </div>
	      <div role="tabpanel" class="tab-pane fade" id="dropdown1" aria-labelledby="dropdown1-tab">
	        <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
	      </div>
	      <div role="tabpanel" class="tab-pane fade" id="dropdown2" aria-labelledby="dropdown2-tab">
	        <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
	      </div>
	    </div>
  	</div>
    <script>
      var BASE_URL = '<c:out value="${initParam['BASE_URL']}" />';
      var DOMAIN = '<c:out value="${initParam['DOMAIN']}" />';
    </script>

<script data-main="js/main" src="js/lib/require.js"></script>
<script src="js/plugins/fuzzy/fuzzy.js"></script>
<script src="file-uploader/fineuploader.js"></script>
</body>
</html>