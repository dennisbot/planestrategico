<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/datepicker3.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/PlanEstrategico.css">

<link rel="stylesheet" media="screen" href="js/plugins/handsontable/dist/handsontable.full.css">
<link href="css/font-awesome.min.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
   <script src="js/plugins/misc/html5shiv.js"></script>
   <script src="js/plugins/misc/respond.min.js"></script>
 <![endif]-->

<title>${initParam.APP_NAME} | LAS BAMBAS</title>
</head>
<body role="document">
    <div class="row vertical-center" id="contenedorLogin">
      <div class="col-md-4 col-md-offset-4 border-left" id="formLogin">
          <div class="form-group">
              <label for="">error, las credenciales usadas no son correctas</label>
          </div>
      </div>
    </div>
    <script data-main="js/login" src="js/lib/require.js"></script>
</body>
</html>