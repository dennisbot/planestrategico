package com.lasbambas.PlanEstrategico.fineuploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lasbambas.PlanEstrategico.util.ExcelKPIMigrator;
import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.tools.correo.CorreoFacade;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.archivo.ArchivoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.archivo.ArchivoDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.itemEstrategico.ItemEstrategicoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.itemEstrategico.ItemEstrategicoDao;

public class UploadKPIsReceiver extends HttpServlet {

	public static File UPLOAD_DIR;
	public static File TEMP_DIR; 

	public static String CONTENT_LENGTH = "Content-Length";
	public static int SUCCESS_RESPONSE_CODE = 200;

	final Logger log = LoggerFactory.getLogger(UploadKPIsReceiver.class);

	int accesses = 0;
	
	private CorreoFacade correoFacade;
	private VelocityEngine velocityEngine;
	private ExcelKPIMigrator excelKPIMigrator;

	@Override
	public void init() throws ServletException {
		String STORAGE_UNIT = getServletContext().getInitParameter("STORAGE_UNIT");
		UPLOAD_DIR  = new File(STORAGE_UNIT + ":\\Intranet\\PlanEstrategico\\FilesKPIsUploaded");
		TEMP_DIR = new File(STORAGE_UNIT + ":\\Intranet\\PlanEstrategico\\FilesKPIsUploadedTemp");
		UPLOAD_DIR.mkdirs();
		correoFacade = (CorreoFacade)getServletContext().getAttribute("correoFacade");
		velocityEngine = (VelocityEngine)getServletContext().getAttribute("velocityEngine");
		excelKPIMigrator = (ExcelKPIMigrator)getServletContext().getAttribute("excelKPIMigrator");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uuid = req.getPathInfo().replaceAll("/", "");
		File curFolder = new File(UPLOAD_DIR, uuid);
		File curFile = curFolder.listFiles()[0];
		String path = curFile.getPath();

		String contentType = Files.probeContentType(Paths.get(path));
		int contentLength = (int)curFile.length();

		resp.setContentType(contentType);
		resp.setContentLength(contentLength);

		FileInputStream in = new FileInputStream(curFile);
		OutputStream out = resp.getOutputStream();
		byte[] buf = new byte[1024];
		int count = 0;
		while ((count = in.read(buf)) >= 0) {
			out.write(buf, 0, count);
		}
		out.close();
		in.close();

	}
	protected String getContentType(String uuid) throws IOException {
		File curFolder = new File(UPLOAD_DIR, uuid);
		File curFile = curFolder.listFiles()[0];
		String path = curFile.getPath();
		
		String contentType = Files.probeContentType(Paths.get(path));
		return contentType;
	}
	protected void test(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		// System.out.println("do get ha sido llamado con éxito");
				resp.setContentType("text/html");
				PrintWriter out = resp.getWriter();
				String uuid = req.getPathInfo().replaceAll("/", "");
				if (uuid != null) {
					out.print("si tiene imageid como parametro<br>");
					File curFolder = new File(UPLOAD_DIR, uuid);
					File curFile = curFolder.listFiles()[0];
					String path = curFile.getPath();
					String contentType = Files.probeContentType(Paths.get(path));
					out.print("el content type del archivo es:<br>");
					out.print(contentType);
					out.print("<br>");
//					resp.setContentType(contentType);
					int contentLength = (int)curFile.length();
					out.print("el content length del archivo es:<br>");
					out.print(contentLength);
					out.print("<br>");

//					java.nio.file.Path.class
					//Files.probeContentType(path);
					out.print("este es el path:<br>");
					out.print(path);
					out.print("<br>");
					out.print("req.getPathInfo()");
					out.print("<br>");
					out.print(req.getPathInfo());
				} else {
					accesses++;
					out.print("Number of times this servlet has been accessed:"
							+ "accesses<br>");
					// super.doGet(req, resp);
					out.println("no tiene imageid como parametro");
					out.print("<br>");
					out.print("req.getPathInfo()");
					out.print("<br>");
					out.print(req.getPathInfo());
				}
	}

	@Override
	public void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String uuid = req.getPathInfo().replaceAll("/", "");

		FileUtils.deleteDirectory(new File(UPLOAD_DIR, uuid));

		if (new File(UPLOAD_DIR, uuid).exists()) {
			System.out.println("no se pudo borrar el archivo " + uuid);
			log.warn("couldn't find or delete " + uuid);
		} else {
			System.out.println("se borró el archivo exitosamente");
			log.info("deleted " + uuid);
		}
		
		resp.setStatus(SUCCESS_RESPONSE_CODE);
		// resp.addHeader("Access-Control-Allow-Origin", "*");
	}

	@Override
	public void doOptions(HttpServletRequest req, HttpServletResponse resp) {
		resp.setStatus(SUCCESS_RESPONSE_CODE);
		// resp.addHeader("Access-Control-Allow-Origin",
		// "http://192.168.130.118:8080");
		// resp.addHeader("Access-Control-Allow-Credentials", "true");
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Methods", "POST, DELETE");
		resp.addHeader("Access-Control-Allow-Headers",
				"x-requested-with, cache-control, content-type");
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		RequestParser requestParser = null;

		boolean isIframe = req.getHeader("X-Requested-With") == null
				|| !req.getHeader("X-Requested-With").equals("XMLHttpRequest");

		try {
			resp.setContentType(isIframe ? "text/html" : "text/plain");
			resp.setStatus(SUCCESS_RESPONSE_CODE);

			if (ServletFileUpload.isMultipartContent(req)) {
				System.out.println("entro a is multipart content");
				MultipartUploadParser multipartUploadParser = new MultipartUploadParser(
						req, TEMP_DIR, getServletContext());
				requestParser = RequestParser.getInstance(req,
						multipartUploadParser);
				writeFileForMultipartRequest(requestParser);
				writeResponse(resp.getWriter(),
						requestParser.generateError() ? "Generated error"
								: null, isIframe, false, requestParser);
				
			} else {
				System.out.println("entro a request parser");
				requestParser = RequestParser.getInstance(req, null);
				writeFileForNonMultipartRequest(req, requestParser);
				writeResponse(resp.getWriter(),
						requestParser.generateError() ? "Generated error"
								: null, isIframe, false, requestParser);
			}
			
			// Custom Line added by dennisbot
			this.migrateKPIs(requestParser, req);
			
		} catch (Exception e) {
			log.error("Problem handling upload request", e);
			if (e instanceof MergePartsException) {
				writeResponse(resp.getWriter(), e.getMessage(), isIframe, true,
						requestParser);
			} else {
				writeResponse(resp.getWriter(), e.getMessage(), isIframe,
						false, requestParser);
			}
		}
	}

	private void migrateKPIs(RequestParser requestParser, HttpServletRequest request) throws Exception {
		String filepath = UPLOAD_DIR + "\\" + requestParser.getUuid();
		String filename = requestParser.getFilename();
		String result = excelKPIMigrator.updateDatabaseWithFile2(filepath, filename);
		this.sendMail(requestParser, request, result);
	}

	private void sendMail(RequestParser requestParser, HttpServletRequest request, String result) {
		
		EmpleadoDTO creador = (EmpleadoDTO)WebUtil.obtenerAtributoenSesion(request, "empleado");
		SimpleMailMessage smm = new SimpleMailMessage();
		
		if (getServletContext().getInitParameter("DEVELOPMENT").equals("1")) {
			smm.setFrom("dennisbot@matrix.dev");
			smm.setTo(new String[]{"carlos@matrix.dev"});	
		}
		else {
			smm.setFrom(creador.getCorreo());
			smm.setCc(new String[] { creador.getCorreo(), getServletContext().getInitParameter("secondFrom") } );
			smm.setTo("isaias.esquivel@mmg.com");
		}
		String mailSubject = "[INFO] - KPIs actualizados en el sistema de Plan Estratégico";
		smm.setSubject(mailSubject);
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("BASE_URL", getServletContext().getInitParameter("BASE_URL"));
		model.put("creador", creador);
		model.put("theFileName", requestParser.getFilename());
		model.put("result", result);
		
		String itemEstrategicoCreadoTpl = VelocityEngineUtils
				.mergeTemplateIntoString(velocityEngine
						, "velocity/kpisUploadedTpl.html"
						, "UTF-8"
						, model
				);
		
		smm.setText(itemEstrategicoCreadoTpl);
		correoFacade.sendMail(smm);
		
	}

	private void writeFileForNonMultipartRequest(HttpServletRequest req,
			RequestParser requestParser) throws Exception {
		File dir = new File(UPLOAD_DIR, requestParser.getUuid());
		dir.mkdirs();

		String contentLengthHeader = req.getHeader(CONTENT_LENGTH);
		long expectedFileSize = Long.parseLong(contentLengthHeader);

		if (requestParser.getPartIndex() >= 0) {
			writeFile(
					req.getInputStream(),
					new File(dir, requestParser.getUuid()
							+ "_"
							+ String.format("%05d",
									requestParser.getPartIndex())), null);

			if (requestParser.getTotalParts() - 1 == requestParser
					.getPartIndex()) {
				File[] parts = getPartitionFiles(dir, requestParser.getUuid());
				File outputFile = new File(dir, requestParser.getFilename());
				for (File part : parts) {
					mergeFiles(outputFile, part);
				}

				assertCombinedFileIsVaid(requestParser.getTotalFileSize(),
						outputFile, requestParser.getUuid());
				deletePartitionFiles(dir, requestParser.getUuid());
			}
		} else {
			writeFile(req.getInputStream(),
					new File(dir, requestParser.getFilename()),
					expectedFileSize);
		}
	}

	private void writeFileForMultipartRequest(RequestParser requestParser)
			throws Exception {
		File dir = new File(UPLOAD_DIR, requestParser.getUuid());
		dir.mkdirs();

		if (requestParser.getPartIndex() >= 0) {
			writeFile(
					requestParser.getUploadItem().getInputStream(),
					new File(dir, requestParser.getUuid()
							+ "_"
							+ String.format("%05d",
									requestParser.getPartIndex())), null);

			if (requestParser.getTotalParts() - 1 == requestParser
					.getPartIndex()) {
				File[] parts = getPartitionFiles(dir, requestParser.getUuid());
				File outputFile = new File(dir,
						requestParser.getOriginalFilename());
				for (File part : parts) {
					mergeFiles(outputFile, part);
				}

				assertCombinedFileIsVaid(requestParser.getTotalFileSize(),
						outputFile, requestParser.getUuid());
				deletePartitionFiles(dir, requestParser.getUuid());
			}
		} else {
			writeFile(requestParser.getUploadItem().getInputStream(), new File(
					dir, requestParser.getFilename()), null);
		}
	}

	private void assertCombinedFileIsVaid(int totalFileSize, File outputFile,
			String uuid) throws MergePartsException {
		if (totalFileSize != outputFile.length()) {
			deletePartitionFiles(UPLOAD_DIR, uuid);
			outputFile.delete();
			throw new MergePartsException("Incorrect combined file size!");
		}

	}

	private static class PartitionFilesFilter implements FilenameFilter {
		private String filename;

		PartitionFilesFilter(String filename) {
			this.filename = filename;
		}

		@Override
		public boolean accept(File file, String s) {
			return s.matches(Pattern.quote(filename) + "_\\d+");
		}
	}

	private static File[] getPartitionFiles(File directory, String filename) {
		File[] files = directory.listFiles(new PartitionFilesFilter(filename));
		Arrays.sort(files);
		return files;
	}

	private static void deletePartitionFiles(File directory, String filename) {
		File[] partFiles = getPartitionFiles(directory, filename);
		for (File partFile : partFiles) {
			partFile.delete();
		}
	}

	private File mergeFiles(File outputFile, File partFile) throws Exception {
		FileOutputStream fos;
		FileInputStream fis;
		byte[] fileBytes;
		int bytesRead = 0;
		fos = new FileOutputStream(outputFile, true);
		fis = new FileInputStream(partFile);
		fileBytes = new byte[(int) partFile.length()];
		bytesRead = fis.read(fileBytes, 0, (int) partFile.length());
		assert (bytesRead == fileBytes.length);
		assert (bytesRead == (int) partFile.length());
		fos.write(fileBytes);
		fos.flush();
		fis.close();
		fos.close();

		return outputFile;
	}

	private File writeFile(InputStream in, File out, Long expectedFileSize)
			throws IOException {
		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(out);

			IOUtils.copy(in, fos);

			if (expectedFileSize != null) {
				Long bytesWrittenToDisk = out.length();
				if (!expectedFileSize.equals(bytesWrittenToDisk)) {
					log.warn(
							"Expected file {} to be {} bytes; file on disk is {} bytes",
							new Object[] { out.getAbsolutePath(),
									expectedFileSize, 1 });
					out.delete();
					throw new IOException(
							String.format(
									"Unexpected file size mismatch. Actual bytes %s. Expected bytes %s.",
									bytesWrittenToDisk, expectedFileSize));
				}
			}

			return out;
		} catch (Exception e) {
			throw new IOException(e);
		} finally {
			IOUtils.closeQuietly(fos);
		}
	}

	private void writeResponse(PrintWriter writer, String failureReason,
			boolean isIframe, boolean restartChunking,
			RequestParser requestParser) {
		if (failureReason == null) {
			// if (isIframe)
			// {
			// writer.print("{\"success\": true, \"uuid\": \"" +
			// requestParser.getUuid() +
			// "\"}<script src=\"http://192.168.130.118:8080/client/js/iframe.xss.response.js\"></script>");
			// }
			// else
			// {
			writer.print("{\"uuid\": \"" + requestParser.getUuid() + "\",\"success\": true}");
			// }
		} else {
			if (restartChunking) {
				writer.print("{\"error\": \"" + failureReason
						+ "\", \"reset\": true}");
			} else {
				// if (isIframe)
				// {
				// writer.print("{\"error\": \"" + failureReason +
				// "\", \"uuid\": \"" + requestParser.getUuid() +
				// "\"}<script src=\"http://192.168.130.118:8080/client/js/iframe.xss.response.js\"></script>");
				// }
				// else
				// {
				writer.print("{\"error\": \"" + failureReason + "\"}");
				// }
			}
		}
	}

	private class MergePartsException extends Exception {
		MergePartsException(String message) {
			super(message);
		}
	}
	public static void main(String[] args) {
		String sep = "/12/18";
		String[] seps = sep.split("/");
		for (String s : seps) {
			System.out.println(s);
		}
	}
}
