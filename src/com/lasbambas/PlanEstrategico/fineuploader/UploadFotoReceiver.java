package com.lasbambas.PlanEstrategico.fineuploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDao;

public class UploadFotoReceiver extends HttpServlet {
//	private static final File UPLOAD_DIR = new File("test/uploads");
	public static File UPLOAD_DIR;
	public static File TEMP_DIR;

	public static String CONTENT_LENGTH = "Content-Length";
	public static int SUCCESS_RESPONSE_CODE = 200;

	final Logger log = LoggerFactory.getLogger(UploadFotoReceiver.class);

	int accesses = 0;
	
	private EmpleadoDao empleadoDao;
	private int idEmpleado;
	private HttpServletRequest curReq;

	@Override
	public void init() throws ServletException {
		String STORAGE_UNIT = getServletContext().getInitParameter("STORAGE_UNIT");
		UPLOAD_DIR  = new File(STORAGE_UNIT + ":\\Intranet\\PlanEstrategico\\Fotos");
		TEMP_DIR = new File(STORAGE_UNIT + ":\\Intranet\\PlanEstrategico\\FotosTemp");
		UPLOAD_DIR.mkdirs();
		empleadoDao = (EmpleadoDao)getServletContext().getAttribute("empleadoDao");
		/*String AbsolutePath = UPLOAD_DIR.getAbsolutePath();
		System.out.println("AbsolutePath");
		System.out.println(AbsolutePath);*/
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uuid = req.getPathInfo().replaceAll("/", "");
		File curFolder = new File(UPLOAD_DIR, uuid);
		File curFile = curFolder.listFiles()[0];
		String path = curFile.getPath();

		String contentType = Files.probeContentType(Paths.get(path));
		int contentLength = (int)curFile.length();

		resp.setContentType(contentType);
		resp.setContentLength(contentLength);

		FileInputStream in = new FileInputStream(curFile);
		OutputStream out = resp.getOutputStream();
		byte[] buf = new byte[1024];
		int count = 0;
		while ((count = in.read(buf)) >= 0) {
			out.write(buf, 0, count);
		}
		out.close();
		in.close();

	}
	protected String getContentType(String uuid) throws IOException {
		File curFolder = new File(UPLOAD_DIR, uuid);
		File curFile = curFolder.listFiles()[0];
		String path = curFile.getPath();
		
		String contentType = Files.probeContentType(Paths.get(path));
		return contentType;
	}
	@Override
	public void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String uuid = req.getPathInfo().replaceAll("/", "");

		FileUtils.deleteDirectory(new File(UPLOAD_DIR, uuid));

		if (new File(UPLOAD_DIR, uuid).exists()) {
			System.out.println("no se pudo borrar el archivo " + uuid);
			log.warn("couldn't find or delete " + uuid);
		} else {
			System.out.println("se borró el archivo exitosamente");
			log.info("deleted " + uuid);
		}
		
		resp.setStatus(SUCCESS_RESPONSE_CODE);
		// resp.addHeader("Access-Control-Allow-Origin", "*");
	}

	@Override
	public void doOptions(HttpServletRequest req, HttpServletResponse resp) {
		resp.setStatus(SUCCESS_RESPONSE_CODE);
		// resp.addHeader("Access-Control-Allow-Origin",
		// "http://192.168.130.118:8080");
		// resp.addHeader("Access-Control-Allow-Credentials", "true");
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Methods", "POST, DELETE");
		resp.addHeader("Access-Control-Allow-Headers",
				"x-requested-with, cache-control, content-type");
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		this.curReq = req;
		// Custom Line added by dennisbot
		this.idEmpleado = Integer.parseInt(req.getPathInfo().replaceAll("/", ""));
		
		RequestParser requestParser = null;

		boolean isIframe = req.getHeader("X-Requested-With") == null
				|| !req.getHeader("X-Requested-With").equals("XMLHttpRequest");

		try {
			resp.setContentType(isIframe ? "text/html" : "text/plain");
			resp.setStatus(SUCCESS_RESPONSE_CODE);

			// resp.addHeader("Access-Control-Allow-Origin",
			// "http://192.168.130.118:8080");
			// resp.addHeader("Access-Control-Allow-Credentials", "true");
			// resp.addHeader("Access-Control-Allow-Origin", "*");

			if (ServletFileUpload.isMultipartContent(req)) {
				System.out.println("entro a is multipart content");
				MultipartUploadParser multipartUploadParser = new MultipartUploadParser(
						req, TEMP_DIR, getServletContext());
				requestParser = RequestParser.getInstance(req,
						multipartUploadParser);
				writeFileForMultipartRequest(requestParser);
				writeResponse(resp.getWriter(),
						requestParser.generateError() ? "Generated error"
								: null, isIframe, false, requestParser);
				
			} else {
				System.out.println("entro a request parser");
				requestParser = RequestParser.getInstance(req, null);
				writeFileForNonMultipartRequest(req, requestParser);
				writeResponse(resp.getWriter(),
						requestParser.generateError() ? "Generated error"
								: null, isIframe, false, requestParser);
			}
			
			// Custom Line added by dennisbot
			this.addEntryDatabase(requestParser);
			
		} catch (Exception e) {
			log.error("Problem handling upload request", e);
			if (e instanceof MergePartsException) {
				writeResponse(resp.getWriter(), e.getMessage(), isIframe, true,
						requestParser);
			} else {
				writeResponse(resp.getWriter(), e.getMessage(), isIframe,
						false, requestParser);
			}
		}
	}

	private void addEntryDatabase(RequestParser requestParser) throws Exception {
		EmpleadoDTO curEmp = (EmpleadoDTO)WebUtil.obtenerAtributoenSesion(this.curReq, "empleado");
		//borramos el anterior
		if (curEmp.getUuidFoto() != null && !curEmp.getUuidFoto().equals(""))
			FileUtils.deleteDirectory(new File(UPLOAD_DIR, curEmp.getUuidFoto()));
		//establecemos el nuevo uuid
		curEmp.setUuidFoto(requestParser.getUuid());
		empleadoDao.update(curEmp);
		WebUtil.almacenarAtributoenSesion(this.curReq, "empleado", curEmp);
	}

	private void writeFileForNonMultipartRequest(HttpServletRequest req,
			RequestParser requestParser) throws Exception {
		File dir = new File(UPLOAD_DIR, requestParser.getUuid());
		dir.mkdirs();

		String contentLengthHeader = req.getHeader(CONTENT_LENGTH);
		long expectedFileSize = Long.parseLong(contentLengthHeader);

		if (requestParser.getPartIndex() >= 0) {
			writeFile(
					req.getInputStream(),
					new File(dir, requestParser.getUuid()
							+ "_"
							+ String.format("%05d",
									requestParser.getPartIndex())), null);

			if (requestParser.getTotalParts() - 1 == requestParser
					.getPartIndex()) {
				File[] parts = getPartitionFiles(dir, requestParser.getUuid());
				File outputFile = new File(dir, requestParser.getFilename());
				for (File part : parts) {
					mergeFiles(outputFile, part);
				}

				assertCombinedFileIsVaid(requestParser.getTotalFileSize(),
						outputFile, requestParser.getUuid());
				deletePartitionFiles(dir, requestParser.getUuid());
			}
		} else {
			writeFile(req.getInputStream(),
					new File(dir, requestParser.getFilename()),
					expectedFileSize);
		}
	}

	private void writeFileForMultipartRequest(RequestParser requestParser)
			throws Exception {
		File dir = new File(UPLOAD_DIR, requestParser.getUuid());
		dir.mkdirs();

		if (requestParser.getPartIndex() >= 0) {
			writeFile(
					requestParser.getUploadItem().getInputStream(),
					new File(dir, requestParser.getUuid()
							+ "_"
							+ String.format("%05d",
									requestParser.getPartIndex())), null);

			if (requestParser.getTotalParts() - 1 == requestParser
					.getPartIndex()) {
				File[] parts = getPartitionFiles(dir, requestParser.getUuid());
				File outputFile = new File(dir,
						requestParser.getOriginalFilename());
				for (File part : parts) {
					mergeFiles(outputFile, part);
				}

				assertCombinedFileIsVaid(requestParser.getTotalFileSize(),
						outputFile, requestParser.getUuid());
				deletePartitionFiles(dir, requestParser.getUuid());
			}
		} else {
			writeFile(requestParser.getUploadItem().getInputStream(), new File(
					dir, requestParser.getFilename()), null);
		}
	}

	private void assertCombinedFileIsVaid(int totalFileSize, File outputFile,
			String uuid) throws MergePartsException {
		if (totalFileSize != outputFile.length()) {
			deletePartitionFiles(UPLOAD_DIR, uuid);
			outputFile.delete();
			throw new MergePartsException("Incorrect combined file size!");
		}

	}

	private static class PartitionFilesFilter implements FilenameFilter {
		private String filename;

		PartitionFilesFilter(String filename) {
			this.filename = filename;
		}

		@Override
		public boolean accept(File file, String s) {
			return s.matches(Pattern.quote(filename) + "_\\d+");
		}
	}

	private static File[] getPartitionFiles(File directory, String filename) {
		File[] files = directory.listFiles(new PartitionFilesFilter(filename));
		Arrays.sort(files);
		return files;
	}

	private static void deletePartitionFiles(File directory, String filename) {
		File[] partFiles = getPartitionFiles(directory, filename);
		for (File partFile : partFiles) {
			partFile.delete();
		}
	}

	private File mergeFiles(File outputFile, File partFile) throws Exception {
		FileOutputStream fos;
		FileInputStream fis;
		byte[] fileBytes;
		int bytesRead = 0;
		fos = new FileOutputStream(outputFile, true);
		fis = new FileInputStream(partFile);
		fileBytes = new byte[(int) partFile.length()];
		bytesRead = fis.read(fileBytes, 0, (int) partFile.length());
		assert (bytesRead == fileBytes.length);
		assert (bytesRead == (int) partFile.length());
		fos.write(fileBytes);
		fos.flush();
		fis.close();
		fos.close();

		return outputFile;
	}

	private File writeFile(InputStream in, File out, Long expectedFileSize)
			throws IOException {
		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(out);

			IOUtils.copy(in, fos);

			if (expectedFileSize != null) {
				Long bytesWrittenToDisk = out.length();
				if (!expectedFileSize.equals(bytesWrittenToDisk)) {
					log.warn(
							"Expected file {} to be {} bytes; file on disk is {} bytes",
							new Object[] { out.getAbsolutePath(),
									expectedFileSize, 1 });
					out.delete();
					throw new IOException(
							String.format(
									"Unexpected file size mismatch. Actual bytes %s. Expected bytes %s.",
									bytesWrittenToDisk, expectedFileSize));
				}
			}

			return out;
		} catch (Exception e) {
			throw new IOException(e);
		} finally {
			IOUtils.closeQuietly(fos);
		}
	}

	private void writeResponse(PrintWriter writer, String failureReason,
			boolean isIframe, boolean restartChunking,
			RequestParser requestParser) {
		if (failureReason == null) {
			// if (isIframe)
			// {
			// writer.print("{\"success\": true, \"uuid\": \"" +
			// requestParser.getUuid() +
			// "\"}<script src=\"http://192.168.130.118:8080/client/js/iframe.xss.response.js\"></script>");
			// }
			// else
			// {
			writer.print("{\"uuid\": \"" + requestParser.getUuid() + "\",\"success\": true}");
			// }
		} else {
			if (restartChunking) {
				writer.print("{\"error\": \"" + failureReason
						+ "\", \"reset\": true}");
			} else {
				// if (isIframe)
				// {
				// writer.print("{\"error\": \"" + failureReason +
				// "\", \"uuid\": \"" + requestParser.getUuid() +
				// "\"}<script src=\"http://192.168.130.118:8080/client/js/iframe.xss.response.js\"></script>");
				// }
				// else
				// {
				writer.print("{\"error\": \"" + failureReason + "\"}");
				// }
			}
		}
	}

	private class MergePartsException extends Exception {
		MergePartsException(String message) {
			super(message);
		}
	}
}
