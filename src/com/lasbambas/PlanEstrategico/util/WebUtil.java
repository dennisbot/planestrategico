/* WebUtil - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.lasbambas.PlanEstrategico.util;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class WebUtil
{
    public static HttpSession obtenerSesion(HttpServletRequest req) {
    	return req.getSession();
    }

    public static Object obtenerAtributo(HttpServletRequest request,
					 String nombreAtributo) {
		Object objeto = null;
		Enumeration enumera = request.getAttributeNames();
		while (enumera.hasMoreElements()) {
		    String atributo = (String) enumera.nextElement();
		    if (nombreAtributo.equals(atributo)) {
			objeto = request.getAttribute(atributo);
			break;
		    }
		}
		return objeto;
    }

    public static Object obtenerAtributoenSesion(HttpServletRequest request,
						 String nombreAtributo) {
		Object objeto = null;
		HttpSession sesion = obtenerSesion(request);
		Enumeration enumera = sesion.getAttributeNames();
		while (enumera.hasMoreElements()) {
		    String atributo = (String) enumera.nextElement();
		    if (nombreAtributo.equals(atributo)) {
			objeto = sesion.getAttribute(atributo);
			break;
		    }
		}
		return objeto;
    }

    public static void almacenarAtributo(HttpServletRequest request,
					 String nombreAtributo, Object valor) {
    	request.setAttribute(nombreAtributo, valor);
    }

    public static void almacenarAtributoenSesion
	(HttpServletRequest request, String nombreAtributo, Object valor) {
		HttpSession sesion = obtenerSesion(request);
		sesion.setAttribute(nombreAtributo, valor);
    }

    public static void eliminarAtributo(HttpServletRequest request,
					String nombreAtributo) {
		Enumeration enumera = request.getAttributeNames();
		while (enumera.hasMoreElements()) {
		    String atributo = (String) enumera.nextElement();
		    if (nombreAtributo.equals(atributo)) {
			request.removeAttribute(nombreAtributo);
			break;
		    }
		}
    }

    public static void eliminarAtributoenSesion(HttpServletRequest request,
						String nombreAtributo) {
		HttpSession sesion = obtenerSesion(request);
		Enumeration enumera = sesion.getAttributeNames();
		while (enumera.hasMoreElements()) {
		    String atributo = (String) enumera.nextElement();
		    if (nombreAtributo.equals(atributo)) {
			sesion.removeAttribute(nombreAtributo);
			break;
		    }
		}
    }

    public static void eliminarSesion(HttpServletRequest req) {
    	obtenerSesion(req).invalidate();
    }

    public static String obtenerRutaFisicaContexto(HttpServletRequest req,
						   String rutaContexto) {
	return obtenerSesion(req).getServletContext()
		   .getRealPath(rutaContexto);
    }

    public static Cookie generarCookie(String nombre, String valor) {
		Cookie ck = new Cookie(nombre, valor);
		ck.setPath("/PlanEstrategico");
		// cookie para 4 horas de sesi�n
		ck.setMaxAge(60 * 60 * 4);
		return ck;
    }

    public static String obtenerValorCookie(HttpServletRequest req,
					    String nombreCookie) {
		String valor = null;
		Cookie[] cs = req.getCookies();

		if (cs == null) return null;

		for (int i = 0; i < cs.length; i++) {
		    String nombre = cs[i].getName();
		    if (nombreCookie.equals(nombre)) {
				valor = cs[i].getValue();
				break;
		    }
		}
		return valor;
    }
    public static void eliminarCookie (
    		HttpServletRequest req,
    		HttpServletResponse resp,
    		String nombreCookie
    		) {
    	resp.setContentType("text/html");
    	Cookie cookie = new Cookie(nombreCookie, "");
    	cookie.setDomain("localhost");
    	cookie.setMaxAge(-1);
    	cookie.setPath("/");
    	//cookie.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
    	resp.addCookie(cookie);
    }
	public static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}
	public static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columns = rsmd.getColumnCount();
        for (int x = 1; x <= columns; x++) {
            if (columnName.equals(rsmd.getColumnName(x))) {
                return true;
            }
        }
        return false;
    }
}
