package com.lasbambas.PlanEstrategico.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.metrica.MetricaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.metrica.MetricaDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.proceso.ProcesoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.proceso.ProcesoDao;

public class ExcelKPIMigrator {
	private ProcesoDao procesoDao;
	private MetricaDao metricaDao;

	public void setProcesoDao(ProcesoDao procesoDao) {
		this.procesoDao = procesoDao;
	}
	
	public void setMetricaDao(MetricaDao metricaDao) {
		this.metricaDao = metricaDao;
	}

	public String updateDatabaseWithFile(String filepath, String filename) throws IOException {
//		System.out.println("filepath:");
//		System.out.println(filepath);
		try {
			FileInputStream file = new FileInputStream(new File(filepath + "\\" + filename));

			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(2);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			
			ProcesoDTO proceso = new ProcesoDTO();
			//saltamos la linea de los encabezados
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				// avanzamos la celda vacía
				Cell cell = cellIterator.next();
				if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
					// procesamos la descripción del proceso
					String descripcionProceso = cell.getStringCellValue();
					// avanzamos una fila porque la info que nos interesa está una fila debajo
					row = rowIterator.next();
					// y recuperamos el iterador de la siguiente fila
					cellIterator = row.cellIterator();
					// recogemos el cod del proceso tmb
					String codProceso = this.getCellValue(cellIterator);
					proceso = new ProcesoDTO();
					proceso.setCodProceso(codProceso);
					proceso.setDescripcion(descripcionProceso);
					// guardamos (se crear o actualiza según sea el caso)
					proceso = procesoDao.saveCod(proceso);
				}
				String codSubProceso = this.getCellValue(cellIterator);
				String descripcionSubProceso = this.getCellValue(cellIterator);
				ProcesoDTO subProceso = new ProcesoDTO();
				subProceso.setCodProceso(codSubProceso);
				subProceso.setDescripcion(descripcionSubProceso);
				subProceso.setProcesoParent(proceso);
				// guardamos (se crea o actualiza según sea el caso)
				subProceso = procesoDao.saveCod(subProceso);
				String codKPI = this.getCellValue(cellIterator);
				String nombreKPI = this.getCellValue(cellIterator);
				String descripcionKPI = this.getCellValue(cellIterator);
				String unidadMedida = this.getCellValue(cellIterator);
				String formula = this.getCellValue(cellIterator);
				String nivelKpi = this.getCellValue(cellIterator);
				String observaciones = this.getCellValue(cellIterator);
				MetricaDTO metrica = new MetricaDTO();
				metrica.setCodKpi(codKPI);
				metrica.setDescripcionMetrica(nombreKPI);
				metrica.setDefinicionExplicacion(descripcionKPI);
				metrica.setUnidadMedida(unidadMedida);
				metrica.setFormula(formula);
				metrica.setNivelKpi(nivelKpi);
				metrica.setObservaciones(observaciones);
				metrica.setSubProceso(subProceso);
				metricaDao.saveCod(metrica);
			}
			file.close();
			return "migración realizada con éxito";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		finally {
			//eliminamos el archivo
			FileUtils.deleteDirectory(new File(filepath));
		}
	}
	public String getCellValue(Iterator<Cell> cellIterator) {
		if (!cellIterator.hasNext()) return "";
		Cell cell = cellIterator.next();
		switch (cell.getCellType()) {
			case Cell.CELL_TYPE_BLANK: return "";
			case Cell.CELL_TYPE_NUMERIC: return String.valueOf(cell.getNumericCellValue()).trim();
			case Cell.CELL_TYPE_BOOLEAN: return String.valueOf(cell.getBooleanCellValue()).trim();
			default: return cell.getStringCellValue().trim();
		}
	}
	public String updateDatabaseWithFile2(String filepath, String filename) throws IOException {
//		System.out.println("filepath:");
//		System.out.println(filepath);
		try {
			FileInputStream file = new FileInputStream(new File(filepath + "\\" + filename));

			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(3);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			
			ProcesoDTO proceso = new ProcesoDTO();
			ProcesoDTO subProceso = new ProcesoDTO();
			//saltamos la linea de los encabezados
			rowIterator.next();
			ObjectMapper mapper = new ObjectMapper();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				// avanzamos la celda vacía
				Cell cell = cellIterator.next();
				if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
					// procesamos la descripción del proceso
					String descripcionProceso = cell.getStringCellValue();
					// avanzamos una fila porque la info que nos interesa está una fila debajo
					row = rowIterator.next();
					// y recuperamos el iterador de la siguiente fila
					cellIterator = row.cellIterator();
					// recogemos el cod del proceso tmb
					String codProceso = this.getCellValue(cellIterator);
					proceso = new ProcesoDTO();
					proceso.setCodProceso(codProceso);
					proceso.setDescripcion(descripcionProceso);
					// guardamos (se crear o actualiza según sea el caso)
					proceso = procesoDao.saveCod(proceso);
					subProceso = new ProcesoDTO();
				}
				// recuperamos el valor de la celda (2da columna)
				cell = cellIterator.next();
				if (cell.getCellType() != cell.CELL_TYPE_BLANK) {
					String descripcionSubProceso = cell.getStringCellValue();
					
					cellIterator = rowIterator.next().cellIterator();
					// avanzamos la primera columna (corresponde a proceso y lo que necesitamos es subproceso)
					cellIterator.next();
					String codSubProceso = this.getCellValue(cellIterator);
					
					subProceso = new ProcesoDTO();
					subProceso.setCodProceso(codSubProceso);
					subProceso.setDescripcion(descripcionSubProceso);
					subProceso.setProcesoParent(proceso);
					
					// guardamos (se crea o actualiza según sea el caso)
					subProceso = procesoDao.saveCod(subProceso);
				}
				
				String codKPI = this.getCellValue(cellIterator);
				String nombreKPI = this.getCellValue(cellIterator);
				String descripcionKPI = this.getCellValue(cellIterator);
				String unidadMedida = this.getCellValue(cellIterator);
				String formula = this.getCellValue(cellIterator);
				String nivelKpi = this.getCellValue(cellIterator);
				String observaciones = this.getCellValue(cellIterator);
				
				MetricaDTO metrica = new MetricaDTO();
				metrica.setCodKpi(codKPI);
				metrica.setDescripcionMetrica(nombreKPI);
				metrica.setDefinicionExplicacion(descripcionKPI);
				metrica.setUnidadMedida(unidadMedida);
				metrica.setFormula(formula);
				metrica.setNivelKpi(nivelKpi);
				metrica.setObservaciones(observaciones);
				metrica.setSubProceso(subProceso);

				if (subProceso.getCodProceso() != null)
					metricaDao.saveCod(metrica);
			}
			file.close();
			return "migración realizada con éxito";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		finally {
			//eliminamos el archivo
			FileUtils.deleteDirectory(new File(filepath));
		}
	}
}
