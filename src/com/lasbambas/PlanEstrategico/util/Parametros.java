package com.lasbambas.PlanEstrategico.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.archivo.ArchivoDao;

/**
 * Servlet implementation class Parametros
 */
public class Parametros extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private DataSource dataSource;
	public Parametros() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// response.setContentType("application/vnd.ms-excel");
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition",
				"attachment; filename=parametros.xlsx");
		try {
			this.generateParametrosFile(response.getOutputStream());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateParametrosFile(ServletOutputStream sos)
			throws ClassNotFoundException, SQLException {
//		String userName = "dennisbot";
//		String password = "controladores";
//		String url = "jdbc:sqlserver://localhost:1433;databaseName=Portal";
//		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//		Connection conn = DriverManager.getConnection(url, userName, password);
		dataSource = (DataSource)getServletContext().getAttribute("dataSourceSqlServer");
		Connection conn = dataSource.getConnection();
		
		Statement statement = conn.createStatement();
		String sql = "SELECT * FROM PESTRATEGICO2.METRICA_KPI ORDER BY ID_METRICA";
		ResultSet rs = statement.executeQuery(sql);
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheetCategorias = workbook.createSheet("categorias");
		XSSFSheet sheetCatOk = workbook.createSheet("categorias_ok");
		XSSFSheet sheetKPIs = workbook.createSheet("kpis");
		// this data needs to be written (Object[])
		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		int rowcount = 0;
		data.put(String.valueOf(rowcount++), new Object[] {
				"DESCRIPCION_METRICA", "COD_KPI", "ID_METRICA",
				"DEFINICION_EXPLICACION", "UNIDAD_MEDIDA", "FORMULA",
				"NIVEL_KPI", "OBSERVACIONES", "ID_SUB_PROCESO" });

		while (rs.next()) {
			data.put(
					rs.getString("COD_KPI"),
					new Object[] { rs.getString("DESCRIPCION_METRICA"),
							rs.getString("COD_KPI"),
							Integer.valueOf(rs.getString("ID_METRICA")),
							rs.getString("DEFINICION_EXPLICACION"),
							rs.getString("UNIDAD_MEDIDA"),
							rs.getString("FORMULA"), rs.getString("NIVEL_KPI"),
							rs.getString("OBSERVACIONES"),
							Integer.valueOf(rs.getString("ID_SUB_PROCESO")) });
		}
		rs.close();

		Set<String> keyset = data.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheetKPIs.createRow(rownum++);

			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				}
				if (obj instanceof Integer) {
					cell.setCellValue((Integer) obj);
				}
			}
		}

		Name namedCell = workbook.createName();
		namedCell.setNameName("KPIS");
		String reference = "kpis!$A$2:$A$" + rownum;
		namedCell.setRefersToFormula(reference);

		// para las categorias
		rowcount = 0;
		data = new TreeMap<String, Object[]>();
		data.put(String.valueOf(rowcount++), new Object[] { "ID_CATEGORIA",
				"DESCRIPCION_CATEGORIA", "ABREVIATURA_CATEGORIA", "PESO",
				"NIVEL", "SIGNIFICANCIA", "ID_CATEGORIA_PARENT" });

		sql = "SELECT * FROM PESTRATEGICO2.CATEGORIA ORDER BY ID_CATEGORIA";
		rs = statement.executeQuery(sql);

		while (rs.next()) {
			// System.out.println(rs.getString("ID_CATEGORIA"));
			data.put(
					rs.getString("ID_CATEGORIA"),
					new Object[] {
							Integer.valueOf(rs.getString("ID_CATEGORIA")),
							rs.getString("DESCRIPCION_CATEGORIA"),
							rs.getString("ABREVIATURA_CATEGORIA"),
							Integer.valueOf(rs.getString("PESO")),
							Integer.valueOf(rs.getString("NIVEL")),
							rs.getString("SIGNIFICANCIA"),
							rs.getString("ID_CATEGORIA_PARENT") == null ? ""
									: Integer.valueOf(rs
											.getString("ID_CATEGORIA_PARENT")) });
		}
		keyset = data.keySet();
		rownum = 0;
		for (String key : keyset) {
			Row row = sheetCategorias.createRow(rownum++);

			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				} else {
					if (obj instanceof Integer) {
						cell.setCellValue((Integer) obj);
					}
				}
			}
		}
		// para las cats_ok
		rowcount = 0;
		data = new TreeMap<String, Object[]>();
		data.put(String.valueOf(rowcount++), new Object[] {
				"DESCRIPCION_CATEGORIA", "ID_CATEGORIA",
				"ABREVIATURA_CATEGORIA", "BAJO", "MEDIO", "ALTO" });

		sql = "SELECT  " + "C.DESCRIPCION_CATEGORIA , " + "C.ID_CATEGORIA , "
				+ "C.ABREVIATURA_CATEGORIA , " + "CAT_PVT.BAJO , "
				+ "CAT_PVT.MEDIO , " + "CAT_PVT.ALTO " + "FROM  " + "( "
				+ "SELECT * FROM " + "( "
				+ "SELECT CP.ID_CATEGORIA, C.PESO, C.SIGNIFICANCIA "
				+ "FROM PESTRATEGICO2.CATEGORIA C "
				+ "INNER JOIN PESTRATEGICO2.CATEGORIA CP " + "ON  "
				+ "CP.ID_CATEGORIA = C.ID_CATEGORIA_PARENT " + "AND  "
				+ "C.ID_CATEGORIA_PARENT IS NOT NULL " + ") AS S " + "PIVOT "
				+ "( " + "SUM(S.PESO) "
				+ "FOR SIGNIFICANCIA IN (BAJO, MEDIO, ALTO) " + ") AS PVT     "
				+ ") CAT_PVT " + "INNER JOIN " + "PESTRATEGICO2.CATEGORIA C "
				+ "ON  " + "C.ID_CATEGORIA = CAT_PVT.ID_CATEGORIA ";
		rs = statement.executeQuery(sql);

		while (rs.next()) {
			// System.out.println(rs.getString("ID_CATEGORIA"));
			data.put(
					rs.getString("ID_CATEGORIA"),
					new Object[] { rs.getString("DESCRIPCION_CATEGORIA"),
							Integer.valueOf(rs.getString("ID_CATEGORIA")),
							rs.getString("ABREVIATURA_CATEGORIA"),
							Integer.valueOf(rs.getString("BAJO")),
							Integer.valueOf(rs.getString("MEDIO")),
							Integer.valueOf(rs.getString("ALTO")) });
		}
		keyset = data.keySet();
		rownum = 0;
		for (String key : keyset) {
			Row row = sheetCatOk.createRow(rownum++);

			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				} else {
					if (obj instanceof Integer) {
						cell.setCellValue((Integer) obj);
					}
				}
			}
		}
		namedCell = workbook.createName();
		namedCell.setNameName("CATEGORIAS");
		reference = "categorias_ok!$A$2:$A$" + rownum;
		namedCell.setRefersToFormula(reference);
		try {
			workbook.write(sos);
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		conn.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
