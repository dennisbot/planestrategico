package com.lasbambas.PlanEstrategico.ws.servicios;

import org.glassfish.jersey.server.ResourceConfig;

import com.lasbambas.PlanEstrategico.ws.servicios.rest.CharlasNotificationsService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.PEstrategicoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.ArchivoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.AreaInspeccionadaService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.BPInService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.CategoriaService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.DescripcionNivelService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.EmpleadoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.ItemEstrategicoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.DependenciaOrganizacionalService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.MetricaService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.PeriodoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.PermisoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.PlanEstrategicoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.ProcesoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.ResponsableService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.RolService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.RolVistaPermisoService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.TipoMetricaService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.UnidadMedidaService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.UtilsService;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.services.VistaService;

public class RESTApplication extends ResourceConfig {

    public RESTApplication() {
//    	register(RequestContextFilter.class);
//        register(PEstrategicoService.class);
//        register(CharlasNotificationsService.class);
        
        register(ItemEstrategicoService.class);
        register(DescripcionNivelService.class);
        register(UnidadMedidaService.class);
        register(MetricaService.class);
        register(TipoMetricaService.class);
        register(DependenciaOrganizacionalService.class);
        register(PlanEstrategicoService.class);
        register(AreaInspeccionadaService.class);
        register(PeriodoService.class);
        register(CategoriaService.class);
        register(ResponsableService.class);
        register(EmpleadoService.class);
        register(UtilsService.class);
        register(ArchivoService.class);
        register(BPInService.class);
        //parametros
        register(RolService.class);
        register(PermisoService.class);
        register(VistaService.class);
        register(RolVistaPermisoService.class);
        register(ProcesoService.class);
//        register(MyObjectMapperProvider.class);
//        register(JacksonFeature.class);
    }
}