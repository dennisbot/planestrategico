package com.lasbambas.PlanEstrategico.ws.servicios.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.velocity.app.VelocityEngine;
import org.glassfish.jersey.server.JSONP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.tools.correo.CorreoFacade;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.empleado.EmpleadoDao;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.gerencia.GerenciaDao;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.superintendencia.SuperintendenciaDao;
import com.pe.xstratacopper.xslb.xmine.util.BUtil;

@Path("/")
@Component
public class CharlasNotificationsService {
	@Autowired
	CorreoFacade correoFacade;
	@Autowired
	EmpleadoDao empleadoDao;
	@Autowired
	GerenciaDao gerenciaDao;
	@Autowired
	SuperintendenciaDao superintendenciaDao;
	

    @Autowired
    VelocityEngine velocityEngine;

    @Context ServletContext context;

	@GET
	@Path("/testa/{idsup}")
	@JSONP
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<EmpleadoDTO> usuarios(@PathParam("idsup") int idSup,
			@Context HttpServletRequest request) {
			return empleadoDao.mostrarSuperintendentePorSuperintendencia(idSup);
	}

	@GET
	@Path("/charla_prueba")
	@Produces(MediaType.TEXT_PLAIN)
	public String prueba() {
		return "hola dennis desde reportes seguridad \n"
				+ BUtil.getCurDate() + '\n'
				+ BUtil.getCurrentTimeStamp();
	}
}
