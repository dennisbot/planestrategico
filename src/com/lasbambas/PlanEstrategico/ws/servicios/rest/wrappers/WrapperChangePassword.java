package com.lasbambas.PlanEstrategico.ws.servicios.rest.wrappers;

import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;


public class WrapperChangePassword {
	public EmpleadoDTO empleado;
	public String oldPassword;
	public String newPassword;
}
