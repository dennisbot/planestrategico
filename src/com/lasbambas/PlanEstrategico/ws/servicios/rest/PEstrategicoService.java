package com.lasbambas.PlanEstrategico.ws.servicios.rest;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.velocity.app.VelocityEngine;
import org.glassfish.jersey.server.JSONP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lasbambas.PlanEstrategico.ws.servicios.rest.wrappers.WrapperBatchTest;
import com.pe.xstratacopper.xslb.xmine.actionRegister.dao.itemAction.ItemActionDTO;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.maestro.empleado.EmpleadoFacade;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.tools.correo.CorreoFacade;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.empleado.EmpleadoDao;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.gerencia.GerenciaDao;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.superintendencia.SuperintendenciaDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.planEstrategico.PlanEstrategicoDao;
import com.pe.xstratacopper.xslb.xmine.util.BUtil;
import com.pe.xstratacopper.xslb.xmine.util.ServiceResponse;

@Path("/")
@Component
public class PEstrategicoService {
	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String HEADER_AUTHORIZATION_DEFAULT_ACCESS_TOKEN = "TGFzIEJhbWJhcyBNSVMgRGFzaGJvYXJk";// base
																												// 64
																												// encode:
																												// Las
																												// Bambas
																												// MIS
																												// SBC
	public static final String HEADER_AUTHORIZATION_VALUE = "Basic "
			+ HEADER_AUTHORIZATION_DEFAULT_ACCESS_TOKEN;

	@Autowired
	CorreoFacade correoFacade;

	@Autowired
	EmpleadoFacade empleadoFacade;

	@Autowired
	GerenciaDao gerenciaDao;

	@Autowired
	SuperintendenciaDao superintendenciaDao;

    @Autowired
	EmpleadoDao empleadoDao;
    @Autowired
    PlanEstrategicoDao planEstrategicoDao;

    @Autowired
    VelocityEngine velocityEngine;

    @Context ServletContext context;

	interface RetrieveData {
		public void retrieveData(ServiceResponse response) throws Exception;
	}

	private ServiceResponse getData(HttpServletRequest request,
			RetrieveData handler) {
		ServiceResponse response = new ServiceResponse();
		if (request.getHeader(HEADER_AUTHORIZATION) != null
				&& request.getHeader(HEADER_AUTHORIZATION).equals(
						HEADER_AUTHORIZATION_VALUE)) {
			response.getHeader().setStatus(Response.Status.OK.getStatusCode());
			try {
				handler.retrieveData(response);
			} catch (Exception e) {
				e.printStackTrace();
				response.getHeader().setStatus(
						Response.Status.BAD_REQUEST.getStatusCode());
			}
		} else {
			response.getHeader().setStatus(
					Response.Status.UNAUTHORIZED.getStatusCode());
		}
		return response;
	}

	@GET
	@Path("/prueba")
	@Produces(MediaType.TEXT_PLAIN)
	public String prueba() {
		return "hola dennis desde reportes seguridad \n"
				+ BUtil.getCurDate() + '\n'
				+ BUtil.getCurrentTimeStamp();
	}
	@GET
	@Path("/base_url")
	@Produces(MediaType.TEXT_PLAIN)
	public String baseurl() {
		return context.getInitParameter("BASE_URL");
	}
	@GET
	@Path("/destino")
	@Produces(MediaType.TEXT_PLAIN)
	public String destino() {
		return context.getInitParameter("CORREOS_TEST");
	}

	@GET
	@Path("/conguardias")
	@Produces(MediaType.APPLICATION_JSON)
	public List conguardias() throws Exception {
		return superintendenciaDao.findAllWithGuardias();
	}

	// @POST @Path("/registrarItemActions")
	// @JSONP
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces({MediaType.APPLICATION_JSON})
	// public void registrarItemActions(List<ItemActionDTO> itemActions,
	// @Context HttpServletRequest request) {
	//
	// }
	@GET
	@Path("/parametrosInspeccion")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Object> parametrosInspeccion() {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		try {
//			mapa.put("lugaresTrabajo", lugarAreaTrabajoDao.findAll());
			mapa.put("empleados", empleadoDao.findAllFromDSSTT());

		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
		}
		return mapa;
	}

	@GET
	@Path("/test/{idsup}")
	@JSONP
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public List<EmpleadoDTO> usuarios(@PathParam("idsup") int idSup,
			@Context HttpServletRequest request) {
			return empleadoDao.mostrarSuperintendentePorSuperintendencia(idSup);
	}

	@POST
	@Path("/testBatch")
	@JSONP
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public void testBatch(WrapperBatchTest wrapperTest,
			@Context HttpServletRequest request) {
		try {
			List<ItemActionDTO> itemActions = wrapperTest.itemActions;
			EmpleadoDTO empleado = wrapperTest.empleado;
		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
		}
	}

}
