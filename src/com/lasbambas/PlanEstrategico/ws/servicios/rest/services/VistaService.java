package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.vista.VistaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.vista.VistaDao;

@Path("/vista")
public class VistaService {

	@Autowired
    VistaDao vistaDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<VistaDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return vistaDao.findAll();
    }
    
    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public VistaDTO findById(@PathParam("id") int idParametro) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return vistaDao.findById(idParametro);
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public VistaDTO create(VistaDTO parametroDetalle) throws Exception {
        return vistaDao.create(parametroDetalle);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public VistaDTO update(VistaDTO parametroDetalle) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	vistaDao.update(parametroDetalle);
        return parametroDetalle;
    }

	@DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int idParametro) throws Exception {
		vistaDao.remove(idParametro);    	
    }

}