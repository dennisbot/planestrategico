package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.responsable.ResponsableDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.responsable.ResponsableDao;

@Path("/responsable")
public class ResponsableService {

	@Autowired
    ResponsableDao responsableDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ResponsableDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return responsableDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ResponsableDTO> findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return responsableDao.findByIdItemEstrategico(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ResponsableDTO create(ResponsableDTO responsableDTO) throws Exception {
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(responsableDTO);
    	// System.out.println("responsableDTO:");
    	// System.out.println(val);
        return responsableDao.create(responsableDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ResponsableDTO update(ResponsableDTO responsableDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	responsableDao.update(responsableDTO);
        return responsableDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") String idResponsable_idItemEstrategico) throws Exception {
    	System.out.println("idResponsable_idItemEstrategico");
    	System.out.println(idResponsable_idItemEstrategico);
    	responsableDao.remove(idResponsable_idItemEstrategico);
    }

}