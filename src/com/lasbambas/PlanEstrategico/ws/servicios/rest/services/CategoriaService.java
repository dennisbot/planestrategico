package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.categoria.CategoriaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.categoria.CategoriaDao;

@Path("/categoria")
public class CategoriaService {

	@Autowired
    CategoriaDao categoriaDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<CategoriaDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return categoriaDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public CategoriaDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return categoriaDao.findById(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public CategoriaDTO create(CategoriaDTO categoriaDTO) throws Exception {
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(categoriaDTO);
    	// System.out.println(val);
        return categoriaDao.create(categoriaDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public CategoriaDTO update(CategoriaDTO categoriaDTO) throws Exception {
    	// System.out.println("updating:");
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(categoriaDTO);
    	// System.out.println(val);
    	categoriaDao.update(categoriaDTO);
        return categoriaDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
    	System.out.println("llega a eliminar");
    	categoriaDao.removeChild(id);
    }

}