package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.metrica.MetricaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.metrica.MetricaDao;

@Path("/metrica_kpi")
public class MetricaService {

	@Autowired
    MetricaDao metricaDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<MetricaDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return metricaDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MetricaDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return metricaDao.findById(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MetricaDTO create(MetricaDTO metricaDTO) throws Exception {
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(metricaDTO);
    	// System.out.println("metricaDTO");
    	// System.out.println(val);
        return metricaDao.create(metricaDTO);
    }
    
    @POST @Path("remove_bulk")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public int remove_bulk(List<MetricaDTO> metricas) throws Exception {
    	boolean ok;
    	int deleted = 0;
    	for (MetricaDTO metrica : metricas) {    		
    		ok = metricaDao.remove(metrica.getIdMetrica());
    		if (ok) deleted++; 
    	}
    	return deleted;
    }    

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public MetricaDTO update(MetricaDTO metricaDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	metricaDao.update(metricaDTO);
        return metricaDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
    	metricaDao.remove(id);
    }

}