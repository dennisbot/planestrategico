package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.lasbambas.PlanEstrategico.ws.servicios.rest.wrappers.WrapperChangePassword;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpRolDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDao;

@Path("/empleado")
public class EmpleadoService {

	@Autowired
    EmpleadoDao empleadoDao;
	@Context HttpServletRequest request;
	
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<EmpleadoDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return empleadoDao.findAll();
    }
    @POST @Path("batch")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public boolean create(List<EmpRolDTO> empRolCollection) throws Exception {
//    	ObjectMapper mapper = new ObjectMapper();
//    	String val = mapper.writeValueAsString(rvpCollection);
//    	System.out.println("rvpCollection");
//    	System.out.println(val);
        return empleadoDao.create(empRolCollection);
    }
    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public EmpleadoDTO findById(@PathParam("id") int idEmpleado) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return empleadoDao.findById(idEmpleado);
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public EmpleadoDTO create(EmpleadoDTO empleado) throws Exception {
        return empleadoDao.create(empleado);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public EmpleadoDTO update(EmpleadoDTO empleado) throws Exception {
     	EmpleadoDTO curEmp =  (EmpleadoDTO)WebUtil.obtenerAtributoenSesion(request, "empleado");
     	// debemos cambiar el usuario ya que ha actualizado sus datos
     	if (curEmp.getIdEmpleado() == empleado.getIdEmpleado()) {
     		WebUtil.almacenarAtributoenSesion(request, "empleado", empleado);
     	}
    	empleadoDao.update(empleado);
        return empleado;
    }

	@DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int idParametro) throws Exception {
		empleadoDao.remove(idParametro);    	
    }
	@POST @Path("changePassword")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     public Response changePassword(WrapperChangePassword wChangePassword) throws Exception {
			EmpleadoDTO empleado = wChangePassword.empleado, sameEmp;
			
			empleado.setPassword(wChangePassword.oldPassword);
			Map<String, Object> m = new HashMap<String, Object>();
			sameEmp = empleadoDao.canLogin(empleado);
			m.put("error", false);
			if (sameEmp != null) {
				empleado.setPassword(wChangePassword.newPassword);
				empleadoDao.changePassword(empleado);
				m.put("message", "contraseña cambiada exitosamente");
			}
			else {
				m.put("error", true);
				m.put("message", "la contraseña antigua no es correcta");
			}
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(m);
//			String val = mapper.writeValueAsString(empleado);
//			System.out.println("empleado as json:");
//			System.out.println(val);
//	        return empleadoDao.create(empleado);
			return Response.status(Response.Status.OK).entity(json).build();
	        
	 }
}