package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.pe.xstratacopper.xslb.xmine.reportesSeguridad.dao.areaInspeccionada.AreaInspeccionadaDTO;
import com.pe.xstratacopper.xslb.xmine.reportesSeguridad.dao.areaInspeccionada.AreaInspeccionadaDao;

@Path("/areaInspeccionada")
public class AreaInspeccionadaService {

	@Autowired
    AreaInspeccionadaDao areaInspeccionadaDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<AreaInspeccionadaDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return areaInspeccionadaDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public AreaInspeccionadaDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return areaInspeccionadaDao.findById(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public AreaInspeccionadaDTO create(AreaInspeccionadaDTO areaInspeccionadaDTO) throws Exception {
        return areaInspeccionadaDao.create(areaInspeccionadaDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public AreaInspeccionadaDTO update(AreaInspeccionadaDTO areaInspeccionadaDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
        areaInspeccionadaDao.update(areaInspeccionadaDTO);
        return areaInspeccionadaDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
        areaInspeccionadaDao.remove(id);
    }

}