package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.descripcionNivel.DescripcionNivelDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.descripcionNivel.DescripcionNivelDao;

@Path("/descripcionNivel")
public class DescripcionNivelService {

	@Autowired
	DescripcionNivelDao descripcionNivelDao;

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<DescripcionNivelDTO> findAll() throws Exception {
		// System.out.println("findAll");
		return descripcionNivelDao.findAll();
	}

	// @GET @Path("search/{query}")
	// @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	// public List<GerenciaDTO> findByName(@PathParam("query") String query) {
	// System.out.println("findByName: " + query);
	// return superintendenciaDao.findByName(query);
	// }

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DescripcionNivelDTO findById(@PathParam("id") String id)
			throws NumberFormatException, Exception {
		// System.out.println("findById " + id);
		return descripcionNivelDao.findById(Integer.parseInt(id));
	}

	@GET
	@Path("exist/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public boolean exist(@PathParam("id") int id) throws NumberFormatException,
			Exception {
		System.out.println("exist " + id);
		return descripcionNivelDao.exist(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DescripcionNivelDTO create(DescripcionNivelDTO descripcionNivelDTO)
			throws Exception {
		// ObjectMapper mapper = new ObjectMapper();
		// System.out.println("descripcionNivelDTO CREATE:");
		// String val = mapper.writeValueAsString(descripcionNivelDTO);
		// System.out.println(val);
		return descripcionNivelDao.create(descripcionNivelDTO);
	}

	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public DescripcionNivelDTO update(DescripcionNivelDTO descripcionNivelDTO)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		System.out.println("descripcionNivelDTO UPDATE:");
		String val = mapper.writeValueAsString(descripcionNivelDTO);
		System.out.println(val);
		return descripcionNivelDao.update(descripcionNivelDTO);
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void remove(@PathParam("id") int id) throws Exception {
		descripcionNivelDao.remove(id);
	}

}