package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.unidadMedida.UnidadMedidaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.unidadMedida.UnidadMedidaDao;

@Path("/unidadMedida")
public class UnidadMedidaService {

	@Autowired
    UnidadMedidaDao unidadMedidaDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<UnidadMedidaDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return unidadMedidaDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public UnidadMedidaDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return unidadMedidaDao.findById(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public UnidadMedidaDTO create(UnidadMedidaDTO unidadMedidaDTO) throws Exception {
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(unidadMedidaDTO);
    	// System.out.println("UnidadMedidaDTO:");
    	// System.out.println(val);
        return unidadMedidaDao.create(unidadMedidaDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public UnidadMedidaDTO update(UnidadMedidaDTO unidadMedidaDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	unidadMedidaDao.update(unidadMedidaDTO);
        return unidadMedidaDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
    	unidadMedidaDao.remove(id);
    }

}