package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.bpin.BpinDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.dependenciaOrganizacional.DependenciaOrganizacionalDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.dependenciaOrganizacional.DependenciaOrganizacionalDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.periodo.PeriodoDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.planEstrategico.PlanEstrategicoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.planEstrategico.PlanEstrategicoDao;

@Path("/utils")
public class UtilsService {
	@Autowired
	BpinDao bpinDao;
	@Autowired
	PeriodoDao periodoDao;
	@Autowired
	PlanEstrategicoDao planEstrategicoDao;
	@Autowired
	DependenciaOrganizacionalDao dependenciaOrganizacionalDao;
	@Context HttpServletRequest request;
	@GET
	@Path("serverDate")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Date serverDate() throws Exception {
		return new Date();
	}

	@GET
	@Path("/setSelectedPlanEstrategico/{year}")
	@Produces({ MediaType.APPLICATION_JSON })
	public void setcuryear(@PathParam("year") int year,
			@Context HttpServletRequest request) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		try {
			PlanEstrategicoDTO curPlanEstrategico = planEstrategicoDao
					.findByYear(year);
			WebUtil.almacenarAtributoenSesion(request, "curPlanEstrategico",
					curPlanEstrategico);
		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
		}
	}

	@GET
	@Path("/parametros")
	@Produces({ MediaType.APPLICATION_JSON })
	public HashMap<String, Object> parametros(
			@Context HttpServletRequest request) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		try {
			EmpleadoDTO empleado = (EmpleadoDTO) WebUtil
					.obtenerAtributoenSesion(request, "empleado");
			List<DependenciaOrganizacionalDTO> hierachy = dependenciaOrganizacionalDao
					.hierachy(empleado.getCargo().getDependenciaOrganizacional()
							.getIdDependenciaOrganizacional());
			// ObjectMapper mapper = new ObjectMapper();
			// String val = mapper.writeValueAsString(hierachy);
			// System.out.println("hierachy:");
			// System.out.println(val);

			DependenciaOrganizacionalDTO gerencia = new DependenciaOrganizacionalDTO(), 
					superintendencia = new DependenciaOrganizacionalDTO();
			for (DependenciaOrganizacionalDTO jerarquiaOrg : hierachy) {
				if (jerarquiaOrg.getIdNivelJerarquico() == 4
						|| jerarquiaOrg.getDescripcionDependencia().startsWith(
								"GERENCIA ")) {
					gerencia = jerarquiaOrg;
				}
				if ((jerarquiaOrg.getIdNivelJerarquico() == 5 && !jerarquiaOrg
						.getDescripcionDependencia().startsWith("GERENCIA "))
						|| jerarquiaOrg.getDescripcionDependencia().startsWith(
								"SUPERINTENDENCIA ")) {
					superintendencia = jerarquiaOrg;
				}
			}
			List<DependenciaOrganizacionalDTO> superintendencias = new ArrayList<DependenciaOrganizacionalDTO>();
			if (superintendencia.getIdDependenciaOrganizacional() == 0) {				
				superintendencias = 
						dependenciaOrganizacionalDao.getSuperintendencias(gerencia.getIdDependenciaOrganizacional());
			}
			mapa.put("curGerencia", gerencia);
			mapa.put("curSuperintendencia", superintendencia);
			mapa.put("superintendencias", superintendencias);
			mapa.put("empleado", empleado);
			mapa.put("permisos",
					WebUtil.obtenerAtributoenSesion(request, "permisos"));
			mapa.put("curPlanEstrategico", WebUtil.obtenerAtributoenSesion(
					request, "curPlanEstrategico"));
			// mapa.put("hash", WebUtil.obtenerAtributoenSesion(request,
			// "hash"));

		} catch (Exception e) {
			System.out.println("ERROR " + e.getMessage());
		}
		return mapa;
	}
	 
    @GET @Path("iniciativas/iddependencia/{idDependencia}/month/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Map<String, Object> parametrosIniciativas(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	Map<String, Object> mapa = new HashMap<String, Object>();
    	mapa.put("iniciativas", bpinDao.findIniciativasByMonth(curPlanEstrategico, idDependencia, month));
    	mapa.put("dependencias", dependenciaOrganizacionalDao.findAll());
    	mapa.put("periodos", periodoDao.findAll());
    	return mapa;
    }
}