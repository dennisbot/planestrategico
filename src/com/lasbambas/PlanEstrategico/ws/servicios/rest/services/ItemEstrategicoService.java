package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lasbambas.PlanEstrategico.fineuploader.UploadReceiver;
import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.tools.correo.CorreoFacade;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.itemEstrategico.ItemEstrategicoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.itemEstrategico.ItemEstrategicoDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.planEstrategico.PlanEstrategicoDTO;

@Path("/itemEstrategico")
public class ItemEstrategicoService {

	@Autowired ItemEstrategicoDao itemEstrategicoDao;
	@Context HttpServletRequest request;
	@Context ServletContext context;
	@Autowired
	CorreoFacade correoFacade;
	@Autowired
	VelocityEngine velocityEngine;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ItemEstrategicoDTO> findAll() throws Exception {
        // System.out.println("findAll");
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
//    	System.out.println("context path:");
//    	System.out.println(request.getContextPath());
//    	String redirBaseUrl = context.getInitParameter("BASE_URL") + request.getContextPath();
        return itemEstrategicoDao.findAll(curPlanEstrategico);
    }
    
    @GET @Path("bylevel/{level}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ItemEstrategicoDTO> bylevel(@PathParam("level") int level) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
        return itemEstrategicoDao.bylevel(curPlanEstrategico, level);
    }
    
    @GET @Path("bylevelHierachy/{level}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ItemEstrategicoDTO> bylevelHierachy(@PathParam("level") int level) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
//    	ObjectMapper mapper = new ObjectMapper();
//    	String val = mapper.writeValueAsString(curPlanEstrategico);
//    	System.out.println("planEstrategicoDTO desde request webUtil session:");
//    	System.out.println(val);
    	return itemEstrategicoDao.bylevel(curPlanEstrategico, level, true);
    }
    
    
    @GET @Path("getChildrenAndNephews/{parentId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ItemEstrategicoDTO> getChildrenAndNephews(@PathParam("parentId") int parentId) throws Exception {
    	return itemEstrategicoDao.getChildrenAndNephews(parentId);
    }
    
    @GET @Path("hierachy/{idItemEstrategico}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ItemEstrategicoDTO> hierachy(@PathParam("idItemEstrategico") int idItemEstrategico) throws Exception {
    	return itemEstrategicoDao.hierachy(idItemEstrategico, false);
    }
    
    @GET @Path("byparent/{parentId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ItemEstrategicoDTO> byparent(@PathParam("parentId") int parentId) throws Exception {
    	return itemEstrategicoDao.byparent(parentId);
    }
    
//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ItemEstrategicoDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return itemEstrategicoDao.findById(Integer.parseInt(id));
    }
    
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ItemEstrategicoDTO create(ItemEstrategicoDTO itemEstrategicoDTO) throws Exception {
    	EmpleadoDTO creador = (EmpleadoDTO) WebUtil.obtenerAtributoenSesion(request, "empleado");
    	if (
			itemEstrategicoDTO.getDiscriminator().equals("OBJETIVO_ESPECIFICO") ||
			itemEstrategicoDTO.getDiscriminator().equals("INICIATIVA")
		) {
    		SimpleMailMessage smm = new SimpleMailMessage();
    		
    		if (context.getInitParameter("DEVELOPMENT").equals("1")) {
    			smm.setFrom("dennisbot@matrix.dev");
    			smm.setTo(new String[]{"carlitos@matrix.dev", "carlos@matrix.dev"});	
    		}
    		else {
    			smm.setFrom(creador.getCorreo());
    			smm.setCc(context.getInitParameter("secondFrom"));
    			smm.setTo(context.getInitParameter("mainFrom"));
    		}
			
			String mailSubject = "Iniciativa nueva registrada", 
					tipoItemEstrategico = "una iniciativa";
			
			if (itemEstrategicoDTO.getDiscriminator().equals("OBJETIVO_ESPECIFICO")) {
				mailSubject = "Objetivo Específico registrado";
				tipoItemEstrategico = "un objetivo específico";
			}
			
			smm.setSubject(mailSubject);
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("BASE_URL", context.getInitParameter("BASE_URL"));
			model.put("descripcionItem", itemEstrategicoDTO.getDescripcionItem());
			model.put("creador", creador);
			model.put("tipoItemEstrategico", tipoItemEstrategico);
			
			String itemEstrategicoCreadoTpl = VelocityEngineUtils
					.mergeTemplateIntoString(velocityEngine
							, "velocity/itemEstrategicoCreadoTpl.html"
							, "UTF-8"
							, model
					);
			
			smm.setText(itemEstrategicoCreadoTpl);
			try {
				correoFacade.sendMail(smm);
			} catch (Exception e) {
				System.out.println("ERROR ENVIANDO CORREO: " + e.getMessage());
			}
    	}
//    	ObjectMapper mapper = new ObjectMapper();
//    	String val = mapper.writeValueAsString(itemEstrategicoDTO);
//    	System.out.println("create operation");
//    	System.out.println("itemEstrategicoDTO");
//    	System.out.println(val);
    	
        return itemEstrategicoDao.create(itemEstrategicoDTO, creador.getIdEmpleado());
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ItemEstrategicoDTO update(ItemEstrategicoDTO itemEstrategicoDTO) throws Exception {
//    	ObjectMapper mapper = new ObjectMapper();
//    	String val = mapper.writeValueAsString(itemEstrategicoDTO);
//    	System.out.println("updated operation:");
//    	System.out.println("ItemEstrategicoDTO");
//    	System.out.println(val);
    	EmpleadoDTO empleado = (EmpleadoDTO)WebUtil.obtenerAtributoenSesion(request, "empleado");
    	itemEstrategicoDao.update(itemEstrategicoDTO, empleado.getIdEmpleado());
        return itemEstrategicoDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
//    	System.out.println("entro a delete!");
    	File UPLOAD_DIR = UploadReceiver.UPLOAD_DIR;
    	itemEstrategicoDao.removeChild(id, UPLOAD_DIR);
    }

}