
package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.bpin.BpinDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.bpin.BpinDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.bpin.IniciativaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.dependenciaOrganizacional.DependenciaOrganizacionalDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.periodo.PeriodoDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.planEstrategico.PlanEstrategicoDTO;

@Path("/bpin")
public class BPInService {
	
	@Autowired
	BpinDao bpinDao;
	@Autowired
	DependenciaOrganizacionalDao dependenciaOrganizacionalDao;
	@Autowired
	PeriodoDao periodoDao;
	@Context HttpServletRequest request;
	
	@GET @Path("superintendencias/anual")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> superintendencias() throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findAll(curPlanEstrategico, 12, false, false);
	}
	
	@GET @Path("superintendencias/ytd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> superintendencias_ytd() throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findYTD(curPlanEstrategico);
	}
	
	@GET @Path("superintendencias/month/{month}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> superintendencias_month(@PathParam("month") int month) throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findByMonth(curPlanEstrategico, month);
	}
	
	@GET @Path("gerencias/anual")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> gerencias() throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findAllGerencias(curPlanEstrategico, 12, false, false);
	}
	
	@GET @Path("gerencias/ytd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> gerencias_ytd() throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findYTDGerencias(curPlanEstrategico);
	}
	
	@GET @Path("gerencias/month/{month}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> gerencias_month(@PathParam("month") int month) throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findByMonthGerencias(curPlanEstrategico, month);
	}
	
	@GET @Path("vicepresidencias/anual")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> vps() throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findAllVps(curPlanEstrategico, 12, false, false);
	}
	
	@GET @Path("vicepresidencias/ytd")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> vps_ytd() throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findYTDVps(curPlanEstrategico);
	}
	
	@GET @Path("vicepresidencias/month/{month}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<BpinDTO> vps_month(@PathParam("month") int month) throws Exception {
		PlanEstrategicoDTO curPlanEstrategico = 
				(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
		return bpinDao.findByMonthVps(curPlanEstrategico, month);
	}
	
    
    @GET @Path("iniciativas/iddependencia/{idDependencia}/xmes/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<IniciativaDTO> iniciativasMensual(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	return bpinDao.findIniciativasByMonth(curPlanEstrategico, idDependencia, month);
    }
    
    @GET @Path("iniciativas/iddependencia/{idDependencia}/xmes_acumulado/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<IniciativaDTO> iniciativasMensualAcumulado(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	return bpinDao.findIniciativasByYTD(curPlanEstrategico, idDependencia, month, false);
    }
    
    @GET @Path("iniciativas/iddependencia/{idDependencia}/ytd")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<IniciativaDTO> iniciativasYTD(@PathParam("idDependencia") int idDependencia) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    	System.out.println("entra a year to date");
    	return bpinDao.findIniciativasByYTD(curPlanEstrategico, idDependencia, month, true);
    }
    
    @GET @Path("iniciativas/iddependencia/{idDependencia}/anual")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<IniciativaDTO> iniciativasAnual(@PathParam("idDependencia") int idDependencia) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	int month = 12;
    	return bpinDao.findIniciativasByYTD(curPlanEstrategico, idDependencia, month, false);
    }
    
    @GET @Path("gerencias/iddependencia/{idDependencia}/ytd")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> gerenciasYTD(@PathParam("idDependencia") int idDependencia) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    	return bpinDao.findObjetivosEspecificosByYTD(curPlanEstrategico, idDependencia, month, false, true);
    }
    @GET @Path("gerencias/iddependencia/{idDependencia}/xmes/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> avanceObjetivosEspecificosMensual(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	return bpinDao.findObjetivosEspecificosByYTD(curPlanEstrategico, idDependencia, month, true, false);
    }
    
    @GET @Path("gerencias/iddependencia/{idDependencia}/xmes_acumulado/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> avanceObjetivosEspecificosMensualAcumulado(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	return bpinDao.findObjetivosEspecificosByYTD(curPlanEstrategico, idDependencia, month, false, false);
    }
    
    @GET @Path("gerencias/iddependencia/{idDependencia}/anual")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> objetivosEspecificosAnual(@PathParam("idDependencia") int idDependencia) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	int month = 12;
    	return bpinDao.findObjetivosEspecificosByYTD(curPlanEstrategico, idDependencia, month, false, false);
    }

    
    @GET @Path("vps/iddependencia/{idDependencia}/ytd")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> vpsYTD(@PathParam("idDependencia") int idDependencia) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    	return bpinDao.findObjetivosClaveByYTD(curPlanEstrategico, idDependencia, month, false, true);
    }
    @GET @Path("vps/iddependencia/{idDependencia}/xmes/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> avanceObjetivosClaveMensual(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	return bpinDao.findObjetivosClaveByYTD(curPlanEstrategico, idDependencia, month, true, false);
    }
    
    @GET @Path("vps/iddependencia/{idDependencia}/xmes_acumulado/{month}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> avanceObjetivosClaveMensualAcumulado(@PathParam("idDependencia") int idDependencia,
    		@PathParam("month") int month) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	return bpinDao.findObjetivosClaveByYTD(curPlanEstrategico, idDependencia, month, false, false);
    }
    
    @GET @Path("vps/iddependencia/{idDependencia}/anual")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<BpinDTO> objetivosClaveAnual(@PathParam("idDependencia") int idDependencia) throws Exception {
    	PlanEstrategicoDTO curPlanEstrategico = 
    			(PlanEstrategicoDTO)WebUtil.obtenerAtributoenSesion(request, "curPlanEstrategico");
    	int month = 12;
    	return bpinDao.findObjetivosClaveByYTD(curPlanEstrategico, idDependencia, month, false, false);
    }
    
    
   
}