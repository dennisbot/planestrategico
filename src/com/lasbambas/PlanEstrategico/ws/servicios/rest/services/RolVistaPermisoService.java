package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.rolVistaPermiso.RolVistaPermisoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.rolVistaPermiso.RolVistaPermisoDao;

@Path("/rolVistaPermiso")
public class RolVistaPermisoService {

	@Autowired
    RolVistaPermisoDao rolVistaPermisoDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<RolVistaPermisoDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return rolVistaPermisoDao.findAll();
    }
    
    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public RolVistaPermisoDTO findById(@PathParam("id") int idParametro) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return rolVistaPermisoDao.findById(idParametro);
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public RolVistaPermisoDTO create(RolVistaPermisoDTO parametroDetalle) throws Exception {
        return rolVistaPermisoDao.create(parametroDetalle);
    }

    @POST @Path("batch")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public boolean create(List<RolVistaPermisoDTO> rvpCollection) throws Exception {
//    	ObjectMapper mapper = new ObjectMapper();
//    	String val = mapper.writeValueAsString(rvpCollection);
//    	System.out.println("rvpCollection");
//    	System.out.println(val);
        return rolVistaPermisoDao.create(rvpCollection);
    }
}