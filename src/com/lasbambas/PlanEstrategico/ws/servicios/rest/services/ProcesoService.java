package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.proceso.ProcesoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.proceso.ProcesoDao;

@Path("/proceso")
public class ProcesoService {

	@Autowired
    ProcesoDao procesoDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ProcesoDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return procesoDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ProcesoDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return procesoDao.findById(Integer.parseInt(id));
    }

    @GET @Path("subprocesos/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ProcesoDTO> findSubProcesosById(@PathParam("id") int id)throws NumberFormatException, Exception {
    	// System.out.println("findById " + id);
    	return procesoDao.findSubProcesosById(id);
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ProcesoDTO create(ProcesoDTO proceso) throws Exception {
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(proceso);
    	// System.out.println(val);
        return procesoDao.create(proceso);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ProcesoDTO update(ProcesoDTO proceso) throws Exception {
    	System.out.println("updating:");
    	ObjectMapper mapper = new ObjectMapper();
    	String val = mapper.writeValueAsString(proceso);
    	System.out.println(val);
    	procesoDao.update(proceso);
        return proceso;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
    	System.out.println("llega a eliminar");
    	procesoDao.removeChild(id);
    }

}