package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.parametroDetalle.ParametroDetalleDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.parametroDetalle.ParametroDetalleDao;

@Path("/rol")
public class RolService {

	@Autowired
    ParametroDetalleDao parametroDetalleDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ParametroDetalleDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return parametroDetalleDao.findAll("PESTRATEGICO_ROL");
    }
    
    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ParametroDetalleDTO findById(@PathParam("id") int idParametro) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return parametroDetalleDao.findById(idParametro);
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ParametroDetalleDTO create(ParametroDetalleDTO parametroDetalle) throws Exception {
        return parametroDetalleDao.create("PESTRATEGICO_ROL", parametroDetalle);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ParametroDetalleDTO update(ParametroDetalleDTO parametroDetalle) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	parametroDetalleDao.update("PESTRATEGICO_ROL", parametroDetalle);
        return parametroDetalle;
    }

	@DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int idParametro) throws Exception {
		parametroDetalleDao.remove(idParametro);    	
    }

}