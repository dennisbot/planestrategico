package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.lasbambas.PlanEstrategico.fineuploader.UploadReceiver;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.archivo.ArchivoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.archivo.ArchivoDao;

@Path("/archivo")
public class ArchivoService {

	@Autowired
    ArchivoDao archivoDao;
	
	@Context ServletContext context;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ArchivoDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return archivoDao.findAll();
    }
    @GET @Path("bymeta/{idItemEstrategico}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<ArchivoDTO> findAllByMeta(@PathParam("idItemEstrategico") int idItemEstrategico) throws Exception {
        // System.out.println("findAll");
        return archivoDao.findAllByMeta(idItemEstrategico);
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ArchivoDTO findById(@PathParam("id") String idArchivo) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return archivoDao.findById(idArchivo);
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ArchivoDTO create(ArchivoDTO archivoDTO) throws Exception {
        return archivoDao.create(archivoDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ArchivoDTO update(ArchivoDTO archivoDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	archivoDao.update(archivoDTO);
        return archivoDTO;
    }

	@DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") String uuid) throws Exception {
    	
    	File UPLOAD_DIR = UploadReceiver.UPLOAD_DIR;
    	
		FileUtils.deleteDirectory(new File(UPLOAD_DIR, uuid));

		if (new File(UPLOAD_DIR, uuid).exists()) {
			System.out.println("no se pudo borrar el archivo " + uuid);
//			log.warn("couldn't find or delete " + uuid);
		} else {
			System.out.println("se borró el archivo exitosamente");
//			log.info("deleted " + uuid);
		}
    	archivoDao.remove(uuid);    	
    }

}