package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;

import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.tools.correo.CorreoFacade;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.dependenciaOrganizacional.DependenciaOrganizacionalDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.dependenciaOrganizacional.DependenciaOrganizacionalDao;

@Path("/dependenciaOrganizacional")
public class DependenciaOrganizacionalService {

	@Autowired
    DependenciaOrganizacionalDao dependenciaOrganizacionalDao;
		
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<DependenciaOrganizacionalDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return dependenciaOrganizacionalDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public DependenciaOrganizacionalDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return dependenciaOrganizacionalDao.findById(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public DependenciaOrganizacionalDTO create(DependenciaOrganizacionalDTO dependenciaOrganizacionalDTO) throws Exception {
        return dependenciaOrganizacionalDao.create(dependenciaOrganizacionalDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public DependenciaOrganizacionalDTO update(DependenciaOrganizacionalDTO dependenciaOrganizacionalDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	dependenciaOrganizacionalDao.update(dependenciaOrganizacionalDTO);
        return dependenciaOrganizacionalDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
    	dependenciaOrganizacionalDao.remove(id);
    }

}