package com.lasbambas.PlanEstrategico.ws.servicios.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.tipoMetrica.TipoMetricaDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.tipoMetrica.TipoMetricaDao;

@Path("/tipoMetrica")
public class TipoMetricaService {

	@Autowired
    TipoMetricaDao tipoMetricaDao;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<TipoMetricaDTO> findAll() throws Exception {
        // System.out.println("findAll");
        return tipoMetricaDao.findAll();
    }

//    @GET @Path("search/{query}")
//    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//    public List<GerenciaDTO> findByName(@PathParam("query") String query) {
//        System.out.println("findByName: " + query);
//        return superintendenciaDao.findByName(query);
//    }

    @GET @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public TipoMetricaDTO findById(@PathParam("id") String id) throws NumberFormatException, Exception {
        // System.out.println("findById " + id);
        return tipoMetricaDao.findById(Integer.parseInt(id));
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public TipoMetricaDTO create(TipoMetricaDTO tipoMetricaDTO) throws Exception {
    	// ObjectMapper mapper = new ObjectMapper();
    	// String val = mapper.writeValueAsString(tipoMetricaDTO);
    	// System.out.println("tipoMetricaDTO");
    	// System.out.println(val);
        return tipoMetricaDao.create(tipoMetricaDTO);
    }

    @PUT @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public TipoMetricaDTO update(TipoMetricaDTO tipoMetricaDTO) throws Exception {
        // System.out.println("Updating AreaInspeccionadaDTO: " + areaInspeccionadaDTO.getIdJefeArea());
    	tipoMetricaDao.update(tipoMetricaDTO);
        return tipoMetricaDTO;
    }

    @DELETE @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void remove(@PathParam("id") int id) throws Exception {
    	tipoMetricaDao.remove(id);
    }

}