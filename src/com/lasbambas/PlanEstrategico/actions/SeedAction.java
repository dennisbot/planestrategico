package com.lasbambas.PlanEstrategico.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.springframework.core.SpringVersion;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.maestro.empleado.EmpleadoFacade;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.maestro.gerencia.GerenciaFacade;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.maestro.superintendencia.SuperintendenciaFacade;

public class SeedAction extends DispatchAction
{

	static Logger logger = Logger.getLogger("dashboardLog");

	private EmpleadoFacade empleadoFacade;
	private GerenciaFacade gerenciaFacade;
	private SuperintendenciaFacade superintendenciaFacade;


	public void setSuperintendenciaFacade(
			SuperintendenciaFacade superintendenciaFacade) {
		this.superintendenciaFacade = superintendenciaFacade;
	}

	public void setGerenciaFacade(GerenciaFacade gerenciaFacade) {
		this.gerenciaFacade = gerenciaFacade;
	}

	public void setEmpleadoFacade(EmpleadoFacade empleadoFacade) {
		this.empleadoFacade = empleadoFacade;
	}

	public ActionForward getPlanEstrategicoValues (
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception {

		response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        response.setHeader( "Pragma", "no-cache" );
        response.setHeader( "Cache-Control", "no-cache" );
        response.setDateHeader( "Expires", 0 );

        PrintWriter out = response.getWriter();

		try {
			System.out.println("Spring Version: ");
			System.out.println(SpringVersion.getVersion());
			HashMap<String, Object> mapa = new HashMap<String, Object>();

			//mapa.put("empleados", empleadoFacade.mostrarResponsablesPlanEstrategico());
			mapa.put("gerencias", gerenciaFacade.mostrar());
			mapa.put("superintendencias", superintendenciaFacade.mostrar());

			//area
			//category

			ObjectMapper mapper = new ObjectMapper();
			out.println(mapper.writeValueAsString(mapa));

		} catch (Exception e) {
		    logger.error(e.getMessage(), e);
		} finally {
            out.close();
        }
		return null;
    }
	public ActionForward mostrarResponsablesPlanEstrategico (
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception {

		response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        response.setHeader( "Pragma", "no-cache" );
        response.setHeader( "Cache-Control", "no-cache" );
        response.setDateHeader( "Expires", 0 );

        PrintWriter out = response.getWriter();

		try {
			ObjectMapper mapper = new ObjectMapper();
//			out.println(mapper.writeValueAsString(empleadoFacade.mostrarResponsablesPlanEstrategico()));

		} catch (Exception e) {
		    logger.error(e.getMessage(), e);
		} finally {
            out.close();
        }
		return null;
    }
	public ActionForward getGerencias (
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception {

		response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        response.setHeader( "Pragma", "no-cache" );
        response.setHeader( "Cache-Control", "no-cache" );
        response.setDateHeader( "Expires", 0 );

        PrintWriter out = response.getWriter();

		try {
			ObjectMapper mapper = new ObjectMapper();
			out.println(mapper.writeValueAsString(gerenciaFacade.mostrar()));

		} catch (Exception e) {
		    logger.error(e.getMessage(), e);
		} finally {
            out.close();
        }
		return null;
    }
	public ActionForward getSuperintendencias (
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception {

		response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        response.setHeader( "Pragma", "no-cache" );
        response.setHeader( "Cache-Control", "no-cache" );
        response.setDateHeader( "Expires", 0 );

        PrintWriter out = response.getWriter();

		try {
			ObjectMapper mapper = new ObjectMapper();
			out.println(mapper.writeValueAsString(superintendenciaFacade.mostrar()));

		} catch (Exception e) {
		    logger.error(e.getMessage(), e);
		} finally {
            out.close();
        }
		return null;
    }

	public ActionForward getSuperintendenciasPorGerencia (
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
			) throws Exception {

		response.setContentType("application/json;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );

		PrintWriter out = response.getWriter();

		int id_gerencia = Integer.valueOf(request.getParameter("id_gerencia"));

		try {
			ObjectMapper mapper = new ObjectMapper();
			out.println(mapper.writeValueAsString(superintendenciaFacade.mostrarPorGerencia(id_gerencia)));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			out.close();
		}
		return null;
	}
}
