/* LoginAction - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.lasbambas.PlanEstrategico.actions;

import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.util.MessageResources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lasbambas.PlanEstrategico.util.WebUtil;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDao;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.PermisoDTO;

public class AuthAction extends DispatchAction
{
    static Logger logger = Logger.getLogger("xminePortalLog");

    private EmpleadoDao empleadoDao;
    
    public void setEmpleadoDao(EmpleadoDao empleadoDao) {
		this.empleadoDao = empleadoDao;
	}

    public ActionForward iniciar (
    		ActionMapping mapping,
    		ActionForm form,
    		HttpServletRequest request,
    		HttpServletResponse response
	)
	throws Exception {

		String forward = "";
		try {
		    String user = System.getProperty("user.name");
		    if (user != null)
			WebUtil.almacenarAtributo(request, "userLogged", user);
		    forward = "login";
		} catch (Exception e) {
		    logger.error(e.getMessage(), e);
		    forward = "error";
		}
		return mapping.findForward(forward);
    }

    public ActionForward estaLogueado(
    		ActionMapping mapping,
    		ActionForm form,
    		HttpServletRequest request,
    		HttpServletResponse response)
    {
    	String forward = "home";
    	try {
    		// probamos si existe sesión con cookie y con la sesión
    		boolean loggedIn = WebUtil.obtenerAtributoenSesion(request, "empleado") != null;
    		boolean logueado = loggedIn && WebUtil.obtenerValorCookie(request, "usuario") != null;
    		if (!logueado) {
                /* eliminamos sesiones */
    			WebUtil.eliminarSesion(request);
    			forward = "login";
    		}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			//por el momento le mostramos el login
			//forward = "error";
			forward = "login";

		}
    	return mapping.findForward(forward);
    }

    public ActionForward checkLoggedIn(
    		ActionMapping mapping,
    		ActionForm form,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {
    	String forward = "home_real_jsp";

		Object empleado = WebUtil.obtenerAtributoenSesion(request, "empleado");
		try {
    		boolean loggedIn = WebUtil.obtenerAtributoenSesion(request, "empleado") != null;
    		// probamos si existe sesión con cookie y con la sesión
    		boolean logueado = loggedIn && WebUtil.obtenerValorCookie(request, "usuario") != null;
    		if (!logueado) forward = "login";
		} catch (Exception e) {
			System.out.println("e.getMessage()");
			System.out.println(e.getMessage());
			forward = "login";
		}
    	return mapping.findForward(forward);
    }

	public ActionForward login (
    		ActionMapping mapping,
    		ActionForm form,
    		HttpServletRequest request,
    		HttpServletResponse response
    		) throws Exception {
    	
		String forward = "login";
    	EmpleadoDTO empleado = new EmpleadoDTO();
		empleado.setUsuario(request.getParameter("txtUsuario"));
		empleado.setPassword(request.getParameter("txtPassword"));
		
		empleado = empleadoDao.canLogin(empleado);
		
		if (empleado != null) {
			HashMap<String, PermisoDTO> permisos = empleadoDao.getMappedPermissions(empleado);
			WebUtil.almacenarAtributoenSesion(request, "empleado", empleado);
			WebUtil.almacenarAtributoenSesion(request, "permisos", permisos);
			/* cookie para 12 horas de sesión */
			Cookie cookie = new Cookie("usuario", URLEncoder.encode(empleado.getUsuario(), "UTF-8"));
			cookie.setMaxAge(60 * 60 * 12);
			response.addCookie(cookie);
			forward = "planAnual";
		}
		else {
			MessageResources resources = getResources(request);
			String mensaje = resources.getMessage("login.mensaje.datosIncorrectos");
			WebUtil.almacenarAtributoenSesion(
					request,
					"mensajeUsuario",
					mensaje
			);
		}
    	return mapping.findForward(forward);
    }

    public ActionForward logout (
    		ActionMapping mapping,
    		ActionForm form,
    		HttpServletRequest request,
    		HttpServletResponse response
		) throws Exception {

    	String forward = "logout";
    	try {
    		Cookie[] mis_cookies = request.getCookies();
    		String[] names = new String[] {"usuario"};
    		if (mis_cookies != null) {
    			for (Cookie cookie : mis_cookies) {
    				for (String name : names) {
    					if (cookie.getName().equals(name)) {
    						cookie.setMaxAge(0);
    						response.addCookie(cookie);
    					}
    				}
    			}
    		}

    		WebUtil.eliminarSesion(request);

        	return mapping.findForward(forward);

		} catch (Exception e) {
			forward = "error";
			return mapping.findForward(forward);
		}
    }
    public ActionForward cerrar (
    		ActionMapping mapping,
    		ActionForm form,
    		HttpServletRequest request,
    		HttpServletResponse response
	) throws Exception {
	String forward = "";
		try {
		    WebUtil.eliminarSesion(request);
		    forward = "logout";
		} catch (Exception e) {
		    logger.error(e.getMessage(), e);
		    forward = "error";
		}
		return mapping.findForward(forward);
    }
    public ActionForward saludar (
    		ActionMapping mapping,
    		ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception {
		PrintWriter out = response.getWriter();
		out.println("funciona!! metodo saludar de la acción LoginAction");
		return null;
	}

}