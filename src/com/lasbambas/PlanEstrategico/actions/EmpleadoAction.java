package com.lasbambas.PlanEstrategico.actions;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.maestro.empleado.EmpleadoFacade;
import com.pe.xstratacopper.xslb.xmine.admin.middleware.dao.maestro.empleado.EmpleadoDTO;

public class EmpleadoAction extends DispatchAction
{

	static Logger logger = Logger.getLogger("dashboardLog");

	private EmpleadoFacade empleadoFacade;

	public void setEmpleadoFacade(EmpleadoFacade empleadoFacade) {
		this.empleadoFacade = empleadoFacade;
	}

	public ActionForward mostrar (
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception {

		response.setContentType("application/json;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        response.setHeader( "Pragma", "no-cache" );
        response.setHeader( "Cache-Control", "no-cache" );
        response.setDateHeader( "Expires", 0 );

        PrintWriter out = response.getWriter();
		try {
			List<EmpleadoDTO> empleados = empleadoFacade.mostrar();
			for (EmpleadoDTO empleado : empleados) {
				System.out.println("observador.getNombres: " + empleado.getNombres());
			}
			ObjectMapper mapper = new ObjectMapper();
			out.println(mapper.writeValueAsString(empleados));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		    logger.error(e.getMessage(), e);
		} finally {
            out.close();
        }
		return null;
    }
}
