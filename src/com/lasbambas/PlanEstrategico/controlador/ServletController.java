/* ServletController - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.lasbambas.PlanEstrategico.controlador;

import java.io.File;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.apache.log4j.PropertyConfigurator;
import org.apache.struts.action.ActionServlet;

public class ServletController extends ActionServlet {
	private static final long serialVersionUID = 8834871247877246620L;
//	static Logger logger = Logger.getLogger("xmineEppLog");

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			inicializarLog4J(config);
			getServletContext().setAttribute("servidorReportes",
					config.getInitParameter("servidorReportes"));
			getServletContext().setAttribute("rutaIndice",
					config.getInitParameter("rutaIndice"));
			getServletContext().setAttribute("rutaTemp",
					config.getInitParameter("rutaTemp"));
			getServletContext().setAttribute("rutaStorage",
					config.getInitParameter("rutaStorage"));
			getServletContext().setAttribute("rutaPapelera",
					config.getInitParameter("rutaPapelera"));
			getServletContext().setAttribute("rutaFotos",
					config.getInitParameter("rutaFotos"));
			getServletContext().setAttribute("rutaFotosProductos",
					config.getInitParameter("rutaFotosProductos"));
		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
			System.out.println("error init ServletController - ServletConfig " + e);
		}
	}

	public void inicializarLog4J(ServletConfig config) throws ServletException {
		try {
			String rutaLog4J = getServletContext().getRealPath(
					config.getInitParameter("log4j"));
			if (rutaLog4J == null || rutaLog4J.length() == 0
					|| !new File(rutaLog4J).isFile())
				throw new ServletException();
			String watch = config.getInitParameter("watch");
			if (watch != null && watch.equalsIgnoreCase("true"))
				PropertyConfigurator.configureAndWatch(rutaLog4J);
			else
				PropertyConfigurator.configure(rutaLog4J);
		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
			System.out.println("error init ServletController - inicializarLog4J " + e);
		}
	}
}
