/* URLFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.lasbambas.PlanEstrategico.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class CharacterEncodingFilter implements Filter {
	final Logger logger = Logger.getLogger("controlWebLog");
	private FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		try {
			this.filterConfig = filterConfig;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		req.setCharacterEncoding("UTF8");
		res.setCharacterEncoding("UTF8");
		chain.doFilter(req, res);		
	}

	public void destroy() {
		filterConfig = null;
	}

}
