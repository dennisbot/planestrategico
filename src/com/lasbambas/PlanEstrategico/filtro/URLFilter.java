/* URLFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.lasbambas.PlanEstrategico.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class URLFilter implements Filter {
	final Logger logger = Logger.getLogger("controlWebLog");
	private FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		try {
			this.filterConfig = filterConfig;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String path = req.getServletPath();
		String PARAM = "?no_session";
		try {
			//// validate user session and restrict access to other parts.
//			String login = filterConfig.getInitParameter("login");
//			String loginAction = filterConfig.getInitParameter("loginAction");
//			String indexAction = filterConfig.getInitParameter("indexAction");
//			boolean isLoggedIn = WebUtil.obtenerAtributoenSesion(req,
//					"empleado") != null;
//			if (login != null && !login.equals("") && loginAction != null
//					&& !loginAction.equals("")) {
//				if (!path.startsWith(loginAction) && !isLoggedIn) {
//					if (path.startsWith(indexAction)) {
//						res.sendRedirect(login + PARAM);
//					}else{
//						res.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());
//					}
//					return;
//				}
//			}
			chain.doFilter(request, response);
		} catch (ServletException se) {
			logger.error(se.getMessage(), se);
			throw se;
		}
	}

	public void destroy() {
		filterConfig = null;
	}

}
