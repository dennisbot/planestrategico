package com.lasbambas.PlanEstrategico.tareas;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.james.mime4j.field.datetime.DateTime;
import org.apache.velocity.app.VelocityEngine;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.pe.xstratacopper.xslb.xmine.admin.logicanegocio.facade.tools.correo.CorreoFacade;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.empleado.EmpleadoDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.notificacion.NotificacionDTO;
import com.pe.xstratacopper.xslb.xmine.planEstrategico.dao.notificacion.NotificacionDao;

public class NotificacionesAutomaticasJob extends QuartzJobBean {
	private ServletContext context;
	private NotificacionDao notificacionDao;
	private VelocityEngine velocityEngine;
	private CorreoFacade correoFacade;

	public void setServletContext(ServletContext context) {
		this.context = context;
	}

	public void setNotificacionDao(NotificacionDao notificacionDao) {
		this.notificacionDao = notificacionDao;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setCorreoFacade(CorreoFacade correoFacade) {
		this.correoFacade = correoFacade;
	}

	@Override
	protected void executeInternal(JobExecutionContext paramJobExecutionContext)
			throws JobExecutionException {
//		int minuto = Calendar.getInstance().get(Calendar.MINUTE);
//		System.out.println("minuto:" + minuto);
//		System.out.println("=================");
//		System.out.println("");
		int dia = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
//		dia = 9;
//		si no es cualquiera de estos dias entonces no hay nada que ejecutar
		if (!(dia == 1 || dia == 5 || dia == 7 || dia == 9))
			return;
		
		List<NotificacionDTO> metasIncompletas = new ArrayList<NotificacionDTO>();
		try {
			// jalamos las metas sin registro de avance del mes anterior al
			// actual
			// se supone que son las que están con Calendar.MONTH
			metasIncompletas = notificacionDao.findMetasIncompletas(Calendar
					.getInstance().get(Calendar.YEAR), Calendar.getInstance()
					.get(Calendar.MONTH));
		} catch (Exception e) {
			e.printStackTrace();
		}
		SimpleMailMessage smm = new SimpleMailMessage();
		for (NotificacionDTO notificacion : metasIncompletas) {
			if (context.getInitParameter("DEVELOPMENT").equals("1"))
				smm.setFrom("dennisbot@matrix.dev");
			else
				smm.setFrom(context.getInitParameter("mainFrom"));

			ArrayList<String> responsablesCorreo = new ArrayList<String>();
			ArrayList<EmpleadoDTO> responsables = new ArrayList<EmpleadoDTO>();
			responsablesCorreo.add(notificacion.getKeyuser().getCorreo());
			responsables.add(notificacion.getKeyuser());
			for (EmpleadoDTO responsable : notificacion.getResponsables()) {
				responsablesCorreo.add(responsable.getCorreo());
				responsables.add(responsable);
			}

			String fase = "[INFO - FASE I]", templateTpl = "";
			Map<String, Object> model = new HashMap<String, Object>();
			String mailSubject = " - NOTIFICACIÓN AUTOMÁTICA DE AVANCE NO REGISTRADO - Sistema de Plan Estratégico";
			switch (dia) {
			case 1:
				model.put("BASE_URL", context.getInitParameter("BASE_URL"));
				model.put("notificacion", notificacion);
				model.put("responsables", responsables);
				model.put("nameFrom", context.getInitParameter("mainNameFrom"));
				model.put("cargoFrom",
						context.getInitParameter("mainCargoFrom"));
				templateTpl = "velocity/notificacionAvanceNoRegistradoFase1Tpl.html";
				mailSubject = fase + mailSubject;
				smm.setSubject(mailSubject);
				String templateNotificacionTpl = VelocityEngineUtils
						.mergeTemplateIntoString(velocityEngine, templateTpl,
								"UTF-8", model);
				smm.setText(templateNotificacionTpl);

				if (context.getInitParameter("DEVELOPMENT").equals("1"))
					smm.setTo(new String[] { "carlitos@matrix.dev",
							"carlos@matrix.dev" });
				else {
					smm.setTo(responsablesCorreo.toArray(new String[] {}));
					smm.setCc(context.getInitParameter("secondFrom"));
				}
				try {
					correoFacade.sendMail(smm);
				} catch (Exception e) {
					correoFacade
							.notificar(
									"ERROR ENVIANDO CORREO ELECTRÓNICO",
									context.getInitParameter("mainFrom"),
									new String[] { context
											.getInitParameter("secondFrom") },
									"Se ha encontrado una Excepción enviando notificación"
											+ " de iniciativa sin registro de metas, con el siguiente detalle:<br>"
											+ e.getMessage());
					System.out.println("ERROR ENVIANDO CORREO: "
							+ e.getMessage());
				}
				break;
			case 5:
				model.put("BASE_URL", context.getInitParameter("BASE_URL"));
				model.put("notificacion", notificacion);
				model.put("nameFrom", context.getInitParameter("mainNameFrom"));
				model.put("cargoFrom",
						context.getInitParameter("mainCargoFrom"));
				model.put("responsables", responsables);
				// además
				model.put("supervisor", notificacion.getSupervisor());
				templateTpl = "velocity/notificacionAvanceNoRegistradoFase2Tpl.html";
				fase = "[INFO - FASE II]";
				mailSubject = fase + mailSubject;
				smm.setSubject(mailSubject);
				templateNotificacionTpl = VelocityEngineUtils
						.mergeTemplateIntoString(velocityEngine, templateTpl,
								"UTF-8", model);
				smm.setText(templateNotificacionTpl);
				if (context.getInitParameter("DEVELOPMENT").equals("1")) {
					smm.setTo(new String[] { "carlitos@matrix.dev",
							"carlos@matrix.dev" });
				} else {
					// notificar también a superintendente
					if (notificacion.getSupervisor().getCorreo() != null) {
						smm.setTo(notificacion.getSupervisor().getCorreo());
						responsablesCorreo.add(context
								.getInitParameter("secondFrom"));
						smm.setCc(responsablesCorreo.toArray(new String[] {}));
					} else {
						correoFacade
								.notificar(
										"ERROR ENVIANDO CORREO ELECTRÓNICO",
										context.getInitParameter("mainFrom"),
										new String[] { context
												.getInitParameter("secondFrom") },
										"el Superintendente / Supervisor <strong>"
												+ notificacion.getSupervisor()
														.getNombreParaMostrar()
												+ "</strong> no tiene correo asignado, no se pudo enviar el mensaje");
					}
				}
				try {
					correoFacade.sendMail(smm);
				} catch (Exception e) {
					correoFacade
							.notificar(
									"ERROR ENVIANDO CORREO ELECTRÓNICO",
									context.getInitParameter("mainFrom"),
									new String[] { context
											.getInitParameter("secondFrom") },
									"Se ha encontrado una Excepción enviando notificación"
											+ " de iniciativa sin registro de metas, con el siguiente detalle:<br>"
											+ e.getMessage());
					System.out.println("ERROR ENVIANDO CORREO: "
							+ e.getMessage());
				}
				break;
			case 7:
				model.put("BASE_URL", context.getInitParameter("BASE_URL"));
				model.put("notificacion", notificacion);
				model.put("nameFrom", context.getInitParameter("mainNameFrom"));
				model.put("cargoFrom",
						context.getInitParameter("mainCargoFrom"));
				model.put("responsables", responsables);
				templateTpl = "velocity/notificacionAvanceNoRegistradoFase3Tpl.html";
				fase = "[INFO - FASE III]";
				mailSubject = fase + mailSubject;
				smm.setSubject(mailSubject);
				for (EmpleadoDTO gerente : notificacion.getGerentes()) {
					model.put("gerente", gerente);
					if (context.getInitParameter("DEVELOPMENT").equals("1")) {
						smm.setTo(new String[] { "carlitos@matrix.dev",
								"carlos@matrix.dev" });
					} else {
						// mensaje al gerente
						if (gerente.getCorreo() != null) {
							smm.setTo(gerente.getCorreo());
							responsablesCorreo.add(context
									.getInitParameter("secondFrom"));
							// notificar también a superintendente
							responsablesCorreo.add(notificacion.getSupervisor()
									.getCorreo());
							responsables.add(notificacion.getSupervisor());
							smm.setCc(responsablesCorreo
									.toArray(new String[] {}));
						} else {
							correoFacade
									.notificar(
											"ERROR ENVIANDO CORREO ELECTRÓNICO",
											context.getInitParameter("mainFrom"),
											new String[] { context
													.getInitParameter("secondFrom") },
											"el Gerente <strong>"
													+ gerente
															.getNombreParaMostrar()
													+ "</strong> no tiene correo asignado, no se pudo enviar el mensaje");
						}
					}
					templateNotificacionTpl = VelocityEngineUtils
							.mergeTemplateIntoString(velocityEngine,
									templateTpl, "UTF-8", model);
					smm.setText(templateNotificacionTpl);
					try {
						correoFacade.sendMail(smm);
					} catch (Exception e) {
						correoFacade
								.notificar(
										"ERROR ENVIANDO CORREO ELECTRÓNICO",
										context.getInitParameter("mainFrom"),
										new String[] { context
												.getInitParameter("secondFrom") },
										"Se ha encontrado una Excepción enviando notificación"
												+ " de iniciativa sin registro de metas, con el siguiente detalle:<br>"
												+ e.getMessage());
						System.out.println("ERROR ENVIANDO CORREO: "
								+ e.getMessage());
					}
				}
				break;
			default:
				// dia == 9
				model.put("BASE_URL", context.getInitParameter("BASE_URL"));
				model.put("notificacion", notificacion);
				model.put("nameFrom", context.getInitParameter("mainNameFrom"));
				model.put("cargoFrom",
						context.getInitParameter("mainCargoFrom"));
				model.put("responsables", responsables);
				templateTpl = "velocity/notificacionAvanceNoRegistradoFase4Tpl.html";
				fase = "[INFO - FASE IV]";
				mailSubject = fase + mailSubject;
				smm.setSubject(mailSubject);
				for (EmpleadoDTO vp : notificacion.getVicepresidentes()) {
					model.put("vp", vp);
					if (context.getInitParameter("DEVELOPMENT").equals("1")) {
						smm.setTo(new String[] { "carlitos@matrix.dev",
								"carlos@matrix.dev" });
					} else {
						// mensaje al vp
						if (vp.getCorreo() != null) {
							smm.setTo(vp.getCorreo());
							for (EmpleadoDTO gerente : notificacion
									.getGerentes()) {
								responsablesCorreo.add(gerente.getCorreo());
								responsables.add(gerente);
							}
							responsablesCorreo.add(context
									.getInitParameter("secondFrom"));
							// notificar también a superintendente
							responsablesCorreo.add(notificacion.getSupervisor()
									.getCorreo());
							responsables.add(notificacion.getSupervisor());
							smm.setCc(responsablesCorreo
									.toArray(new String[] {}));
						} else {
							correoFacade
									.notificar(
											"ERROR ENVIANDO CORREO ELECTRÓNICO",
											context.getInitParameter("mainFrom"),
											new String[] { context
													.getInitParameter("secondFrom") },
											"el Vice-presidente <strong>"
													+ vp.getNombreParaMostrar()
													+ "</strong> no tiene correo asignado, no se pudo enviar el mensaje");
						}
					}

					templateNotificacionTpl = VelocityEngineUtils
							.mergeTemplateIntoString(velocityEngine,
									templateTpl, "UTF-8", model);
					smm.setText(templateNotificacionTpl);
					try {
						correoFacade.sendMail(smm);
					} catch (Exception e) {
						correoFacade
								.notificar(
										"ERROR ENVIANDO CORREO ELECTRÓNICO",
										context.getInitParameter("mainFrom"),
										new String[] { context
												.getInitParameter("secondFrom") },
										"Se ha encontrado una Excepción enviando notificación"
												+ " de iniciativa sin registro de metas, con el siguiente detalle:<br>"
												+ e.getMessage());
						System.out.println("ERROR ENVIANDO CORREO: "
								+ e.getMessage());
					}
				}
				break;
			}
		}
		correoFacade
				.notificar(
						"Notificación de Ejecución de cronJob para avances de iniciativas",
						context.getInitParameter("mainFrom"),
						new String[] { context.getInitParameter("mainFrom"),
								context.getInitParameter("secondFrom") },
						"Se ha realizado la notificación de mensajes de aviso de falta de registro de "
								+ "metas del mes pasado exitosamente");
	}
}
